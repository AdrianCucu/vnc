/*
https://www.sqlite.org/c3ref/exec.html
*/
#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "db.h"

int main(int argc, char **argv)
{
  db_connection *db_conn;
  int i;

  if (db_open_connection(&db_conn) != SQL_OK)
  {
    fprintf(stderr, "Failed to open db\n");
    exit(EXIT_FAILURE);
  }

  if (db_create_table(db_conn) != SQL_OK)
  {
    fprintf(stderr, "db_create_table error: %s\n", db_conn->errmsg);
  }

  db_select_all(db_conn);

  if (db_insert_new_client(db_conn, "192.168.1.1", strlen("192.168.1.1"),
                           "12345678", strlen("12345678"),
                           "Salut", strlen("Salut"),
                           "decafeca", strlen("decafeca")) != SQL_OK)
  {
    fprintf(stderr, "Error db_insert_new_client: %s\n", db_conn->errmsg);
  }

  if (db_insert_new_client(db_conn, "192.168.1.1", strlen("192.168.1.1"),
                           "12345679", strlen("12345678"),
                           "wefsd41", strlen("wefsd41"),
                           "coff3e3e3", strlen("coff3e3e3")) != SQL_OK)
  {
    fprintf(stderr, "Error db_insert_new_client: %s\n", db_conn->errmsg);
  }

  db_select_all(db_conn);

  // Delete client with ID:
  if (db_delete_client(db_conn, 1) != SQL_OK)
  {
    fprintf(stderr, "Error db_delete_client: %s\n", db_conn->errmsg);
  }

  db_select_all(db_conn);

  // sleep some time before updating client last connect time
  sleep(2);

  client cl;
  strcpy(cl.c_ip, "192.168.1.1");

  if (db_update_client_last_connect(db_conn, &cl) != SQL_OK)
  {
    fprintf(stderr, "Error db_update_client_last_connect: %s\n", db_conn->errmsg);
  }

  db_select_all(db_conn);

  // clear the struct
  memset(&cl, 0, sizeof(client));
  if (db_select_client_by_address(db_conn, &cl, "192.168.1.1", strlen("192.168.1.1")) != SQL_OK)
  {
    fprintf(stderr, "Error db_select_client_by_address: %s\n", db_conn->errmsg);
  }
  else
  {
    printf("Found by address: #%d %s %s %s\n", cl.c_id, cl.c_ip, cl.c_sessid, cl.c_last_connect);
  }

  memset(&cl, 0, sizeof(client));
  if (db_select_client_by_address(db_conn, &cl, "192.168.0.1", strlen("192.168.0.1")) != SQL_OK)
  {
    fprintf(stderr, "Error db_select_client_by_address: %s\n", db_conn->errmsg);
  }
  else
  {
    printf("Found by address: #%d %s %s %s\n", cl.c_id, cl.c_ip, cl.c_sessid, cl.c_last_connect);
  }

  memset(&cl, 0, sizeof(client));
  if (db_select_client_by_sessid(db_conn, &cl, "123", strlen("123")) != SQL_OK)
  {
    fprintf(stderr, "Error db_select_client_by_sessid: %s\n", db_conn->errmsg);
  }
  else
  {
    printf("Found by sessid: #%d %s %s %s\n", cl.c_id, cl.c_ip, cl.c_sessid, cl.c_last_connect);
  }

  memset(&cl, 0, sizeof(client));
  if (db_select_client_by_sessid(db_conn, &cl, "12345679", strlen("12345679")) != SQL_OK)
  {
    fprintf(stderr, "Error db_select_client_by_sessid: %s\n", db_conn->errmsg);
  }
  else
  {
    printf("Found by sessid: #%d %s %s %s\n", cl.c_id, cl.c_ip, cl.c_sessid, cl.c_last_connect);
  }

  memset(&cl, 0, sizeof(client));
  if (db_select_client_by_sessid(db_conn, &cl, "123456791111", strlen("123456791111")) != SQL_OK)
  {
    fprintf(stderr, "Error db_select_client_by_sessid: %s\n", db_conn->errmsg);
  }
  else
  {
    printf("Found by sessid: #%d %s %s %s\n", cl.c_id, cl.c_ip, cl.c_sessid, cl.c_last_connect);
  }

  db_destroy(db_conn);
  return 0;
}
