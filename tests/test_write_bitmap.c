#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>
//https://stackoverflow.com/questions/14279242/read-bitmap-file-into-structure

typedef struct __attribute__((packed)) _BitmapHeader_t
{
  uint16_t type; // "BM"
  uint32_t size;
  uint32_t __reserved;
  uint32_t offset;
} BitmapHeader_t;

typedef struct __attribute__((packed)) _BitmapInfoHeader_t
{
  uint32_t size;            // size of this header (40 bytes)
  uint32_t width;           // bitmap width  in pixels
  uint32_t height;          // bitmap height in pixels
  uint16_t color_planes;    // must be 1
  uint16_t bits_per_pixel;  //Typical values are  1, 4, 8, 16, 24 and 32
  uint32_t compress_type;   // compression method being used 0 = none
  uint32_t raw_img_size;    // size of image raw data
  int32_t w_resolution;     // horizontal resolution (pixel per metre, signed int)
  int32_t h_resolution;     // vertical resolution (pixel per metre, signed int)
  uint32_t num_col_pallete; // 0 to 2^n
  uint32_t num_col_used;    // numbers of important colors used,
                            // or 0 when every color is important; generally ignored
} BitmapInfoHeader_t;

int main(int argc, char **argv)
{
  int w = 1001; /* Put here what ever width you want */
  int h = 1001; /* Put here what ever height you want */

  if (argc < 3)
  {
    fprintf(stderr, "usage %s: <width> <height>\n", argv[0]);
    return 1;
  }

  w = atoi(argv[1]);
  h = atoi(argv[2]);

  time_t timer;
  struct tm *tm_info;

  FILE *f;
  char file_name[256];
  unsigned char *img = NULL;
  int filesize = 54 + 3 * w * h;
  //w is your image width, h is image height, both int

  img = (unsigned char *)malloc(3 * w * h);
  memset(img, 0, sizeof(img));
  int x, y;

  for (y = 0; y < h; ++y)
  {
    for (x = 0; x < w; ++x)
    {
      if (x < y)
      {
        img[(y * w + x) * 3 + 0] = 0x00; // B
        img[(y * w + x) * 3 + 1] = 0x00; // G
        img[(y * w + x) * 3 + 2] = 0xff; // R
      }
      else
      {
        img[(y * w + x) * 3 + 0] = 0xff; // B
        img[(y * w + x) * 3 + 1] = 0x00; // G
        img[(y * w + x) * 3 + 2] = 0x00; // R
      }

      if (x < 5)
      {
        img[(y * w + x) * 3 + 0] = 0xff; // B
        img[(y * w + x) * 3 + 1] = 0x00; // G
        img[(y * w + x) * 3 + 2] = 0x00; // R
      }
    }
  }
  /*
    unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0};
    unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0};
    unsigned char bmppad[3] = {0,0,0};

    bmpfileheader[ 2] = (unsigned char)(filesize    );
    bmpfileheader[ 3] = (unsigned char)(filesize>> 8);
    bmpfileheader[ 4] = (unsigned char)(filesize>>16);
    bmpfileheader[ 5] = (unsigned char)(filesize>>24);

    bmpinfoheader[ 4] = (unsigned char)(       w    );
    bmpinfoheader[ 5] = (unsigned char)(       w>> 8);
    bmpinfoheader[ 6] = (unsigned char)(       w>>16);
    bmpinfoheader[ 7] = (unsigned char)(       w>>24);
    bmpinfoheader[ 8] = (unsigned char)(       h    );
    bmpinfoheader[ 9] = (unsigned char)(       h>> 8);
    bmpinfoheader[10] = (unsigned char)(       h>>16);
    bmpinfoheader[11] = (unsigned char)(       h>>24);
    */

  unsigned char bmppad[3] = {0, 0, 0};
  BitmapHeader_t bmp_hdr;
  BitmapInfoHeader_t bmp_info_hdr;

  memset(&bmp_hdr, 0, sizeof(BitmapHeader_t));
  memset(&bmp_info_hdr, 0, sizeof(BitmapInfoHeader_t));

  strncpy((char *)&bmp_hdr, "BM", 2);
  bmp_hdr.offset = sizeof(BitmapHeader_t) + sizeof(BitmapInfoHeader_t);

  bmp_info_hdr.size = sizeof(BitmapInfoHeader_t);
  bmp_info_hdr.width = w;
  bmp_info_hdr.height = h;
  bmp_info_hdr.color_planes = 1;
  bmp_info_hdr.bits_per_pixel = 3 * 8;

  time(&timer);
  tm_info = localtime(&timer);
  strftime(file_name, 256, "generated_%Y_%m_%d__%H_%M_%S.bmp", tm_info);

  f = fopen(file_name, "wb");
  //fwrite(bmpfileheader,1,14,f);
  //fwrite(bmpinfoheader,1,40,f);

  fwrite(&bmp_hdr, 1, sizeof(bmp_hdr), f);
  fwrite(&bmp_info_hdr, 1, sizeof(bmp_info_hdr), f);

  /*
    for (int i=0; i<h; i++)
    {
        printf("== %d\n", (4-(i*3)%4)%4);
    }
    */

  printf("width=%d, bytes_per_line=%d\n", w, w * 3);
  printf("need padding: %d between lines\n", (4 - (w * 3) % 4) % 4);

  /*
    for (int i=0; i<h; i++)
    {
            fwrite(img+(w*(h-i-1)*3),3,w,f);
            // n your case, each row must be a multiple of 4 bytes (32 bits).
            fwrite(bmppad,1,(4-(w*3)%4)%4,f);
    }
    */

  for (int i = 0; i < h; ++i)
  {
    //fwrite(img + ((i * w) * 3), 3, w, f);
    fwrite(img + (w * (h - i - 1) * 3), 3, w, f);
    // n your case, each row must be a multiple of 4 bytes (32 bits).
    fwrite(bmppad, 1, (4 - (w * 3) % 4) % 4, f);
  }

  fclose(f);

  printf("%ld + %ld = ?\n", sizeof(BitmapHeader_t), sizeof(BitmapInfoHeader_t));

  f = fopen("img.bmp", "rb");

  fread(&bmp_hdr, sizeof(BitmapHeader_t), 1, f);
  fread(&bmp_info_hdr, sizeof(BitmapInfoHeader_t), 1, f);

  write(1, &bmp_hdr.type, 2);
  write(1, "\n", 1);

  printf("file size = %d, offset = %d\n", bmp_hdr.size, bmp_hdr.offset);

  printf("img: w=%d, h=%d\n", bmp_info_hdr.width, bmp_info_hdr.height);
  printf("raw image size = %d\n", bmp_info_hdr.raw_img_size);

  fclose(f);
  return 0;
}
