#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>

#include "tv.h"

int main(int argc, char **argv)
{
  struct timeval start_tv, stop_tv, avg;
  struct tv_avg avg_acc = INIT_TV_AVG;
  int w = 0;
  double ms;

  while (1)
  {
    gettimeofday(&start_tv, NULL);

    // sleep 100, 200, 300, 400 miliseconds
    switch (w++ % 4)
    {
    case 0:
      usleep(100000);
      break;
    case 1:
      usleep(200000);
      break;
    case 2:
      usleep(300000);
      break;
    case 3:
      usleep(400000);
      break;
    }

    gettimeofday(&stop_tv, NULL);

    tvsub(&stop_tv, &start_tv);
    // printf("Took %ld.%ld\n", stop_tv.tv_sec, stop_tv.tv_usec);
    add_to_avg(&avg_acc, &stop_tv);
    get_avg(&avg_acc, &avg);
    ms = avg.tv_sec * 1000 + avg.tv_usec / 1000.0;
    printf("Aerage time: %f milliseconds\n", ms);
  }

  return 0;
}
