#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <unistd.h>
#include <signal.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <xcb/xcb.h>

#include "X11_utils.h"
#include "bitmap.h"
#include "tv.h"

static int caught_signal = 0;

void sighandler(int sig)
{
  caught_signal = 1;
}


/* Entry point */
int main(int argc, char **argv)
{
  xcb_connection_t *conn;
  xcb_screen_t *screen;
  xcb_get_image_cookie_t iq;
  xcb_get_image_reply_t *img;
  xcb_generic_error_t *e = NULL;

  signal(SIGHUP, sighandler);
  signal(SIGINT, sighandler);
  signal(SIGTERM, sighandler);

  conn = xcb_connect(NULL, NULL);

  if (xcb_connection_has_error(conn) > 0)
  {
    printf("Cannot open display\n");
    exit(EXIT_FAILURE);
  }

  screen = xcb_setup_roots_iterator(xcb_get_setup(conn)).data;

  UINT32 last_frame_counter = 0, frame_counter = 0;
  UINT64 last_fps_timestamp = get_time_micro();

  while (!caught_signal)
  {
    iq = xcb_get_image(conn,
                       XCB_IMAGE_FORMAT_Z_PIXMAP,
                       screen->root,
                       0, 0,
                       screen->width_in_pixels,
                       screen->height_in_pixels,
                       ~0);

    img = xcb_get_image_reply(conn, iq, &e);

    if (e)
    {
      fprintf("Cannot get the image data "
              "event_error: response_type:%u error_code:%u "
              "sequence:%u resource_id:%u minor_code:%u major_code:%u.\n",
              e->response_type, e->error_code,
              e->sequence, e->resource_id, e->minor_code, e->major_code);
      break;
    }
    
    ++frame_counter;

    INT64 timestamp = get_time_micro();
    INT64 time = timestamp - last_fps_timestamp;
    if (time > 500000)
    {
      UINT32 frames = frame_counter - last_frame_counter;
      last_fps_timestamp = timestamp;
      last_frame_counter = frame_counter;
      printf("FPS: %f\n", (double)frames / ((double)time * 1.0e-6));
    }

    free(img);
  }

  xcb_disconnect(conn);
  return 0;
}