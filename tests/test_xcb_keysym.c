#include <stdlib.h>
#include <stdio.h>
#include <xcb/xcb.h>

void xcb_show_keyboard_mapping(xcb_connection_t *connection, const xcb_setup_t *setup)
{
  xcb_get_keyboard_mapping_reply_t *keyboard_mapping = xcb_get_keyboard_mapping_reply(connection, xcb_get_keyboard_mapping(connection, setup->min_keycode, setup->max_keycode - setup->min_keycode + 1), NULL);
  int nkeycodes = keyboard_mapping->length / keyboard_mapping->keysyms_per_keycode;
  int nkeysyms = keyboard_mapping->length;
  xcb_keysym_t *keysyms = (xcb_keysym_t *)(keyboard_mapping + 1); // `xcb_keycode_t` is just a `typedef u8`, and `xcb_keysym_t` is just a `typedef u32`
  printf("nkeycodes %u  nkeysyms %u  keysyms_per_keycode %u\n\n", nkeycodes, nkeysyms, keyboard_mapping->keysyms_per_keycode);

  for (int keycode_idx = 0; keycode_idx < nkeycodes; ++keycode_idx)
  {
    printf("keycode %3u ", setup->min_keycode + keycode_idx);
    for (int keysym_idx = 0; keysym_idx < keyboard_mapping->keysyms_per_keycode; ++keysym_idx)
    {
      printf(" %8x", keysyms[keysym_idx + keycode_idx * keyboard_mapping->keysyms_per_keycode]);
    }
    putchar('\n');
  }

  free(keyboard_mapping);
}

int main()
{
  xcb_connection_t *connection = xcb_connect(NULL, NULL);
  const xcb_setup_t *setup = xcb_get_setup(connection);
  xcb_show_keyboard_mapping(connection, setup);
  xcb_disconnect(connection);
}