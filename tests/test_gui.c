#include <gtk/gtk.h>
#include <gio/gio.h>
#include <stdio.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <netinet/tcp.h>
#include <arpa/inet.h>

#include "proxy_messages.h"
#include "vnc_server.h"
#include "gtk_vnc_viewer.h"

#define SUPPRESS_UNUSED_FUN __attribute__((unused))
#define SUPPRESS_UNUSED_VAR(__var) (void)__var

//#define PROXY_ADDRESS "192.168.1.6"
#define PROXY_ADDRESS "127.0.0.1"
#define PROXY_PORT 7777

#define WIN_WIDTH 300
#define WIN_HEIGHT 400

#define MSG_ERROR -1
#define MSG_SUCCESS 0
#define MSG_LOADING 1

typedef struct __MyGUI
{
  // Box
  GtkWidget *hbox, *btnbox;
  GtkWidget *window, *button, *label;
  GtkWidget *grid;

  GtkWidget *id_label, *pass_label;
  GtkWidget *id_field, *pass_field;

  GtkWidget *top_inner_hbox;
  GtkWidget *sessid_field;
  GtkWidget *connect_btn;

  GtkWidget *hseparator;

  GtkWidget *status_image, *status_label;

  int set_as_host;
  MyVncViewerGUI vnc_viewer;
  VNCServerPtr vnc_server;

  // last message
  int msg_type;
  char *msg;

  // Box
  GtkWidget *root_vbox, *top_hbox, *bottom_hbox, *status_hbox;

  GdkPixbuf *pbuf;

  // g socket connection
  GError *gerror;
  GSocketAddress *gaddr;
  GCancellable *gcancel;
  GSocket *gsock;
  GSocketConnection *gsock_conn;
  GIOChannel *giochannel;
  GSource *gsource;
} MyGUI;

static void open_error_dialog(MyGUI *gui, const char *msg);
static void open_info_dialog(MyGUI *gui, const char *msg);
static void open_dialog(MyGUI *gui, const char *msg);
static void set_status_message(MyGUI *gui, const char *msg, int msgtype);

gboolean recv_function(GIOChannel *gio,
                       GIOCondition condition,
                       gpointer data);
static void gsource_destroy(gpointer data);

static void print_gsocket_connection_end_points(GSocketConnection *g_sock_conn);

static void
on_simple_dialog_response(GtkDialog *dialog,
                          gint response_id,
                          gpointer data)
{
  if (dialog == NULL)
    return;

  /*This will cause the dialog to be destroyed*/
  gtk_widget_destroy(GTK_WIDGET(dialog));
}

void print_ginet_socket_end_points(GSocket *gsock)
{
  GError *gerror = NULL;
  GSocketAddress *gsock_local_addr = NULL;
  GSocketAddress *gsock_remote_addr = NULL;
  GInetAddress *ginet_local_addr = NULL;
  GInetAddress *ginet_remote_addr = NULL;
  int ginet_local_port, ginet_remote_port;
  gchar *local_addr_str = NULL, *remote_addr_str = NULL;

  gsock_local_addr = g_socket_get_local_address(gsock, &gerror);
  if (gerror)
  {
    fprintf(stderr, "Error: %s\n", gerror->message);
    g_clear_error(&gerror);
    return;
  }

  gsock_remote_addr = g_socket_get_remote_address(gsock, &gerror);
  if (gerror)
  {
    fprintf(stderr, "Error: %s\n", gerror->message);
    g_clear_error(&gerror);
    return;
  }

  ginet_local_addr = g_inet_socket_address_get_address(gsock_local_addr);
  ginet_local_port = g_inet_socket_address_get_port(gsock_local_addr);
  ginet_remote_addr = g_inet_socket_address_get_address(gsock_remote_addr);
  ginet_remote_port = g_inet_socket_address_get_port(gsock_remote_addr);

  local_addr_str = g_inet_address_to_string(ginet_local_addr);
  remote_addr_str = g_inet_address_to_string(ginet_remote_addr);

  printf("GSocketConnection: %s:%d->%s:%d\n",
         local_addr_str, ginet_local_port, remote_addr_str, ginet_remote_port);

  g_free(local_addr_str);
  g_free(remote_addr_str);
  g_object_unref(gsock_local_addr);
  g_object_unref(gsock_remote_addr);
}

static void print_gsocket_connection_end_points(GSocketConnection *g_sock_conn)
{
  GSocket *gsock = NULL;
  if (!G_IS_SOCKET_CONNECTION(g_sock_conn))
    return;

  gsock = g_socket_connection_get_socket(g_sock_conn);
  if (!G_IS_SOCKET(gsock))
    return;

  switch (g_socket_get_family(gsock))
  {
  case G_SOCKET_FAMILY_IPV4:
  case G_SOCKET_FAMILY_IPV6:
    print_ginet_socket_end_points(gsock);
    break;
  case G_SOCKET_FAMILY_UNIX:
    // TODO
    break;
  case G_SOCKET_FAMILY_INVALID:
  default:
    break;
  }
}

static void *iter2(void *data)
{
  MyGUI *gui = (MyGUI *)data;

  for (;;)
  {
    runVNCServer(gui->vnc_server);
    usleep(30000);
  }
}

static VNCServerPtr vncServer = NULL;

static void clientgone(rfbClientPtr cl)
{
  free(cl->clientData);
  cl->clientData = NULL;
}

static enum rfbNewClientAction newclient(rfbClientPtr cl)
{
  cl->clientData = (void *)calloc(sizeof(client_data), 1);
  client_data *data = (client_data *)cl->clientData;
  data->server = vncServer;
  cl->clientGoneHook = clientgone;
  return RFB_CLIENT_ACCEPT;
}

void print_sock4_name(int sock)
{
  socklen_t addrlen;
  struct sockaddr_in inaddr;
  if (sock == -1)
    return;

  addrlen = sizeof(inaddr);
  if (getsockname(sock, (struct sockaddr *)&inaddr, &addrlen) == -1)
  {
    perror("getsockname");
    return;
  }

  printf("socket[%d] bound on %s:%d\n",
         sock, inet_ntoa(inaddr.sin_addr), ntohs(inaddr.sin_port));
}

void print_sock4_end_points(int sock)
{
  socklen_t addrlen;
  struct sockaddr_in local_inaddr;
  struct sockaddr_in remote_inaddr;
  if (sock == -1)
    return;

  addrlen = sizeof(local_inaddr);
  if (getsockname(sock, (struct sockaddr *)&local_inaddr, &addrlen) == -1)
  {
    perror("getsockname");
    return;
  }

  addrlen = sizeof(remote_inaddr);
  if (getpeername(sock, (struct sockaddr *)&remote_inaddr, &addrlen) == -1)
  {
    perror("getpeername");
    return;
  }

  printf("socket[%d] bound on %s:%d connected to %s:%d\n",
         sock,
         inet_ntoa(local_inaddr.sin_addr), ntohs(local_inaddr.sin_port),
         inet_ntoa(remote_inaddr.sin_addr), ntohs(remote_inaddr.sin_port));
}

int socket_connect(char *address, int port)
{
  int sock;
  int flags, error;
  socklen_t len;
  int retval, optval = 1;
  fd_set rset, wset;
  struct sockaddr_in sock_addr;
  struct timeval tv;

  assert(address);
  assert(port > 0 && port < 65535);

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    goto err;
  // if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) < 0)
  //   goto err;
  flags = fcntl(sock, F_GETFL, 0);
  if (fcntl(sock, F_SETFL, flags | O_NONBLOCK) < 0)
    goto err;

  memset(&sock_addr, 0, sizeof(sock_addr));
  sock_addr.sin_family = AF_INET;
  sock_addr.sin_port = htons(port);

  if (address == NULL)
    sock_addr.sin_addr.s_addr = INADDR_ANY;
  else
    sock_addr.sin_addr.s_addr = inet_addr(address);

  printf("Trying connect to: %s ( %s ) on port: %d...\r\n",
         address, inet_ntoa(sock_addr.sin_addr), port);

  retval = connect(sock, (struct sockaddr *)(&sock_addr), sizeof(sock_addr));
  if (retval < 0)
    if (errno != EINPROGRESS)
      goto err;

  if (retval == 0)
    return sock; /* connect completed immediately */

  FD_ZERO(&wset);
  FD_SET(sock, &wset);

  tv.tv_sec = 5;
  tv.tv_usec = 0;

  retval = select(sock + 1, NULL, &wset, NULL, &tv);
  if (retval < 0)
    goto err;
  if (retval == 0)
  {
    /* timeout */
    // errno = ETIMEDOUT;
    goto err;
  }

  if (FD_ISSET(sock, &wset))
  {
    len = sizeof(error);
    if (getsockopt(sock, SOL_SOCKET, SO_ERROR, &error, &len) < 0)
      goto err; /* Solaris pending error */

    if (error)
    {
      errno = error;
      perror("connect");
      goto err;
    }

    // Socket connected
    return sock;
  }

err:
  if (sock != -1)
    close(sock);
  return -1;
}

static void
on_response(GtkDialog *dialog,
            gint response_id,
            gpointer data)
{
  MyGUI *gui = (MyGUI *)data;

  ProxyAskForConnectMsg msg;
  rfbProtocolVersionMsg pv;
  gsize bytes_written = 0;
  GIOStatus ret;

  if (response_id != GTK_RESPONSE_ACCEPT)
  {
    g_print("'CANCEL' was pressed...\r\n");
    msg.type = DECLINE_FOR_CONNECT;

    ret = g_io_channel_write_chars(gui->giochannel,
                                   (const gchar *)&msg, sz_ProxyAskForConnectMsg,
                                   &bytes_written, &gui->gerror);
    g_io_channel_flush(gui->giochannel, NULL);

    /*This will cause the dialog to be destroyed*/
    gtk_widget_destroy(GTK_WIDGET(dialog));
    return;
  }

  g_print("'OK' was pressed...\r\n");
  msg.type = APPROVE_FOR_CONNECT;
  ret = g_io_channel_write_chars(gui->giochannel,
                                 (const gchar *)&msg, sz_ProxyAskForConnectMsg,
                                 &bytes_written, &gui->gerror);
  g_io_channel_flush(gui->giochannel, NULL);

  gui->set_as_host = TRUE;

  /*This will cause the dialog to be destroyed*/
  gtk_widget_destroy(GTK_WIDGET(dialog));
}

static void
open_dialog(MyGUI *gui, const char *msg)
{
  GtkWidget *dialog, *label;

  if (gui == NULL || gui->window == NULL)
    return;

  dialog = gtk_dialog_new_with_buttons("Connection request",
                                       GTK_WINDOW(gui->window),
                                       GTK_DIALOG_MODAL,
                                       "Accept", GTK_RESPONSE_ACCEPT,
                                       "Decline", GTK_RESPONSE_REJECT,
                                       NULL);

  g_signal_connect(dialog, "response", G_CALLBACK(on_response), (gpointer)gui);

  label = gtk_label_new(msg);
  gtk_box_pack_start(GTK_BOX(gtk_dialog_get_content_area(GTK_DIALOG(dialog))), label, 0, 0, 0);
  gtk_widget_show_all(dialog);
}

static void open_error_dialog(MyGUI *gui, const char *msg)
{
  GtkWidget *dialog;

  if (gui == NULL || gui->window == NULL)
    return;

  dialog = gtk_message_dialog_new(GTK_WINDOW(gui->window), 0,
                                  GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, msg);
  g_signal_connect(dialog, "response",
                   G_CALLBACK(on_simple_dialog_response), gui);
  gtk_widget_show_all(dialog);
}

static void open_info_dialog(MyGUI *gui, const char *msg)
{
  GtkWidget *dialog;

  if (gui == NULL || gui->window == NULL)
    return;

  dialog = gtk_message_dialog_new(GTK_WINDOW(gui->window), 0,
                                  GTK_MESSAGE_INFO, GTK_BUTTONS_OK, msg);
  g_signal_connect(dialog, "response",
                   G_CALLBACK(on_simple_dialog_response), gui);
  gtk_widget_show_all(dialog);
}

SUPPRESS_UNUSED_FUN static gboolean
connection_check_timeout(gpointer data)
{
  MyGUI *gui = NULL;

  gui = (MyGUI *)data;
  if (gui == NULL)
    return G_SOURCE_REMOVE;

  if (!G_IS_SOCKET(gui->gsock) ||
      !G_IS_TCP_CONNECTION(gui->gsock_conn) ||
      !g_socket_is_connected(gui->gsock) ||
      !g_socket_connection_is_connected(gui->gsock_conn))
  {
    // Not connected
    gui_connect(gui);
    return G_SOURCE_CONTINUE;
  }

  // print_gsocket_connection_end_points(gui->gsock_conn);
  return G_SOURCE_CONTINUE; /* or G_SOURCE_REMOVE when you want to stop */
}

/* Our usual callback function */
static void on_press_connect_button(GtkWidget *widget,
                                    gpointer data)
{
  MyGUI *gui = (MyGUI *)data;
  const gchar *sessid = gtk_entry_get_text(gui->sessid_field);

  if (sessid == NULL || strlen(sessid) == 0 || strlen(sessid) > SESSID_LEN)
  {
    g_print("Enter a valid sessid\n");
    return;
  }

  g_print("Connect to: %s\n", sessid);

  ProxyReqConnMsg rcm;
  rcm.type = REQUEST_CONNECTION;
  strcpy(rcm.sessid, sessid);
  strcpy(rcm.passwd, "Test");

  if (gui->gerror)
  {
    fprintf(stderr, "Error: %s\n", gui->gerror->message);
    g_clear_error(&gui->gerror);
  }

  gsize bytes_written = 0;
  GIOStatus ret = g_io_channel_write_chars(gui->giochannel,
                                           (const gchar *)&rcm, sz_ProxyReqConnMsg, &bytes_written, &gui->gerror);
  g_io_channel_flush(gui->giochannel, NULL);
  gui->set_as_host = FALSE;
}

static void set_status_message(MyGUI *gui, const char *msg, int msgtype)
{
  gui->msg_type = MSG_ERROR;
  gui->msg = strdup(msg);

  // GdkColor color;
  // color.red = 0xffff;
  // color.green = 0x0;
  // color.blue = 0x0;
  // gtk_widget_modify_fg(GTK_WIDGET(gui->status_label), GTK_STATE_NORMAL, &color);

  int x, y, rx, ry;
  char *pixels, *pix;
  int width, height;
  int radius;

  const GdkPixbuf *pbuf = gtk_image_get_pixbuf(gui->status_image);

  pixels = gdk_pixbuf_get_pixels(pbuf);

  width = gdk_pixbuf_get_width(pbuf);
  height = gdk_pixbuf_get_height(pbuf);
  radius = (width / 2) - 1;
  for (y = -radius; y <= radius; ++y)
    for (x = -radius; x <= radius; ++x)
      if (x * x + y * y < radius * radius)
      {
        rx = radius - x;
        ry = radius - y;

        pix = (pixels + ry * width * 4 + rx * 4);
        switch (msgtype)
        {
        case MSG_ERROR:
          pix[0] = 0xff; // r
          pix[1] = 0x00; // g
          pix[2] = 0x00; // b
          pix[3] = 0xff; // a
          break;
        case MSG_SUCCESS:
          pix[0] = 0x00; // r
          pix[1] = 0xff; // g
          pix[2] = 0x00; // b
          pix[3] = 0xff; // a
          break;
        case MSG_LOADING:
          pix[0] = 0xff; // r
          pix[1] = 0xff; // g
          pix[2] = 0x00; // b
          pix[3] = 0xff; // a
          break;
        }
        if (x == 0 || y == 0 || rx == radius || ry == radius)
          pix[3] = 0x00;
      }

  gtk_image_set_from_pixbuf(gui->status_image, pbuf);
  gtk_label_set_text(GTK_LABEL(gui->status_label), msg);
}

static void gui_init(MyGUI *gui)
{
  g_assert((gui->window = gtk_window_new(GTK_WINDOW_TOPLEVEL)));

  g_assert((gui->root_vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0)));
  g_assert((gui->top_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0)));
  g_assert((gui->top_inner_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0)));
  g_assert((gui->bottom_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0)));
  g_assert((gui->status_hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0)));
  g_assert((gui->grid = gtk_grid_new()));
  g_assert((gui->hseparator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL)));
  g_assert((gui->id_label = gtk_label_new("Your ID")));
  g_assert((gui->pass_label = gtk_label_new("Password")));
  g_assert((gui->id_field = gtk_label_new("-")));
  g_assert((gui->pass_field = gtk_label_new("-")));
  g_assert((gui->status_label = gtk_label_new("Loading...")));
  g_assert((gui->sessid_field = gtk_entry_new()));
  g_assert((gui->connect_btn = gtk_button_new_with_label("Connect")));

  g_signal_connect(gui->window, "destroy",
                   G_CALLBACK(gtk_main_quit), (gpointer)NULL);
  g_signal_connect(gui->connect_btn, "clicked",
                   G_CALLBACK(on_press_connect_button), (gpointer)gui);

  gui->vnc_server = NULL;

  GdkColor color;
  color.red = 0xaaa;
  color.green = 0xaaaa;
  color.blue = 0xffff;
  gtk_widget_modify_bg(GTK_WIDGET(gui->top_hbox), GTK_STATE_NORMAL, &color);

  color.red = 0xc0c0;
  color.green = 0xc0c0;
  color.blue = 0xc0c0;
  gtk_widget_modify_bg(GTK_WIDGET(gui->bottom_hbox), GTK_STATE_NORMAL, &color);

  color.red = 0xfafa;
  color.green = 0xfafa;
  color.blue = 0xfafa;
  gtk_widget_modify_bg(GTK_WIDGET(gui->status_hbox), GTK_STATE_NORMAL, &color);

  PangoAttrList *attrlist;
  PangoAttribute *attr;
  PangoFontDescription *df;

  attrlist = pango_attr_list_new();
  df = pango_font_description_new();

  pango_font_description_set_family(df, "Arial Rounded MT");
  pango_font_description_set_size(df, 10000);
  pango_font_description_set_weight(df, PANGO_WEIGHT_BOLD);
  pango_font_description_set_gravity(df, PANGO_GRAVITY_SOUTH);

  attr = pango_attr_font_desc_new(df);
  pango_attr_list_insert(attrlist, attr);
  pango_font_description_free(df);

  attr = pango_attr_foreground_new(0x4000, 0x5000, 0x2000);
  pango_attr_list_insert(attrlist, attr);

  // And finally, give the GtkLabel our attribute list.
  gtk_label_set_attributes(GTK_LABEL(gui->pass_label), attrlist);
  gtk_label_set_attributes(GTK_LABEL(gui->id_label), attrlist);

  pango_attr_list_unref(attrlist);

  attrlist = pango_attr_list_new();
  df = pango_font_description_new();

  pango_font_description_set_family(df, "Arial Rounded MT");
  pango_font_description_set_size(df, 15000);
  pango_font_description_set_weight(df, PANGO_WEIGHT_BOLD);
  pango_font_description_set_gravity(df, PANGO_GRAVITY_SOUTH);
  attr = pango_attr_font_desc_new(df);
  pango_font_description_free(df);
  pango_attr_list_insert(attrlist, attr);

  // And finally, give the GtkLabel our attribute list.
  gtk_label_set_attributes(GTK_LABEL(gui->pass_field), attrlist);
  gtk_label_set_attributes(GTK_LABEL(gui->id_field), attrlist);

  pango_attr_list_unref(attrlist);

  gtk_label_set_selectable(GTK_LABEL(gui->pass_field), TRUE);
  gtk_label_set_selectable(GTK_LABEL(gui->id_field), TRUE);

  gtk_label_set_xalign(GTK_LABEL(gui->status_label), 0.1);
  // gtk_misc_set_alignment(GTK_MISC(gui->status_label), 0.0, 0.0);

  gtk_grid_attach(GTK_GRID(gui->grid), gui->id_label, 0, 0, 1, 1);
  gtk_grid_attach(GTK_GRID(gui->grid), gui->id_field, 0, 1, 1, 1);
  gtk_grid_attach(GTK_GRID(gui->grid), gui->hseparator, 0, 2, 1, 1);
  gtk_grid_attach(GTK_GRID(gui->grid), gui->pass_label, 0, 3, 1, 1);
  gtk_grid_attach(GTK_GRID(gui->grid), gui->pass_field, 0, 4, 1, 1);

  gtk_grid_set_row_spacing(GTK_GRID(gui->grid), 10);
  gtk_grid_set_column_spacing(GTK_GRID(gui->grid), 10);
  gtk_widget_set_margin_top(GTK_WIDGET(gui->grid), 30);

  gui->pbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, 1, 8, 16, 16);
  gui->status_image = gtk_image_new_from_pixbuf(gui->pbuf);

  guchar *pixels = gdk_pixbuf_get_pixels(gui->pbuf);
  memset(pixels, 0x0, 16 * 16 * 4);

  GtkWidget *gr = gtk_grid_new();
  gtk_grid_attach(GTK_GRID(gr), gui->sessid_field, 0, 0, 1, 1);
  gtk_grid_attach(GTK_GRID(gr), gui->connect_btn, 1, 0, 2, 2);
  gtk_widget_set_margin_end(GTK_WIDGET(gr), 5);
  gtk_widget_set_margin_top(GTK_WIDGET(gr), 5);

  gtk_box_pack_end(GTK_BOX(gui->top_hbox), GTK_WIDGET(gr), 0, 1, 0);

  GtkWidget *grd = gtk_grid_new();
  gtk_grid_attach(GTK_GRID(grd), gui->status_image, 0, 0, 1, 1);
  gtk_grid_attach(GTK_GRID(grd), gui->status_label, 1, 0, 1, 1);
  gtk_grid_set_column_spacing(GTK_GRID(grd), 5);
  gtk_widget_set_margin_start(GTK_WIDGET(grd), 5);

  gtk_box_pack_start(GTK_BOX(gui->bottom_hbox), GTK_WIDGET(gui->grid), 1, 0, 0);
  gtk_box_pack_start(GTK_BOX(gui->status_hbox), GTK_WIDGET(grd), 1, 1, 0);

  gtk_widget_set_size_request(GTK_WIDGET(gui->top_hbox), WIN_WIDTH, 40);
  gtk_widget_set_size_request(GTK_WIDGET(gui->bottom_hbox), WIN_WIDTH, WIN_HEIGHT - 50);
  gtk_widget_set_size_request(GTK_WIDGET(gui->status_hbox), WIN_WIDTH, 20);

  gtk_box_pack_start(GTK_BOX(gui->root_vbox), GTK_WIDGET(gui->top_hbox), 0, 1, 0);
  gtk_box_pack_start(GTK_BOX(gui->root_vbox), GTK_WIDGET(gui->bottom_hbox), 1, 1, 0);
  gtk_box_pack_end(GTK_BOX(gui->root_vbox), GTK_WIDGET(gui->status_hbox), 0, 1, 0);

  gtk_container_add(GTK_CONTAINER(gui->window), GTK_WIDGET(gui->root_vbox));

  gtk_widget_set_size_request(gui->window, WIN_WIDTH, WIN_HEIGHT);
  gtk_widget_show_all(gui->window);

  g_timeout_add(5 * 1000 /* milliseconds */, connection_check_timeout, gui);
}

gboolean recv_function(GIOChannel *gio,
                       GIOCondition condition,
                       gpointer data)
{
  MyGUI *gui = (MyGUI *)data;
  ProxyMsg msg;
  GIOStatus ret;
  GError *err = NULL;
  gchar buf[1024];
  gsize len;

  if (condition & G_IO_HUP)
  {
    fprintf(stderr, "Read end of pipe died!\n");
    // Drop last reference on connection
    g_object_unref(gui->gsock_conn);
    // Remove the event source
    return G_SOURCE_REMOVE;
  }

  printf("##########################\n");

  ret = g_io_channel_read_chars(gio, (gchar *)&msg, 1, &len, &err);
  // ret = g_io_channel_read_chars(gio, (gchar *)&msg, sizeof(msg), &len, &err);
  if (ret == G_IO_STATUS_ERROR)
  {
    fprintf(stderr, "Error reading: %s\n", err->message);
    // Drop last reference on connection
    g_object_unref(gui->gsock_conn);
    // Remove the event source
    return G_SOURCE_REMOVE;
  }
  else if (ret == G_IO_STATUS_EOF)
  {
    return G_SOURCE_REMOVE;
  }

  // g_print("g_io_channel_read_chars ret=%d, bytes_readed=%d(need 1)\n", ret, len);

  switch (msg.type)
  {
  case INIT_MESSAGE:
    ret = g_io_channel_read_chars(gio, ((char *)&msg) + 1, sz_ProxyInitMsg - 1, &len, &err);
    if (ret == G_IO_STATUS_ERROR)
    {
      fprintf(stderr, "Error reading: %s\n", err->message);
      // Drop last reference on connection
      g_object_unref(gui->gsock_conn);
      // Remove the event source
      return G_SOURCE_REMOVE;
    }
    else if (ret == G_IO_STATUS_EOF)
    {
      return G_SOURCE_REMOVE;
    }
    ProxyInitMsg *pm = (ProxyInitMsg *)&msg;
    printf("%s %s\n", pm->sessid, pm->passwd);
    gtk_label_set_text(GTK_LABEL(gui->id_field), pm->sessid);
    gtk_label_set_text(GTK_LABEL(gui->pass_field), pm->passwd);
    gtk_widget_set_sensitive(GTK_WIDGET(gui->connect_btn), TRUE);
    break;
  case ASK_FOR_CONNECT:
    open_dialog(gui, "Accept connection ?");
    break;
  case INIT_CONNECTION:
    ret = g_io_channel_read_chars(gio, ((char *)&msg) + 1,
                                  sz_ProxyInitConnectionMsg - 1, &len,
                                  &gui->gerror);
    if (ret == G_IO_STATUS_ERROR)
    {
      fprintf(stderr, "Error reading: %s\n", err->message);
      // Drop last reference on connection
      g_object_unref(gui->gsock_conn);
      // Remove the event source
      return G_SOURCE_REMOVE;
    }
    else if (ret == G_IO_STATUS_EOF)
    {
      return G_SOURCE_REMOVE;
    }

    ProxyInitConnectionMsg *pafcm = (ProxyInitConnectionMsg *)((char *)&msg);
    // printf("[CLIENT]I must connect to: %d\n", pafcm->port);

    int vnc_sock = socket_connect(SERVER_IP, pafcm->port);
    if (vnc_sock == -1)
    {
      perror("sock: ");
      break;
    }

    if (gui->set_as_host)
    {
      printf("\033[32m HOST Socket connected!\033[00m \n");
      print_sock4_end_points(vnc_sock);

      if (gui->vnc_server != NULL)
      {
        printf(COLOR_RED "Already in vnc session\n" COLOR_NONE);

        rfbClientPtr cl = NULL;
        cl = rfbNewClient(gui->vnc_server->rfbPtr, vnc_sock);
        if (cl && !cl->onHold)
          rfbStartOnHoldClient(cl);
      }
      else
      {
        vncServer = getVNCServer();
        gui->vnc_server = vncServer;
        initVNCServer(gui->vnc_server, NULL, NULL);
        gui->vnc_server->rfbPtr->newClientHook = newclient;

        setServerSock(gui->vnc_server, vnc_sock);

        pthread_t th;
        pthread_create(&th, NULL, iter2, (void *)gui);
      }
    }
    else
    {
      printf("\033[32m CLIENT Socket connected!\033[00m \n");
      print_sock4_end_points(vnc_sock);

      vnc_viewer_gui_init(&gui->vnc_viewer);
      vnc_viewer_gui_set_socket(&gui->vnc_viewer, vnc_sock);
      vnc_viewer_gui_main(&gui->vnc_viewer);
      // vnc_viewer_gui_destroy(&gui->vnc_viewer);
    }

    set_status_message(gui, "Connected to host", MSG_SUCCESS);
    break;
  case DECLINE_FOR_CONNECT:
    set_status_message(gui, "Connection failed!", MSG_ERROR);
    open_error_dialog(gui, "Failed to connect!");
    break;
  case REQUEST_CONNECTION_FAIL:
    set_status_message(gui, "Connection failed!", MSG_ERROR);
    open_error_dialog(gui, "Failed to connect!");
    break;
  default:
    /* Unknown message */
    break;
  }

  //g_io_channel_flush(gio, NULL);
  return G_SOURCE_CONTINUE;
}

static void on_gui_connection_lost(MyGUI *gui)
{
  if (gui == NULL)
    return;

  gtk_label_set_text(GTK_LABEL(gui->id_field), "-");
  gtk_label_set_text(GTK_LABEL(gui->pass_field), "-");
  gtk_widget_set_sensitive(GTK_WIDGET(gui->connect_btn), FALSE);
  set_status_message(gui, "Disconnected", MSG_ERROR);
}

static void gsource_destroy(gpointer data)
{
  MyGUI *gui = (MyGUI *)data;

  if (gui == NULL)
    return;

  g_socket_close(gui->gsock, NULL);
  g_socket_shutdown(gui->gsock, TRUE, TRUE, NULL);
  g_object_unref(gui->gsock);
  // gui->gsock = NULL;

  // g_io_channel_shutdown(gui->giochannel, TRUE, &gui->gerror);
  // g_io_channel_unref(gui->giochannel);

  /* We did an attach() with the GSource, so we need to
     * destroy() it */
  g_source_destroy(gui->gsource);

  /* We need to unref() the GSource to end the last reference
     * we got */
  g_source_unref(gui->gsource);
  printf("GSource destroyed!\n");

  on_gui_connection_lost(gui);
}

static void fnc_g_socket_connect_async_callback(GObject /*GSocketConnection*/ *obj,
                                                GAsyncResult *res,
                                                gpointer data)
{
  MyGUI *gui = NULL;
  GSocket *gsock = NULL;
  GSocketConnection *conn;
  GError *gerr = NULL;

  gui = (MyGUI *)data;
  if (gui == NULL)
    return;

  conn = (GSocketConnection *)obj;

  if (!g_socket_connection_connect_finish(conn, res, &gerr))
  {
    set_status_message(gui, "Connection failed", MSG_ERROR);
    fprintf(stderr, "Connection failed: %s\n", gerr->message);
    g_object_unref(gui->gsock);
    g_object_unref(gui->gsock_conn);
    return;
  }

  // Socket is connected
  set_status_message(gui, "Successfully conected", MSG_SUCCESS);

  gsock = g_socket_connection_get_socket(conn);

  int fd = g_socket_get_fd(gsock);
  gui->giochannel = g_io_channel_unix_new(fd);
  if (gui->giochannel == NULL)
  {
    fprintf(stderr, "error: g_io_channel_unix_new\n");
    return;
  }

  g_io_channel_set_encoding(gui->giochannel, NULL /*"UTF-8"*/, &gui->gerror);
  if (gui->gerror)
  {
    fprintf(stderr, "g_io_channel_set_encoding: %s\n", gui->gerror->message);
    g_clear_error(&gui->gerror);
  }

  // source = g_io_add_watch(channel, G_IO_IN,
  //                               (GIOFunc)recv_function, gui);
  // g_io_channel_unref(channel);
  // g_source_remove(source);

  gui->gsource = g_io_create_watch(gui->giochannel, G_IO_IN | G_IO_HUP | G_IO_ERR);
  g_source_set_callback(gui->gsource, (GIOFunc)recv_function,
                        (gpointer)gui, (GDestroyNotify)gsource_destroy);
  g_source_attach(gui->gsource, NULL);

  // g_io_channel_unref(gui->giochannel);
  // gui->giochannel = NULL;

  printf("gsource TAG: %d\n", g_source_get_id(gui->gsource));
  // g_source_remove(g_source_get_id(gui->gsource));
}

void gui_connect(MyGUI *gui)
{
  gui->gerror = NULL;
  gui->gsock = g_socket_new(G_SOCKET_FAMILY_IPV4,
                            G_SOCKET_TYPE_STREAM,
                            G_SOCKET_PROTOCOL_DEFAULT,
                            &gui->gerror);
  if (!gui->gsock)
  {
    fprintf(stderr, "gui_connect: %s\n", gui->gerror->message);
    g_clear_error(&gui->gerror);
    return;
  }

  gui->gsock_conn = g_socket_connection_factory_create_connection(gui->gsock);

  printf("GSocketConnection: [%p]\n\n", gui->gsock_conn);

  gui->gcancel = g_cancellable_new();
  gui->gaddr = g_inet_socket_address_new_from_string(PROXY_ADDRESS, PROXY_PORT);

  printf("Socket fd: %d\n", g_socket_get_fd(gui->gsock));

  g_socket_connection_connect_async(gui->gsock_conn,
                                    gui->gaddr,
                                    gui->gcancel,
                                    fnc_g_socket_connect_async_callback,
                                    (gpointer)gui);
}

void gui_destroy(MyGUI *gui)
{
  if (gui->gsock)
  {
    if (!g_socket_is_closed(gui->gsock))
    {
      if (!g_socket_close(gui->gsock, &gui->gerror))
      {
        fprintf(stderr, "gui_destroy error: %s\n", gui->gerror->message);
        g_clear_error(&gui->gerror);
      }
    }
  }
}

int main(int argc, char **argv)
{
  MyGUI gui;

  gtk_init(&argc, &argv);

  gui_init(&gui);
  gui_connect(&gui);

  gtk_main();

  gui_destroy(&gui);
  return 0;
}
