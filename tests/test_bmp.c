#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include "bitmap.h"

static BOOL write_X11Image_bmp(const char *filename, XImage *img)
{
  BOOL ret;
  UINT32 pix_value;
  UINT32 *pix;
  INT32 y, x;
  Pixel_t *pixel_array = NULL;
  Pixel_t *pixel = NULL;

  assert(filename);
  assert(img);

  if (img->format != ZPixmap)
  {
    fprintf(stderr, "XImage format is not ZPixmap\n");
    return FALSE;
  }

  pixel_array = (Pixel_t *)malloc(img->width * img->height * sizeof(Pixel_t));
  if (!pixel_array)
  {
    fprintf(stderr, "malloc not enough memory\n");
    return FALSE;
  }

  pixel = pixel_array;

  /* collect separate RGB values to pixel_array */
  pix = (UINT32 *)img->data;

  for (y = 0; y < img->height; y++)
  {
    for (x = 0; x < img->width; x++)
    {
      pix_value = *pix++;
      pixel->r = (UINT8)((pix_value >> (ffs(img->red_mask) - 1)) & 0xff);
      pixel->g = (UINT8)((pix_value >> (ffs(img->green_mask) - 1)) & 0xff);
      pixel->b = (UINT8)((pix_value >> (ffs(img->blue_mask) - 1)) & 0xff);
      ++pixel;
    }
  }

  ret = write_pixel_array_bmp(filename, img->width, img->height, pixel_array);

  free(pixel_array);
  return ret;
}

int main(int argc, char **argv)
{
  Display *dpy;
  int x, y, width, height;
  char filename[256];

  // Init X11 server
  dpy = XOpenDisplay(getenv("DISPLAY"));
  if (dpy == NULL)
  {
    fprintf(stderr, "Cannot connect to X server %s\n",
            getenv("DISPLAY") ? getenv("DISPLAY") : "(default)");
    exit(EXIT_FAILURE);
  }

  width  = 48;
  height = 98;

  XImage *ximg = XCreateImage(dpy,
                              DefaultVisual(dpy, DefaultScreen(dpy)),
                              DefaultDepth(dpy, DefaultScreen(dpy)),
                              ZPixmap, 0, 0,
                              width, height,
                              BitmapPad(dpy), 0);
  if (!ximg)
  {
    fprintf(stderr, "XCreateImage failed\n");
    exit(EXIT_FAILURE);
  }

  ximg->data = (char *)malloc(height * ximg->bytes_per_line);
  if (!ximg->data)
  {
    fprintf(stderr, "malloc image memory failed\n");
    exit(EXIT_FAILURE);
  }

  //printf("red shift=%d, green shift=%d, blue shift=%d\n",
  //    ffs(img->red_mask) - 1, ffs(img->green_mask) - 1, ffs(img->blue_mask) - 1);

  // Make a distinctive image
  for (y = 0; y < height; ++y)
  {
    for (x = 0; x < width; ++x)
    {
      if (x < y)
      {
        XPutPixel(ximg, x, y, ximg->red_mask);
      }
      else
      {
        XPutPixel(ximg, x, y, ximg->blue_mask);
      }
    }
  }

  snprintf(filename, 256, "%s_%dX%d.bmp", argv[0], width, height);
  write_X11Image_bmp(filename, ximg);

  XDestroyImage(ximg);
  XCloseDisplay(dpy);
  return 0;
}