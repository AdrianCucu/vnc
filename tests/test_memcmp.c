#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(int argc, char **argv)
{
  int a[3] = {0x00000000, 0x0000000, 0x00000001};
  int b[3] = {0x00000000, 0x0000000, 0x00000004};

  printf("%d\n", memcmp(a, b, 3 * sizeof(a[0])));

  return 0;
}
