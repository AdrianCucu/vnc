#include "gtk_vnc_viewer.h"

int main(int argc, char **argv)
{
  MyVncViewerGUI vncViewerGUI;
  vnc_viewer_gui_init(&vncViewerGUI);
  vnc_viewer_gui_main(&vncViewerGUI);
  vnc_viewer_gui_destroy(&vncViewerGUI);
  return 0;
}