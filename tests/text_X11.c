/*
  g++ -I. lib_vnc_example.cpp 
  -lvncserver -L/usr/X11/lib -lXrandr -lX11 -lXext -lXtst -lXfixes 
  -o lib_vnc_example
*/

#ifdef WIN32
#define sleep Sleep
#else
#include <unistd.h>
#endif

#ifdef __IRIX__
#include <netdb.h>
#endif

#include <rfb/rfb.h>
#include <rfb/keysym.h>
#include <rfb/rfbregion.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>

#include <X11/extensions/XTest.h>
#include <X11/extensions/Xfixes.h>
#include <X11/extensions/Xrandr.h>

#include <assert.h>
#include <signal.h>

#define MAXSAMPLES 10

void tvsub(struct timeval *out, const struct timeval *in)
{
  if ((out->tv_usec -= in->tv_usec) < 0)
  {
    out->tv_sec--;
    out->tv_usec += 1000000;
  }
  out->tv_sec -= in->tv_sec;
}

void tvadd(struct timeval *out, const struct timeval *in)
{
  if ((out->tv_usec += in->tv_usec) >= 1000000)
  {
    out->tv_sec++;
    out->tv_usec %= 1000000;
  }
  out->tv_sec += in->tv_sec;
}

struct tv_avg
{
  struct timeval sum;
  struct timeval samples[MAXSAMPLES];
  int curr_sample_idx;
};

#define INIT_TV_AVG \
  {                 \
    {0, 0}, {0}, 0  \
  }

void add_to_avg(struct tv_avg *avg,
                struct timeval *avg_out,
                const struct timeval *tv)
{
  tvsub(&avg->sum, &avg->samples[avg->curr_sample_idx]);
  tvadd(&avg->sum, tv);

  avg->samples[avg->curr_sample_idx].tv_sec = tv->tv_sec;
  avg->samples[avg->curr_sample_idx].tv_usec = tv->tv_usec;

  avg_out->tv_sec = avg->sum.tv_sec / MAXSAMPLES;
  avg_out->tv_usec =
      ((avg->sum.tv_sec % MAXSAMPLES) * 1000000 + avg->sum.tv_usec) / MAXSAMPLES;

  if (++avg->curr_sample_idx == MAXSAMPLES)
    avg->curr_sample_idx = 0;
}

/* Entry point */
int main(int argc, char **argv)
{
  Display *dpy;
  Window root;
  XImage *xim;
  int width, height;
  struct timeval tvstart, tvstop;

  // Init X11 server
  dpy = XOpenDisplay(getenv("DISPLAY"));

  if (dpy == NULL)
  {
    fprintf(stderr, "Cannot connect to X server\n");
    return FALSE;
  }

  width = DisplayWidth(dpy, DefaultScreen(dpy));
  height = DisplayHeight(dpy, DefaultScreen(dpy));

  xim = XCreateImage(dpy,
                     DefaultVisual(dpy, DefaultScreen(dpy)),
                     DefaultDepth(dpy, DefaultScreen(dpy)),
                     ZPixmap, 0, 0,
                     width,
                     height,
                     BitmapPad(dpy), 0);

  xim->data = malloc(xim->bytes_per_line * xim->height);

  // struct timespec tmstart, tmstop;
  // clock_gettime(CLOCK_MONOTONIC_RAW, &tmstart);
  gettimeofday(&tvstart, NULL);

  // Metoda 1
  // XGetSubImage(dpy,
  //              DefaultRootWindow(dpy),
  //              0, 0,
  //              width,
  //              height,
  //              AllPlanes,
  //              ZPixmap,
  //              xim, 0, 0);
  // // int y, x;

  // int w = xim->width;
  // int h = xim->height;

  // for (y = 0; y < h; y += (h / 2))
  //   for (x = 0; x < w; x += (w / 2))
  //     //printf("%d, %d -> %d, %d\n", x, y, x + (w / 2), y + (h / 2));
  //     XGetSubImage(dpy,
  //                  DefaultRootWindow(dpy),
  //                  x, y,
  //                  xim->width  / 2,
  //                  xim->height / 2,
  //                  AllPlanes,
  //                  ZPixmap,
  //                  xim, x, y);
  usleep(50000);

  gettimeofday(&tvstop, NULL);
  // clock_gettime(CLOCK_MONOTONIC_RAW, &tmstop);

  printf("%ld.%ld -> %ld.%ld\n",
         tvstart.tv_sec, tvstart.tv_usec,
         tvstop.tv_sec, tvstop.tv_usec);

  // printf("%ld.%ld -> %ld.%ld\n",
  //        tmstart.tv_sec, tmstart.tv_nsec,
  //        tmstop.tv_sec, tmstop.tv_nsec);

  tvsub(&tvstop, &tvstart);

  double ms = tvstop.tv_sec * 1000 + tvstop.tv_usec / 1000.0;
  printf("Took %.2f\n", ms / 1000);

  if (xim->data)
  {
    printf("%04x %04x\n", xim->data[0], xim->data[4]);
    XFree(xim->data);
  }
  XFree(xim);

  //   gettimeofday(&tvstart, NULL);

  //   xim = XGetImage(dpy,
  //                   DefaultRootWindow(dpy),
  //                   0, 0,
  //                   width,
  //                   height,
  //                   AllPlanes,
  //                   ZPixmap);

  //   gettimeofday(&tvstop, NULL);

  //   tvsub(&tvstop, &tvstart);

  //   printf("Took %ld.%ld\n", tvstop.tv_sec, tvstop.tv_usec);

  //   if (xim->data)
  //   {
  //     printf("%04x %04x\n", xim->data[0], xim->data[4]);
  //     XFree(xim->data);
  //   }
  //   XFree(xim);

  XCloseDisplay(dpy);
  return 0;
}