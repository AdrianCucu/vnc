#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <signal.h>

#include <xcb/xcb.h>
#include <xcb/shm.h>
#include <xcb/xcb_image.h>

#include "X11_utils.h"
#include "constants.h"
#include "bitmap.h"
#include "tv.h"

// #define USE_SHM 1

#define SHM_NOT_AVAILABLE 0
#define HAVE_SHM_XIMAGE 1
#define HAVE_SHM_PIXMAP 2

/*
 * Check if the X Shared Memory extension is available.
 * Return:  0 = not available
 *          1 = shared XImage support available
 *          2 = shared Pixmap support available also
 */
int check_for_xshm(xcb_connection_t *con)
{
  xcb_shm_query_version_reply_t *reply;

  reply = xcb_shm_query_version_reply(con, xcb_shm_query_version(con), NULL);
  if (!reply || !reply->shared_pixmaps)
    return SHM_NOT_AVAILABLE;

  if (reply->shared_pixmaps)
    return HAVE_SHM_PIXMAP;

  return SHM_NOT_AVAILABLE;
}

void alloc_xshm(xcb_connection_t *con,
                xcb_visualid_t vis,
                int width,
                int height,
                int bytes_per_pixel,
                xcb_shm_segment_info_t *shminfo)
{
  assert(con);
  assert(shminfo);

  shminfo->shmid = shmget(IPC_PRIVATE, width * height * bytes_per_pixel, IPC_CREAT | 0600);
  shminfo->shmaddr = shmat(shminfo->shmid, 0, 0);

  shminfo->shmseg = xcb_generate_id(con);
  xcb_shm_attach(con, shminfo->shmseg, shminfo->shmid, 0);
  shmctl(shminfo->shmid, IPC_RMID, 0);
}

void destroy_xshm(xcb_connection_t *conn, xcb_shm_segment_info_t *shminfo)
{
  xcb_shm_detach(conn, shminfo->shmseg);
  shmdt(shminfo->shmaddr);
}

static int caught_signal = 0;

void sighandler(int sig)
{
  caught_signal = 1;
}


/* Entry point */
int main(int argc, char **argv)
{
  xcb_connection_t *conn;
  xcb_window_t root_window;
  xcb_screen_t *screen;
  xcb_visualid_t vis;
  xcb_generic_error_t *e = NULL;
  xcb_image_t *xim;

  int width, height;
  int depth, bytes_per_pixel;
  
  struct tv_avg avg = INIT_TV_AVG;
  struct timeval tvstart, tvstop;
  double ms;

  signal(SIGHUP, sighandler);
  signal(SIGINT, sighandler);
  signal(SIGTERM, sighandler);

  // Init XCB connection with X server
  conn = xcb_connect(NULL, NULL);
  if (xcb_connection_has_error(conn) > 0)
  {
    fprintf(stderr, "Cannot open display\n");
    exit(EXIT_FAILURE);
  }

  screen = xcb_setup_roots_iterator(xcb_get_setup(conn)).data;
  if (screen == NULL)
  {
    fprintf(stderr, "Failed to obtain screen\n");
    exit(EXIT_FAILURE);
  }

  width = screen->width_in_pixels;
  height = screen->height_in_pixels;
  vis = screen->root_visual;
  root_window = screen->root;

  xcb_format_iterator_t it = xcb_setup_pixmap_formats_iterator(xcb_get_setup(conn));
  for (; it.rem; xcb_format_next(&it))
  {
    if (it.data->depth == screen->root_depth)
    {
      printf("bpp = %d, %d, %d\n", it.data->bits_per_pixel, it.data->depth, it.data->scanline_pad);
      depth = it.data->depth;
      bytes_per_pixel = it.data->bits_per_pixel / 8;
      if (bytes_per_pixel == 0)
        bytes_per_pixel = 1;
      break;
    }
  }
#if defined(USE_SHM)
  /* USE SHM */
  xcb_shm_segment_info_t shminfo;
  if (!check_for_xshm(conn))
  {
    fprintf(stderr, "MITM-SHM extension is not available\n");
    xcb_disconnect(conn);
    exit(EXIT_FAILURE);
  }
  printf("Let's go with" COLOR_GREEN " MITM-SHM!\n" COLOR_NONE);

  alloc_xshm(conn, vis, width, height, bytes_per_pixel, &shminfo);
  uint32_t *data = shminfo.shmaddr;
#endif

  int iter = 1000;
  for (; !caught_signal && iter--;)
  {
    gettimeofday(&tvstart, NULL);

#if defined(USE_SHM)
    /* WITH SHM*/
    xcb_shm_get_image_cookie_t iq;
    xcb_shm_get_image_reply_t *img;
    xcb_generic_error_t *e = NULL;

    iq = xcb_shm_get_image(conn,
                           root_window,
                           0, 0,
                           width, height,
                           ~0U,
                           XCB_IMAGE_FORMAT_Z_PIXMAP,
                           shminfo.shmseg,
                           0);
    img = xcb_shm_get_image_reply(conn, iq, NULL);

    if (e)
    {
      fprintf(stderr, "error: \n");
      free(e);
      break;
    }
    free(img);
#else
    /* NO SHM */
    xcb_get_image_cookie_t iq;
    xcb_get_image_reply_t *img;

    iq = xcb_get_image(conn,
                       XCB_IMAGE_FORMAT_Z_PIXMAP,
                       root_window,
                       0, 0,
                       width, height,
                       ~0);
    img = xcb_get_image_reply(conn, iq, &e);

    if (e)
    {
      fprintf(stderr, "error: \n");
      break;
    }
    free(img);
#endif

    gettimeofday(&tvstop, NULL);
    tvsub(&tvstop, &tvstart);
    add_to_avg(&avg, &tvstop);
  }

  get_avg(&avg, &tvstop);
  ms = tvstop.tv_sec * 1000 + tvstop.tv_usec / 1000.0;
  printf("Average: %f ms\n", ms);

  // Release resources
#if defined(USE_SHM)
  destroy_xshm(conn, &shminfo);
#endif
  xcb_disconnect(conn);
  return 0;
}