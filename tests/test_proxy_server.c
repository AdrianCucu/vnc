#define _GNU_SOURCE

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <netinet/tcp.h>
#include <arpa/inet.h>

#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <netdb.h>
#include <resolv.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <pthread.h>

#include "proxy_messages.h"
#include "types.h"
#include "db.h"

#define FREE(__ptr)    \
  do                   \
  {                    \
    if (__ptr != NULL) \
      free(__ptr);     \
    __ptr = NULL;      \
  } while (0)

#define CLOSE_FD(__fd) \
  do                   \
  {                    \
    if (__fd >= 0)     \
      close(__fd);     \
    __fd = -1;         \
  } while (0)

#define DEBUG_LIST_SHOW 1
#define USE_SPLICE 1
#define DEBUG 1

#undef DEBUG_WRITE_EXACT
#undef DEBUG_READ_EXACT

#define INIT_MUTEX_ATTR(mutex, attr) pthread_mutex_init(&(mutex), &(attr))
#define INIT_MUTEX(mutex) pthread_mutex_init(&(mutex), NULL)
#define TINI_MUTEX(mutex) pthread_mutex_destroy(&(mutex))
#define TSIGNAL(cond) pthread_cond_signal(&(cond))
#define WAIT(cond, mutex) pthread_cond_wait(&(cond), &(mutex))
#define COND(cond) pthread_cond_t(cond)
#define INIT_COND(cond) pthread_cond_init(&(cond), NULL)
#define TINI_COND(cond) pthread_cond_destroy(&(cond))
#define MUTEX(mutex) pthread_mutex_t(mutex)
#define LOCK(mutex) pthread_mutex_lock(&(mutex))
#define UNLOCK(mutex) pthread_mutex_unlock(&(mutex))

#define MAX_VNC_PEERS 10
#define BUF_SIZE 16384

#define PIPE_RD_END 0
#define PIPE_WR_END 1

#define SERVER_SOCKET_ERROR -1
#define SERVER_SETSOCKOPT_ERROR -2
#define SERVER_BIND_ERROR -3
#define SERVER_LISTEN_ERROR -4
#define CLIENT_SOCKET_ERROR -5
#define CLIENT_RESOLVE_ERROR -6
#define CLIENT_CONNECT_ERROR -7
#define CREATE_PIPE_ERROR -8
#define BROKEN_PIPE_ERROR -9
#define SYNTAX_ERROR -10

// typedef enum {TRUE = 1, FALSE = 0} bool;

static int signal_catched = 0;

/* Handle finished child process */
static void sigchld_handler(int signal)
{
  while (waitpid(-1, NULL, WNOHANG) > 0)
    ;
}

/* Handle term signal */
static void sigterm_handler(int signal)
{
  signal_catched = 1;
}

static void generate_passwd(char *passwd_out, size_t len)
{
  static char alphanum[] = "abcdefghijklmnopqrstuwxyz0123456789";
  int i, idx;

  while (len-- > 0)
  {
    idx = (int)(rand() % strlen(alphanum));
    *passwd_out++ = alphanum[idx];
  }
  *passwd_out++ = 0;
}

static void generate_sessid(char *sessid_out, size_t len)
{
  int random;
  // make some shit randomness
  random = rand();
  while (len-- > 0)
  {
    random ^= rand();
    *sessid_out++ = '0' + (random % 9);
  }
  *sessid_out++ = '\0';
}

typedef struct __proxy_client proxy_client;
typedef struct __vnc_session vnc_session;

typedef struct __vnc_session_connection
{
  pthread_t conn_thread;
  BOOL is_vnc_host;
  BOOL should_run;
  proxy_client *client;
  vnc_session *vnc_sess;
  // Forwards data from sock_rd to sock_wr
  int sock_rd;
  int sock_wr;
  struct __vnc_session_connection *prev, *next;
} vnc_session_connection;

typedef struct __vnc_session
{
  pthread_mutex_t sync_mtx;
  // int num_vnc_peers;
  // List of connection on the vnc host side
  vnc_session_connection *host_connection_list_head;
  // List of connection on the vnc clients side
  vnc_session_connection *connection_list_head;
  int id;
  struct __vnc_session *prev, *next;
} vnc_session;

void vnc_session_add_connection(vnc_session *vnc_sess, vnc_session_connection *conn);
void vnc_session_remove_connection(vnc_session *vnc_sess, vnc_session_connection *conn);
void vnc_session_connection_destroy(vnc_session_connection *conn);
vnc_session *vnc_session_new(proxy_client *host, proxy_client *client);
void vnc_session_abort(vnc_session *vnc_sess);
BOOL vnc_session_start(vnc_session *vnc_sess);
void *vnc_session_connection_loop(void *conn);

typedef struct __proxy_client
{
  pthread_mutexattr_t mta;
  pthread_mutex_t client_write_mtx;
  pthread_mutex_t client_update_mtx;
  int client_sock;
  char *client_addr;
  void *client_data;
  pthread_t client_thread;

  vnc_session *vnc_sess_ctx;

  // Lock client_update_mtx and set to TRUE is you need to receive message
  BOOL need_response;

  client cl;

  struct __proxy_client *prev, *next;
} proxy_client;

typedef struct __proxy_server
{
  BOOL foreground;
  char *addr;
  int port;
  struct in_addr inetaddr;
  int sock;
  int connections_processed;
  int clients_count;

  pthread_mutex_t client_list_mtx;
  proxy_client *client_list_head;

  pthread_mutex_t vnc_sessions_list_mtx;
  vnc_session *vnc_sessions_list_head;

  // database
  db_connection *db_conn;
} proxy_server;

typedef struct
{
  proxy_client *client;
  proxy_server *server;
} proxy_client_iterator;

typedef struct
{
  proxy_server *server;
  vnc_session *current;
  vnc_session *prev, *next;
} vnc_session_iterator;

proxy_server *get_proxy_server();
BOOL proxy_server_process_arguments(proxy_server *server, int argc, char **argv);
BOOL init_proxy_server(proxy_server *server);
void destroy_proxy_server(proxy_server *server);
void proxy_server_run(proxy_server *server);

static void add_client(proxy_server *server, proxy_client *client);
static void remove_client(proxy_server *server, proxy_client *client);
static proxy_client *search_client(proxy_server *server, unsigned char *sessid);
static proxy_client *make_new_client(proxy_server *server, int client_socket, char *addr);
static void close_client(proxy_client *client);
static void client_connection_gone(proxy_client *client);
static int create_listen_socket(char *addr, int port);

proxy_client_iterator *get_client_iterator(proxy_server *server);
proxy_client *client_iterator_head(proxy_client_iterator *i);
proxy_client *client_iterator_next(proxy_client_iterator *i);
void release_client_iterator(proxy_client_iterator *i);

vnc_session_iterator *get_vnc_session_iterator(proxy_server *server);
vnc_session *vnc_session_iterator_head(vnc_session_iterator *i);
vnc_session *vnc_session_iterator_next(vnc_session_iterator *i);
void release_vnc_session_iterator(vnc_session_iterator *i);

proxy_client_iterator *get_client_iterator(proxy_server *server)
{
  proxy_client_iterator *i = NULL;

  if (server == NULL)
    return NULL;

  i = (proxy_client_iterator *)malloc(sizeof(proxy_client_iterator));
  if (i == NULL)
    return NULL;

  i->client = NULL;
  i->server = server;
  return i;
}

proxy_client *client_iterator_head(proxy_client_iterator *i)
{
  if (i == NULL || i->server == NULL)
    return NULL;

  LOCK(i->server->client_list_mtx);
  i->client = i->server->client_list_head;
  UNLOCK(i->server->client_list_mtx);
  return i->client;
}

proxy_client *client_iterator_next(proxy_client_iterator *i)
{
  if (i == NULL || i->server == NULL)
    return NULL;

  LOCK(i->server->client_list_mtx);
  if (i->client != NULL)
    i->client = i->client->next;
  else
    i->client = NULL;
  UNLOCK(i->server->client_list_mtx);
  return i->client;
}

void release_client_iterator(proxy_client_iterator *i)
{
  FREE(i);
}

vnc_session_iterator *get_vnc_session_iterator(proxy_server *server)
{
  vnc_session_iterator *i = NULL;

  if (server == NULL)
    return NULL;

  i = (vnc_session_iterator *)malloc(sizeof(vnc_session_iterator));
  if (i == NULL)
    return NULL;

  i->server = server;
  i->current = NULL;
  i->next = i->prev = NULL;
  return i;
}

vnc_session *vnc_session_iterator_head(vnc_session_iterator *i)
{
  if (i == NULL || i->server == NULL)
    return NULL;

  LOCK(i->server->vnc_sessions_list_mtx);
  i->current = i->server->vnc_sessions_list_head;
  if (i->current != NULL)
  {
    i->prev = i->current->prev;
    i->next = i->current->next;
  }
  else
  {
    i->prev = NULL;
    i->next = NULL;
  }
  UNLOCK(i->server->vnc_sessions_list_mtx);
  return i->current;
}

vnc_session *vnc_session_iterator_next(vnc_session_iterator *i)
{
  if (i == NULL)
    return NULL;

  i->current = i->next;

  if (i->current != NULL)
  {
    i->next = i->current->next;
    i->prev = i->current->prev;
  }
  else
  {
    i->next = NULL;
    i->prev = NULL;
  }
  return i->current;
}

void release_vnc_session_iterator(vnc_session_iterator *i)
{
  FREE(i);
}

static proxy_server *g_proxyserver;

int main(int argc, char **argv)
{
  signal(SIGCHLD, sigchld_handler); // prevent ended children from becoming zombies
  signal(SIGTERM, sigterm_handler); // handle KILL signal
  signal(SIGINT, sigterm_handler);  // handle KILL signal
  signal(SIGHUP, sigterm_handler);

  g_proxyserver = get_proxy_server();
  if (!g_proxyserver)
  {
    fprintf(stderr, "Failed to get_proxy_server\n");
    exit(EXIT_FAILURE);
  }

  if (proxy_server_process_arguments(g_proxyserver, argc, argv) == FALSE)
  {
    fprintf(stderr, "Failed to proxy_server_process_arguments\n");
    exit(EXIT_FAILURE);
  }

  if (init_proxy_server(g_proxyserver) == FALSE)
  {
    fprintf(stderr, "Failed to init_proxy_server\n");
    exit(EXIT_FAILURE);
  }

  // proxy_server_show_db_list(proxy_server);

  if (g_proxyserver->foreground || 1)
    proxy_server_run(g_proxyserver);

  destroy_proxy_server(g_proxyserver);

  return EXIT_SUCCESS;
}

//////////////////////////// UTILS /////////////////////////////
int wait_connection(int on_sock, int timeout_milli)
{
  int ret;
  struct timeval tv;
  struct sockaddr_storage addr;
  struct sockaddr_storage peer;
  socklen_t addrlen;
  int sock = -1;
  fd_set listen_fds; /* temp file descriptor list for select() */

  FD_ZERO(&listen_fds);
  // set file descriptors for select
  FD_SET(on_sock, &listen_fds);

  // set timeout
  tv.tv_sec = timeout_milli / 1000;
  tv.tv_usec = (timeout_milli % 1000) * 1000;

  ret = select(on_sock + 1, &listen_fds, NULL, NULL, &tv /* NULL = block*/);

  if (ret == -1)
  {
    perror("select");
    goto error;
  }

  if (ret == 0)
  {
    printf("select: timeout expired\n");
    goto error;
  }

  addrlen = sizeof(peer);
  sock = accept(on_sock, (struct sockaddr *)&addr, &addrlen);

#ifdef DEBUG
  if (getsockname(sock, (struct sockaddr *)&addr, &addrlen) == -1)
  {
    perror("getsockname");
    goto error;
  }

  if (getpeername(sock, (struct sockaddr *)&peer, &addrlen) == -1)
  {
    perror("getpeername");
    goto error;
  }
  printf("new connection: sock[%d]: %s:%d->%s:%d\n",
         sock,
         inet_ntoa(((struct sockaddr_in *)&addr)->sin_addr),
         ntohs(((struct sockaddr_in *)&addr)->sin_port),
         inet_ntoa(((struct sockaddr_in *)&peer)->sin_addr),
         ntohs(((struct sockaddr_in *)&peer)->sin_port));
#endif
  return sock;
error:
  CLOSE_FD(sock);
  return -1;
}

void print_sock4_name(int sock)
{
  socklen_t addrlen;
  struct sockaddr_in inaddr;
  if (sock == -1)
    return;

  addrlen = sizeof(inaddr);
  if (getsockname(sock, (struct sockaddr *)&inaddr, &addrlen) == -1)
  {
    perror("getsockname");
    return;
  }

  printf("socket[%d]: bound on %s:%d\n",
         sock, inet_ntoa(inaddr.sin_addr), ntohs(inaddr.sin_port));
}

// Get socket bound port number (if valid socket)
// port 0 - means error
uint16_t get_bound_socket_port(int sock)
{
  socklen_t addrlen;
  struct sockaddr_in inaddr;

  if (sock == -1)
    return 0;

  addrlen = sizeof(inaddr);
  if (getsockname(sock, (struct sockaddr *)&inaddr, &addrlen) == -1)
  {
    perror("getsockname");
    return 0;
  }
  return ntohs(inaddr.sin_port);
}

static int create_listen_socket(char *addr, int port)
{
  int sock, optval = 1;
  int flags;
  struct sockaddr_in sockaddr;

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    return SERVER_SOCKET_ERROR;
  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) < 0)
    return SERVER_SETSOCKOPT_ERROR;
  flags = fcntl(sock, F_GETFL, 0);
  if (fcntl(sock, F_SETFL, flags | O_NONBLOCK) < 0)
    return -1;

  memset(&sockaddr, 0, sizeof(sockaddr));
  sockaddr.sin_family = AF_INET;
  sockaddr.sin_port = htons(port);

  if (addr == NULL)
    sockaddr.sin_addr.s_addr = INADDR_ANY;
  else
    sockaddr.sin_addr.s_addr = inet_addr(addr);

  if (bind(sock, (struct sockaddr *)&sockaddr, sizeof(sockaddr)) != 0)
  {
    fprintf(stderr, "create_socket: Bind error!\n");
    return SERVER_BIND_ERROR;
  }

  if (listen(sock, 1) < 0)
    return SERVER_LISTEN_ERROR;

  return sock;
}

static int create_socket(char *addr, int port)
{
  int server_sock, optval = 1;
  struct sockaddr_in server_addr;

  if ((server_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    return SERVER_SOCKET_ERROR;

  if (setsockopt(server_sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) < 0)
    return SERVER_SETSOCKOPT_ERROR;

  memset(&server_addr, 0, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(port);

  if (addr == NULL)
    server_addr.sin_addr.s_addr = INADDR_ANY;
  else
    server_addr.sin_addr.s_addr = inet_addr(addr);

  if (bind(server_sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) != 0)
  {
    fprintf(stderr, "create_socket: Bind error!\n");
    return SERVER_BIND_ERROR;
  }

  if (listen(server_sock, 20) < 0)
    return SERVER_LISTEN_ERROR;

  return server_sock;
}
//////////////////////////// UTILS /////////////////////////////

void vnc_session_add_connection(vnc_session *vnc_sess, vnc_session_connection *conn)
{
  if (vnc_sess == NULL || conn == NULL)
    return;

  LOCK(vnc_sess->sync_mtx);
  if (conn->is_vnc_host)
  {
    conn->next = vnc_sess->host_connection_list_head;
    conn->prev = NULL;
    if (vnc_sess->host_connection_list_head != NULL)
      vnc_sess->host_connection_list_head->prev = conn;
    vnc_sess->host_connection_list_head = conn;
  }
  else
  {
    conn->next = vnc_sess->connection_list_head;
    conn->prev = NULL;
    if (vnc_sess->connection_list_head != NULL)
      vnc_sess->connection_list_head->prev = conn;
    vnc_sess->connection_list_head = conn;
  }
  UNLOCK(vnc_sess->sync_mtx);
}

void vnc_session_remove_connection(vnc_session *vnc_sess, vnc_session_connection *conn)
{
  vnc_session_connection *ic = NULL;

  if (vnc_sess == NULL || conn == NULL)
    return;

  LOCK(vnc_sess->sync_mtx);
  if (conn->is_vnc_host)
  {
    for (ic = vnc_sess->host_connection_list_head; ic != NULL; ic = ic->next)
    {
      if (ic == conn)
      {
        if (ic->prev != NULL)
          ic->prev->next = ic->next;
        if (ic->next != NULL)
          ic->next->prev = ic->prev;
        if (ic == vnc_sess->host_connection_list_head)
          vnc_sess->host_connection_list_head = ic->next;
        break;
      }
    }
  }
  else
  {
    for (ic = vnc_sess->connection_list_head; ic != NULL; ic = ic->next)
    {
      if (ic == conn)
      {
        if (ic->prev != NULL)
          ic->prev->next = ic->next;
        if (ic->next != NULL)
          ic->next->prev = ic->prev;
        if (ic == vnc_sess->connection_list_head)
          vnc_sess->connection_list_head = ic->next;
        break;
      }
    }
  }
  UNLOCK(vnc_sess->sync_mtx);
}

void vnc_session_connection_destroy(vnc_session_connection *conn)
{
  if (conn == NULL)
    return;
  CLOSE_FD(conn->sock_rd);
  CLOSE_FD(conn->sock_wr);
  FREE(conn);
}

vnc_session_connection *try_open_new_connection(proxy_client *client)
{
  vnc_session_connection *conn = NULL;
  ProxyInitConnectionMsg msg;
  int listen_sock = -1, sock = -1;
  char buf[256];

  if (client == NULL)
    return -1;

  listen_sock = create_listen_socket(NULL, 0);
  if (listen_sock == -1)
    return NULL;

#ifdef DEBUG
  print_sock4_name(listen_sock);
#endif

  msg.type = INIT_CONNECTION;
  msg.port = get_bound_socket_port(listen_sock);

  if (proxy_client_write_exact_timeout(client, (char *)&msg, sz_ProxyInitConnectionMsg, 1000) != 1)
  {
    fprintf(stderr, "Error writing to client\n\n");
    CLOSE_FD(listen_sock);
    return NULL;
  }

  sock = wait_connection(listen_sock, 20000);
  if (sock == -1)
  {
    fprintf(stderr, "host socket connection failed\n");
    CLOSE_FD(listen_sock);
    return NULL;
  }

  CLOSE_FD(listen_sock);

  conn = (vnc_session_connection *)malloc(sizeof(vnc_session_connection));
  if (conn == NULL)
  {
    CLOSE_FD(sock);
    return NULL;
  }

  conn->next = NULL;
  conn->prev = NULL;
  conn->sock_rd = sock;
  conn->client = client;

  return conn;
}

vnc_session *vnc_session_init_proxy_connection(proxy_client *host, proxy_client *client)
{
  vnc_session *vnc_sess = NULL;
  vnc_session_connection *client_conn_ctx = NULL;
  vnc_session_connection *host_conn_ctx = NULL;

  if (host == NULL || client == NULL)
    return NULL;

  if ((host_conn_ctx = try_open_new_connection(host)) == NULL)
    goto error;
  if ((client_conn_ctx = try_open_new_connection(client)) == NULL)
    goto error;

  host_conn_ctx->sock_wr = client_conn_ctx->sock_rd;
  client_conn_ctx->sock_wr = host_conn_ctx->sock_rd;

  host_conn_ctx->is_vnc_host = TRUE;
  client_conn_ctx->is_vnc_host = FALSE;

  if (host->vnc_sess_ctx != NULL)
  {
    vnc_sess = host->vnc_sess_ctx;

    LOCK(host->client_update_mtx);
    host_conn_ctx->vnc_sess = vnc_sess;
    UNLOCK(host->client_update_mtx);
    vnc_session_add_connection(vnc_sess, host_conn_ctx);

    LOCK(client->client_update_mtx);
    client->vnc_sess_ctx = vnc_sess;
    client_conn_ctx->vnc_sess = vnc_sess;
    UNLOCK(client->client_update_mtx);
    vnc_session_add_connection(vnc_sess, client_conn_ctx);
  }
  else
  {
    vnc_sess = (vnc_session *)calloc(1, sizeof(vnc_session));
    if (vnc_sess == NULL)
      return NULL;
    INIT_MUTEX(vnc_sess->sync_mtx);

    LOCK(host->client_update_mtx);
    host->vnc_sess_ctx = vnc_sess;
    host_conn_ctx->vnc_sess = vnc_sess;
    UNLOCK(host->client_update_mtx);
    vnc_session_add_connection(vnc_sess, host_conn_ctx);

    LOCK(client->client_update_mtx);
    client->vnc_sess_ctx = vnc_sess;
    client_conn_ctx->vnc_sess = vnc_sess;
    UNLOCK(client->client_update_mtx);
    vnc_session_add_connection(vnc_sess, client_conn_ctx);
  }

  host_conn_ctx->should_run = TRUE;
  pthread_create(&host_conn_ctx->conn_thread, NULL,
                 vnc_session_connection_loop, host_conn_ctx);
  client_conn_ctx->should_run = TRUE;
  pthread_create(&client_conn_ctx->conn_thread, NULL,
                 vnc_session_connection_loop, client_conn_ctx);

  return vnc_sess;

error:
  FREE(host_conn_ctx);
  FREE(client_conn_ctx);
  return NULL;
}

void *vnc_session_connection_loop(void *addr_conn)
{
  vnc_session_connection *conn;
  struct timeval tv;
  fd_set rfds; /* temp file descriptor list for select() */
  char buf[BUF_SIZE];
  int nbytes;

  conn = (vnc_session_connection *)addr_conn;
  if (conn == NULL)
    return NULL;

#ifdef USE_SPLICE
  int pipe_fds[2];
  if (pipe(pipe_fds) == -1)
  {
    perror("pipe");
    vnc_session_connection_closed(conn->vnc_sess, conn);
    return NULL;
  }
#endif

  while (!signal_catched && conn->should_run)
  {
    if (conn->sock_rd == -1)
      break;

    FD_ZERO(&rfds);
    FD_SET(conn->sock_rd, &rfds);

    tv.tv_sec = 0;
    tv.tv_usec = 500000;

    if (select(conn->sock_rd + 1, &rfds, NULL, NULL, &tv /* NULL = block*/) == -1)
    {
      fprintf(stderr, "vnc_session_connection_loop: error in select\n");
      goto err;
    }

    if (!FD_ISSET(conn->sock_rd, &rfds))
      continue; // timeout

#ifdef USE_SPLICE
    nbytes = splice(conn->sock_rd, NULL, pipe_fds[PIPE_WR_END], NULL,
                    SSIZE_MAX, SPLICE_F_NONBLOCK | SPLICE_F_MOVE);
    if (nbytes <= 0)
    {
      perror("splice");
      goto err;
    }
    if (splice(pipe_fds[PIPE_RD_END], NULL,
               conn->sock_wr, NULL,
               nbytes, SPLICE_F_MOVE) <= 0)
    {
      perror("splice");
      goto err;
    }
#else
    nbytes = read(conn->sock_rd, buf, BUF_SIZE);
    if (nbytes <= 0)
      goto err;

    if (write(conn->sock_wr, buf, nbytes) <= 0)
    {
      perror("write");
      goto err;
    }
#endif
  }
err:
#ifdef USE_SPLICE
  CLOSE_FD(pipe_fds[0]);
  CLOSE_FD(pipe_fds[1]);
#endif
  vnc_session_connection_closed(conn->vnc_sess, conn);
}

BOOL vnc_session_proxy_client_have_host_connection(vnc_session *vnc_sess, proxy_client *cl)
{
  BOOL found = FALSE;
  vnc_session_connection *i;
  if (vnc_sess == NULL || cl == NULL)
    return FALSE;

  LOCK(vnc_sess->sync_mtx);
  for (i = vnc_sess->host_connection_list_head; i != NULL; i = i->next)
  {
    if (i->client == cl)
    {
      found = TRUE;
      break;
    }
  }
  UNLOCK(vnc_sess->sync_mtx);
  return found;
}

static void vnc_session_connection_print(vnc_session_connection *conn)
{
  socklen_t addrlen;
  struct sockaddr_in addr1, addr2;

  if (conn == NULL)
    return;

  addrlen = sizeof(addr1);
  if (getpeername(conn->sock_rd, (struct sockaddr *)&addr1, &addrlen) == -1)
  {
    perror("getsockname");
    return;
  }

  addrlen = sizeof(addr2);
  if (getpeername(conn->sock_wr, (struct sockaddr *)&addr2, &addrlen) == -1)
  {
    perror("getsockname");
    return;
  }

  if (conn->is_vnc_host)
  {
    printf("VNC HOST SIDE(%s:%d) forwards to(%s:%d)\n",
           inet_ntoa(addr1.sin_addr), ntohs(addr1.sin_port),
           inet_ntoa(addr2.sin_addr), ntohs(addr2.sin_port));
  }
  else
  {
    printf("VNC CLIENT SIDE(%s:%d) forwards to(%s:%d)\n",
           inet_ntoa(addr1.sin_addr), ntohs(addr1.sin_port),
           inet_ntoa(addr2.sin_addr), ntohs(addr2.sin_port));
  }
}

static void vnc_session_print(vnc_session *vnc_sess)
{
  if (vnc_sess == NULL)
    return;

  printf("=-=-==-=-= vncSession[%p] =-=-==-=-=\n", vnc_sess);
  LOCK(vnc_sess->sync_mtx);
  if (vnc_sess->connection_list_head == NULL)
    printf("Clients side connections(NULL)\n");
  else
  {
    vnc_session_connection *conn;
    for (conn = vnc_sess->connection_list_head;
         conn != NULL; conn = conn->next)
      vnc_session_connection_print(conn);
  }

  if (vnc_sess->host_connection_list_head == NULL)
    printf("Host side connections(NULL)\n");
  else
  {
    vnc_session_connection *conn;
    for (conn = vnc_sess->host_connection_list_head;
         conn != NULL; conn = conn->next)
      vnc_session_connection_print(conn);
  }
  UNLOCK(vnc_sess->sync_mtx);
  printf("=-=-==-=-=-=-=-=-==-=-=-=-=-=-==-=-=-=-=-=-=-=\n");
}

void vnc_session_abort(vnc_session *vnc_sess)
{
  vnc_session_connection *i, nxt;

  if (vnc_sess == NULL)
    return;

  LOCK(vnc_sess->sync_mtx);
  for (i = vnc_sess->host_connection_list_head; i != NULL; i = i->next)
  {
    i->should_run = FALSE;
    CLOSE_FD(i->sock_rd);
    CLOSE_FD(i->sock_wr);
  }

  for (i = vnc_sess->connection_list_head; i != NULL; i = i->next)
  {
    i->should_run = FALSE;
    CLOSE_FD(i->sock_rd);
    CLOSE_FD(i->sock_wr);
  }

  UNLOCK(vnc_sess->sync_mtx);
}

void vnc_session_proxy_client_closed(vnc_session *vnc_sess, proxy_client *client)
{
  vnc_session_connection *i;

  if (vnc_sess == NULL || client == NULL)
    return;

  LOCK(vnc_sess->sync_mtx);
  for (i = vnc_sess->host_connection_list_head; i != NULL; i = i->next)
  {
    if (i->client == client)
    {
      // vnc_session_connection_closed(vnc_sess, i);
      CLOSE_FD(i->sock_rd);
      CLOSE_FD(i->sock_wr);
    }
  }

  for (i = vnc_sess->connection_list_head; i != NULL; i = i->next)
  {
    if (i->client == client)
    {
      // vnc_session_connection_closed(vnc_sess, i);
      CLOSE_FD(i->sock_rd);
      CLOSE_FD(i->sock_wr);
    }
  }
  UNLOCK(vnc_sess->sync_mtx);
}

void vnc_session_connection_closed(vnc_session *vnc_sess, vnc_session_connection *conn)
{
  if (vnc_sess == NULL || conn == NULL)
    return;

  conn->should_run = FALSE;
  CLOSE_FD(conn->sock_rd);
  CLOSE_FD(conn->sock_wr);

  pthread_join(conn->conn_thread, NULL);

  vnc_session_remove_connection(vnc_sess, conn);
  vnc_session_connection_destroy(conn);
}

proxy_server *get_proxy_server()
{
  proxy_server *server = (proxy_server *)calloc(1, sizeof(proxy_server));
  if (server == NULL)
    return NULL;
  server->sock = -1;
  server->port = -1;
  return server;
}

/* Parse command line options */
BOOL proxy_server_process_arguments(proxy_server *server, int argc, char **argv)
{
  int c, port = 0;

  if (!server)
    return FALSE;

  while ((c = getopt(argc, argv, "p:b:fs")) != -1)
  {
    switch (c)
    {
    case 'p':
      port = atoi(optarg);
      if (!(port > 0 && port < 65535))
      {
        fprintf(stderr, "Invalid port: %d\n", port);
        return FALSE;
      }
      server->port = port;
      printf("Server port set to: %d\n", server->port);
      break;
    case 'b':
      if (inet_aton(optarg, &server->inetaddr) == 0)
      {
        fprintf(stderr, "Invalid address: '%s'\n", optarg);
        return FALSE;
      }
      server->addr = inet_ntoa(server->inetaddr);
      printf("Server bind address set to: %s\n", server->addr);
      break;
    case 'f':
      printf("Server run in foreground flag set to: TRUE\n");
      break;
    }
  }
  return TRUE;
}

BOOL init_proxy_server(proxy_server *server)
{
  if (server == NULL)
    return FALSE;

  if ((server->sock = create_socket(server->addr, server->port)) < 0)
  {
    fprintf(stderr, "Cannot create server socket\n");
    return FALSE;
  }

  if (db_open_connection(&server->db_conn) != SQL_OK)
  {
    fprintf(stderr, "Failed to open db!\n");
    return FALSE;
  }

  // Seed number for rand()
  srand(time(NULL));
  INIT_MUTEX(server->client_list_mtx);
  INIT_MUTEX(server->vnc_sessions_list_mtx);
  server->connections_processed = 0;
  server->clients_count = 0;

  return TRUE;
}

void destroy_proxy_server(proxy_server *server)
{
  if (server == NULL)
    return;

  CLOSE_FD(server->sock);
  db_destroy(server->db_conn);
  TINI_MUTEX(server->client_list_mtx);
  TINI_MUTEX(server->vnc_sessions_list_mtx);
  FREE(server);
  printf("CLEANUP DONE!\n");
}

void proxy_server_add_vnc_session(proxy_server *server, vnc_session *vnc_sess)
{
  if (server == NULL || vnc_sess == NULL)
    return;

  LOCK(server->vnc_sessions_list_mtx);
  vnc_sess->next = server->vnc_sessions_list_head;
  if (server->vnc_sessions_list_head != NULL)
    server->vnc_sessions_list_head->prev = vnc_sess;
  server->vnc_sessions_list_head = vnc_sess;
  UNLOCK(server->vnc_sessions_list_mtx);
}

void proxy_server_remove_vnc_session(proxy_server *server, vnc_session *vnc_sess)
{
  vnc_session *i = NULL;
  if (server == NULL || vnc_sess == NULL)
    return;

  LOCK(server->vnc_sessions_list_mtx);
  for (i = server->vnc_sessions_list_head; i != NULL; i = i->next)
  {
    if (i == vnc_sess)
    {
      if (i->next != NULL)
        i->next->prev = i->prev;
      if (i->prev != NULL)
        i->prev->next = i->next;
      if (i == server->vnc_sessions_list_head)
        server->vnc_sessions_list_head = i->next;
      break;
    }
  }
  UNLOCK(server->vnc_sessions_list_mtx);
}

static void add_client(proxy_server *server, proxy_client *client)
{
  if (server == NULL || client == NULL)
    return;

  LOCK(server->client_list_mtx);
  client->next = server->client_list_head;
  client->prev = NULL;
  if (server->client_list_head != NULL)
    server->client_list_head->prev = client;
  server->client_list_head = client;
  server->clients_count++;
  UNLOCK(server->client_list_mtx);
}

static void remove_client(proxy_server *server, proxy_client *client)
{
  if (server == NULL || client == NULL)
    return;

  LOCK(server->client_list_mtx);
  if (client->prev)
    client->prev->next = client->next;
  else
    server->client_list_head = client->next;
  if (client->next)
    client->next->prev = client->prev;
  server->clients_count--;
  UNLOCK(server->client_list_mtx);
}

static proxy_client *search_client(proxy_server *server, unsigned char *sessid)
{
  if (server == NULL || sessid == NULL)
    return NULL;

  LOCK(server->client_list_mtx);

  proxy_client *iter_cl = server->client_list_head;
  for (iter_cl = server->client_list_head; iter_cl; iter_cl = iter_cl->next)
  {
    if (strcmp(sessid, iter_cl->cl.c_sessid) == 0)
    {
      printf("FOUND: %d %s %s %s\n",
             iter_cl->client_sock,
             iter_cl->client_addr,
             iter_cl->cl.c_sessid,
             iter_cl->cl.c_passwd);
      break;
    }
  }
  UNLOCK(server->client_list_mtx);

  return iter_cl;
}

static proxy_client *make_new_client(proxy_server *server, int client_socket, char *addr)
{
  int one = 1;
  proxy_client *new_client = NULL;

  if (server == NULL)
    return NULL;

  new_client = (proxy_client *)calloc(1, sizeof(proxy_client));
  if (new_client == NULL)
    return NULL;

  new_client->client_sock = client_socket;
  new_client->client_addr = addr;
  new_client->client_data = (void *)server;

  strcpy(new_client->cl.c_ip, addr);

  if (setsockopt(client_socket, IPPROTO_TCP, TCP_NODELAY, (char *)&one, sizeof(one)) < 0)
    perror("setsockopt failed: can't set TCP_NODELAY flag, non TCP socket?");

  if (fcntl(client_socket, F_SETFL, O_NONBLOCK) < 0)
    perror("setsockopt failed: can't set NON BLOCKING SOCKET");

  new_client->cl.c_id = 1;
  strcpy(new_client->cl.c_ip, new_client->client_addr);
  generate_sessid(new_client->cl.c_sessid, SESSID_LEN);
  generate_passwd(new_client->cl.c_passwd, PASSWD_LEN);

  printf("SESSID: %s, PASSWD: %s\n\n", new_client->cl.c_sessid, new_client->cl.c_passwd);

  // if (db_select_client(server->db_conn, &new_client->cl) == SQL_OK)
  // {
  // //   update_db(server, new_client);

  // //   printf("-----------------------\n");
  // //   printf("\033[32mFound\033[00m\n");
  // //   printf("#%d\nIP: %s\nCLIENT_ID: %s\nLAST_CONNECT: %s\n",
  // //          new_client->id,
  // //          new_client->ip,
  // //          new_client->client_id,

  // //          new_client->last_connect);
  // //   printf("-----------------------\n");
  //   printf("FOUND\n");
  //   if (db_update_client(server->db_conn, &new_client->cl) != SQL_OK)
  //   {
  //     printf("Eroare\n\n");
  //   }
  // }
  // else
  // {
  //   // insert db
  //   printf("NOP\n");
  //   if (db_insert_new_client(server->db_conn, new_client->cl.c_ip) != SQL_OK)
  //   {
  //     printf("Client failed to insert into db: %s\n", server->db_conn->errmsg);
  //   }
  //   else
  //   {
  //     printf("Client inserted into db!\n");
  //   }

  //   if (db_select_client(server->db_conn, &new_client->cl) != SQL_OK)
  //   {
  //     printf("Eroare\n\n");
  //   }
  // }

  add_client(server, new_client);

  assert(pthread_mutexattr_init(&new_client->mta) == 0);
  pthread_mutexattr_settype(&new_client->mta, PTHREAD_MUTEX_RECURSIVE);

  assert(INIT_MUTEX(new_client->client_write_mtx) == 0);
  // assert(INIT_MUTEX(new_client->client_update_mtx) == 0);
  assert(INIT_MUTEX_ATTR(new_client->client_update_mtx, new_client->mta) == 0);

  ProxyInitMsg msg;
  msg.type = INIT_MESSAGE;
  strcpy(msg.sessid, new_client->cl.c_sessid);
  strcpy(msg.passwd, new_client->cl.c_passwd);

  proxy_client_write_exact_timeout(new_client, &msg, sz_ProxyInitMsg, 500);

  return new_client;
}

static void close_client(proxy_client *client)
{
  if (client == NULL)
    return;

  vnc_session_proxy_client_closed(client->vnc_sess_ctx, client);
  CLOSE_FD(client->client_sock);
  remove_client(client->client_data, client);
}

static void client_connection_gone(proxy_client *client)
{
  if (client == NULL)
    return;

  // free everything
  TINI_MUTEX(client->client_write_mtx);
  TINI_MUTEX(client->client_update_mtx);
  FREE(client);
}

/*
  * proxy_client_write_exact_timeout writes an exact number of bytes to a client.  
  * Returns 1 if those bytes have been written, or -1 if an error occurred 
  * (errno is set to ETIMEDOUT if it timed out).
  */
int proxy_client_write_exact_timeout(proxy_client *cl, char *buf, int len, int timeout)
{
  int sock = cl->client_sock;
  int n;
  fd_set fds;
  struct timeval tv;
  int totalTimeWaited = 0;

#ifdef DEBUG_WRITE_EXACT
  fprintf(stderr, "proxy_client_write_exact_timeout: %d bytes\n", len);
  for (n = 0; n < len; n++)
    fprintf(stderr, "%02x ", (unsigned char)buf[n]);
  fprintf(stderr, "\n");
#endif
  LOCK(cl->client_write_mtx);
  while (len > 0)
  {
    n = write(sock, buf, len);

    if (n > 0)
    {
      printf("sent %d remaining: %d\n", n, (len - n));
      buf += n;
      len -= n;
    }
    else if (n == 0)
    {
      fprintf(stderr, "proxy_client_write_exact_timeout: write returned 0?\n");
      return 0;
    }
    else
    {
      if (errno == EINTR)
        continue;

      if (errno != EWOULDBLOCK && errno != EAGAIN)
      {
        UNLOCK(cl->client_write_mtx);
        return n;
      }

      /* Retry every 5 seconds until we exceed timeout.  We
                need to do this because select doesn't necessarily return
                immediately when the other end has gone away */

      FD_ZERO(&fds);
      FD_SET(sock, &fds);
      tv.tv_sec = 5;
      tv.tv_usec = 0;
      n = select(sock + 1, NULL, &fds, NULL /* &fds */, &tv);
      if (n < 0)
      {
        if (errno == EINTR)
          continue;
        rfbLogPerror("WriteExact: select");
        UNLOCK(cl->client_write_mtx);
        return n;
      }
      if (n == 0)
      {
        fprintf(stderr, "1 timeout exceeded\n");
        totalTimeWaited += 5000;
        if (totalTimeWaited >= timeout)
        {
          errno = ETIMEDOUT;
          UNLOCK(cl->client_write_mtx);
          return -1;
        }
      }
      else
      {
        totalTimeWaited = 0;
      }
    }
  }
  UNLOCK(cl->client_write_mtx);
  return 1;
}

/*
  * proxy_client_read_exact_timeout reads an exact number of bytes from a client.  
  * Returns 1 if those bytes have been read, 0 if the other end has closed, 
  * or -1 if an error occurred (errno is set to ETIMEDOUT if it timed out).
  */
int proxy_client_read_exact_timeout(proxy_client *cl, char *buf, int len, int timeout)
{
  int sock = cl->client_sock;
  int n;
  fd_set fds;
  struct timeval tv;
  int bytes_read = 0;
  char *buf_out;

  buf_out = buf;
  while (len > 0)
  {
    n = read(sock, buf_out, len);

    if (n > 0)
    {
      buf_out += n;
      len -= n;
      bytes_read += n;
    }
    else if (n == 0)
    {
      return 0;
    }
    else
    {
      if (errno == EINTR)
        continue;

      if (errno != EWOULDBLOCK && errno != EAGAIN)
      {
        return n;
      }

      FD_ZERO(&fds);
      FD_SET(sock, &fds);
      tv.tv_sec = timeout / 1000;
      tv.tv_usec = (timeout % 1000) * 1000;
      n = select(sock + 1, &fds, NULL, &fds, &tv);
      if (n < 0)
      {
        fprintf(stderr, "ReadExact: select");
        return n;
      }
      if (n == 0)
      {
        fprintf(stderr, "ReadExact: select timeout FOR: %s \n", cl->cl.c_sessid);
        errno = ETIMEDOUT;
        return -1;
      }
    }
  }

#ifdef DEBUG_READ_EXACT
  printf("proxy_client_read_exact_timeout %d bytes\n", bytes_read); //len);
  for (n = 0; n < bytes_read; n++)
    fprintf(stderr, "%02x ", (unsigned char)buf[n]);
  fprintf(stderr, "\n");
#endif
  return 1;
}

static void
proxy_process_client_message(proxy_client *cl)
{
  int n = 0;
  ProxyMsg msg;
  char *str;
  int i;

  if ((n = proxy_client_read_exact_timeout(cl, (char *)&msg, 1, 10000)) <= 0)
  {
    if (n != 0)
      fprintf(stderr, "proxy_process_client_message: read\n");
    close_client(cl);
    return;
  }

  switch (msg.type)
  {
  case INIT_MESSAGE:
    if ((n = proxy_client_read_exact_timeout(cl, ((char *)&msg) + 1, sz_ProxyInitMsg - 1, 10000)) <= 0)
    {
      if (n != 0)
        fprintf(stderr, "proxy_process_client_message: read");
      close_client(cl);
      return;
    }
    break;
  case REQUEST_CONNECTION:
    if ((n = proxy_client_read_exact_timeout(cl, ((char *)&msg) + 1, sz_ProxyReqConnMsg - 1, 10000)) <= 0)
    {
      if (n != 0)
        fprintf(stderr, "proxy_process_client_message: read");
      close_client(cl);
      return;
    }

    ProxyReqConnMsg *rcm = (ProxyReqConnMsg *)&msg;
    proxy_server *server = (proxy_server *)cl->client_data;
    proxy_client *oth_cl = NULL;
    ProxyAskForConnectMsg m;

    if ((oth_cl = search_client(server, rcm->sessid)) == NULL)
    {
      printf("Client with sessid: %s was not found!\n", rcm->sessid);
      m.type = REQUEST_CONNECTION_FAIL;
      proxy_client_write_exact_timeout(cl, &m, sz_ProxyAskForConnectMsg, 500);
      return;
    }

    if (oth_cl == cl)
    {
      m.type = REQUEST_CONNECTION_FAIL;
      proxy_client_write_exact_timeout(cl, &m, sz_ProxyAskForConnectMsg, 500);
      return;
    }

    LOCK(oth_cl->client_update_mtx);
    if (oth_cl->vnc_sess_ctx != NULL &&
        vnc_session_proxy_client_have_host_connection(oth_cl->vnc_sess_ctx, oth_cl))
    {
      UNLOCK(oth_cl->client_update_mtx);
      m.type = REQUEST_CONNECTION_FAIL;
      proxy_client_write_exact_timeout(cl, &m, sz_ProxyAskForConnectMsg, 500);
      return;
    }

    oth_cl->need_response = TRUE;

    m.type = ASK_FOR_CONNECT;
    if (proxy_client_write_exact_timeout(oth_cl, (char *)&m, sz_ProxyAskForConnectMsg, 500) == -1)
    {
      UNLOCK(oth_cl->client_update_mtx);
      m.type = REQUEST_CONNECTION_FAIL;
      proxy_client_write_exact_timeout(cl, &m, sz_ProxyAskForConnectMsg, 500);
      return;
    }
    if (proxy_client_read_exact_timeout(oth_cl, (char *)&m, sz_ProxyAskForConnectMsg, 10000) == -1)
    {
      UNLOCK(oth_cl->client_update_mtx);
      m.type = REQUEST_CONNECTION_FAIL;
      proxy_client_write_exact_timeout(cl, &m, sz_ProxyAskForConnectMsg, 500);
      return;
    }
    if (m.type != APPROVE_FOR_CONNECT)
    {
      UNLOCK(oth_cl->client_update_mtx);
      printf("Client: %s declined\n", oth_cl->client_addr);
      m.type = DECLINE_FOR_CONNECT;
      proxy_client_write_exact_timeout(cl, &m, sz_ProxyAskForConnectMsg, 500);
      return;
    }

    if (oth_cl->vnc_sess_ctx != NULL)
    {
      // Already in session and client is already host
      vnc_session *vnc_sess;
      if (vnc_session_init_proxy_connection(oth_cl, cl) == -1)
      {
        UNLOCK(oth_cl->client_update_mtx);
        m.type = DECLINE_FOR_CONNECT;
        proxy_client_write_exact_timeout(cl, &m, sz_ProxyAskForConnectMsg, 500);
        return;
      }
    }
    else
    {
      vnc_session *new_vnc_session;

      if (!(new_vnc_session = vnc_session_init_proxy_connection(oth_cl, cl)))
      {
        UNLOCK(oth_cl->client_update_mtx);
        m.type = REQUEST_CONNECTION_FAIL;
        proxy_client_write_exact_timeout(cl, &m, sz_ProxyAskForConnectMsg, 500);
        return;
      }

      proxy_server_add_vnc_session(server, new_vnc_session);
    }

    UNLOCK(oth_cl->client_update_mtx);
    break;
  default:
    fprintf(stderr, "UNKNOWN MESSAGE TYPE!\n");
    break;
  }
}

static void *client_input(void *data)
{
  proxy_client *cl = (proxy_client *)data;
  fd_set rfds, wfds, efds;
  struct timeval tv;
  int n;

  for (;;)
  {
    if (cl->client_sock == -1)
      /* Client has disconnected. */
      break;

    // clear file descriptors
    FD_ZERO(&rfds);
    FD_ZERO(&efds);
    // FD_ZERO(&wfds);

    // set file descriptors for select
    FD_SET(cl->client_sock, &rfds);
    //FD_SET(cl->client_sock, &wfds); 7
    FD_SET(cl->client_sock, &efds);

    tv.tv_sec = 60; /* 1 minute */
    tv.tv_usec = 0;

    n = select(cl->client_sock + 1, &rfds, &wfds, &efds, &tv);

    if (n < 0)
    {
      perror("client_input: select");
      break;
    }

    if (n == 0) /* timeout */
    {
      //rfbSendFileTransferChunk(cl);
      continue;
    }

    /* We have some space on the transmit queue, send some data */
    if (FD_ISSET(cl->client_sock, &wfds))
      continue;

    //rfbSendFileTransferChunk(cl);
    if (FD_ISSET(cl->client_sock, &rfds) || FD_ISSET(cl->client_sock, &efds))
    {
      LOCK(cl->client_update_mtx);
      if (!cl->need_response)
      {
        proxy_process_client_message(cl);
      }
      cl->need_response = FALSE;
      UNLOCK(cl->client_update_mtx);
    }
  }

  client_connection_gone(cl);
  return NULL;
}

/* Main server loop */
void proxy_server_run(proxy_server *server)
{
  int client_fd;
  fd_set listen_fds; /* temp file descriptor list for select() */
  struct sockaddr_storage peer;
  socklen_t peer_addrlen;
  struct timeval tv;
  struct sockaddr_in inaddr;

  if (server == NULL)
    return;

  peer_addrlen = sizeof(inaddr);
  if (getsockname(server->sock, (struct sockaddr *)&inaddr, &peer_addrlen) == -1)
    perror("getsockname");

  printf("server running on: %s:%d\n", inet_ntoa(inaddr.sin_addr), ntohs(inaddr.sin_port));

  while (!signal_catched)
  {
    // clear file descriptors
    client_fd = -1;
    FD_ZERO(&listen_fds);

    // set file descriptors for select
    FD_SET(server->sock, &listen_fds);

    // 1.5 sec timeout
    tv.tv_sec = 1;
    tv.tv_usec = 500000;

    if (select(server->sock + 1, &listen_fds, NULL, NULL, &tv /* NULL = block*/) == -1)
    {
      fprintf(stderr, "proxy_server_run: error in select\n");
      break;
    }

    if (FD_ISSET(server->sock, &listen_fds))
    {
      ++server->connections_processed;

      peer_addrlen = sizeof(peer);
      client_fd = accept(server->sock, (struct sockaddr *)&peer, &peer_addrlen);

      if (client_fd >= 0)
      {
        struct sockaddr_in *addr = (struct sockaddr_in *)&peer;
        proxy_client *new_client = make_new_client(server, client_fd, inet_ntoa(addr->sin_addr));
        if (new_client == NULL)
          CLOSE_FD(client_fd);
        else
        {
          pthread_create(&new_client->client_thread, NULL, client_input, (void *)new_client);
        }
      }
    }

#ifdef DEBUG_LIST_SHOW
    proxy_client_iterator *i = NULL;
    proxy_client *client = NULL;

    vnc_session_iterator *vnc_sess_iter = NULL;
    vnc_session *vnc_sess = NULL;

    printf("############## Client list %d  ##############\n",
           server->clients_count,
           server->connections_processed);
    for (i = get_client_iterator(server), client = client_iterator_head(i);
         client != NULL; client = client_iterator_next(i))
    {
      printf("%d %s %s %s\n",
             client->client_sock,
             client->client_addr,
             client->cl.c_sessid,
             client->cl.c_passwd);

      LOCK(client->client_update_mtx);
      if (client->vnc_sess_ctx != NULL)
      {
        if (client->vnc_sess_ctx->host_connection_list_head == NULL ||
            client->vnc_sess_ctx->connection_list_head == NULL)
        {
          client->vnc_sess_ctx = NULL;
        }
      }
      UNLOCK(client->client_update_mtx);
    }
    release_client_iterator(i);

    if (server->vnc_sessions_list_head == NULL)
      printf("No vnc sessions\n");
    else
    {
      for (vnc_sess_iter = get_vnc_session_iterator(server),
          vnc_sess = vnc_session_iterator_head(vnc_sess_iter);
           vnc_sess != NULL;
           vnc_sess = vnc_session_iterator_next(vnc_sess_iter))
      {
        vnc_session_print(vnc_sess);

        if (vnc_sess->host_connection_list_head == NULL ||
            vnc_sess->connection_list_head == NULL)
        {
          printf("\033[32m Needs to be closed \033[00m\n");

          proxy_server_remove_vnc_session(server, vnc_sess);
          free(vnc_sess);
        }
      }
      release_vnc_session_iterator(vnc_sess_iter);
    }
    printf("##############################################\n");
#endif
  }
}
/* vim: set ts=2 sts=2 sw=2: */
