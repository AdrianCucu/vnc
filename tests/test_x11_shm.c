#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <signal.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xrandr.h>
#include <X11/extensions/XShm.h>

#include "X11_utils.h"
#include "constants.h"
#include "bitmap.h"
#include "tv.h"

// #define USE_SHM 1

#define SHM_NOT_AVAILABLE 0
#define HAVE_SHM_XIMAGE 1
#define HAVE_SHM_PIXMAP 2

/*
 * Check if the X Shared Memory extension is available.
 * Return:  0 = not available
 *          1 = shared XImage support available
 *          2 = shared Pixmap support available also
 */
int check_for_xshm(Display *dpy)
{
  int major, minor, ignore;
  Bool pixmaps;

  if (XQueryExtension(dpy, "MIT-SHM", &ignore, &ignore, &ignore))
    if (XShmQueryVersion(dpy, &major, &minor, &pixmaps))
      return pixmaps ? HAVE_SHM_PIXMAP : HAVE_SHM_XIMAGE;
  return SHM_NOT_AVAILABLE;
}

/*
 * Error handling.
 */
static int ErrorFlag = 0;

static int HandleXError(Display *dpy, XErrorEvent *err)
{
  char xerror[1024];
  XGetErrorText(dpy, err->error_code, xerror, 1024);
  fprintf(stderr, "Error: %s\n", xerror);
  ErrorFlag = 1;
  return 0;
}

/*
 * Allocate a shared memory XImage.
 */
XImage *alloc_xshm_image(Display *dpy,
                         Visual *vis,
                         int width,
                         int height,
                         int depth,
                         XShmSegmentInfo *shminfo)
{
  XImage *img;

  assert(dpy);
  assert(vis);
  assert(shminfo);

  /*
    * We have to do a _lot_ of error checking here to be sure we can
    * really use the XSHM extension.  It seems different servers trigger
    * errors at different points if the extension won't work.  Therefore
    * we have to be very careful...
    */

  img = XShmCreateImage(dpy, vis, depth,
                        ZPixmap, NULL, shminfo,
                        width, height);
  if (img == NULL)
  {
    printf("XShmCreateImage failed!\n");
    return NULL;
  }

  shminfo->shmid = shmget(IPC_PRIVATE, img->bytes_per_line * img->height, IPC_CREAT | 0600);
  if (shminfo->shmid < 0)
  {
    perror("shmget");
    XDestroyImage(img);
    return NULL;
  }

  shminfo->shmaddr = img->data = (char *)shmat(shminfo->shmid, 0, 0);
  if (shminfo->shmaddr == (char *)-1)
  {
    perror("alloc_back_buffer");
    XDestroyImage(img);
    img = NULL;
    return NULL;
  }

  shminfo->readOnly = False;
  ErrorFlag = 0;
  XSetErrorHandler(HandleXError);
  /* This may trigger the X protocol error we're ready to catch: */
  XShmAttach(dpy, shminfo);
  XSync(dpy, False);

  if (ErrorFlag)
  {
    /* we are on a remote display, this error is normal, don't print it */
    XFlush(dpy);
    ErrorFlag = 0;
    XDestroyImage(img);
    shmdt(shminfo->shmaddr);
    shmctl(shminfo->shmid, IPC_RMID, 0);
    return NULL;
  }

  shmctl(shminfo->shmid, IPC_RMID, 0); /* nobody else needs it */
  return img;
}

void destroy_xshm(Display *dpy, XShmSegmentInfo *shminfo)
{
  XShmDetach(dpy, shminfo);
  shmdt(shminfo->shmaddr);
}

static int caught_signal = 0;

void sighandler(int sig)
{
  caught_signal = 1;
}

/* Entry point */
int main(int argc, char **argv)
{
  Display *dpy;
  Window root;
  XImage *xim;

  int width, height;
  int scr;
  int depth;
  
  struct tv_avg avg = INIT_TV_AVG;
  struct timeval tvstart, tvstop;
  double ms;

  signal(SIGHUP, sighandler);
  signal(SIGINT, sighandler);
  signal(SIGTERM, sighandler);

  // Init X11 connection with X server
  dpy = XOpenDisplay(getenv("DISPLAY"));
  if (dpy == NULL)
  {
    fprintf(stderr, "Cannot connect to X server\n");
    exit(EXIT_FAILURE);
  }

  scr = DefaultScreen(dpy);
  width = DisplayWidth(dpy, scr);
  height = DisplayHeight(dpy, scr);
  depth = DefaultDepth(dpy, scr);
  Visual *vis = DefaultVisual(dpy, scr);

#if defined(USE_SHM)
  XShmSegmentInfo shminfo;
  int check_shm = check_for_xshm(dpy);
  if (check_shm == SHM_NOT_AVAILABLE)
  {
    fprintf(stderr, "MITM-SHM extension is not available\n");
    XCloseDisplay(dpy);
    exit(EXIT_FAILURE);
  }
  printf("Let's go with" COLOR_GREEN " MITM-SHM!\n" COLOR_NONE);

  /* make shared XImage */
  xim = alloc_xshm_image(dpy, vis, width, height, depth, &shminfo);
  if (!xim)
  {
    fprintf(stderr, "couldn't allocate shared XImage\n");
    XCloseDisplay(dpy);
    exit(EXIT_FAILURE);
  }
#else
  /* NO SHM */
  printf("Let's go without" COLOR_RED " MITM-SHM!\n" COLOR_NONE);
  xim = XCreateImage(dpy, vis, depth, ZPixmap, 0, 0, width, height, BitmapPad(dpy), 0);
  xim->data = malloc(xim->bytes_per_line * xim->height);
#endif

  int iter = 1000;
  for (; !caught_signal && iter--;)
  {
    gettimeofday(&tvstart, NULL);

#if defined(USE_SHM)
    /* WITH SHM*/
    if (!XShmGetImage(dpy, DefaultRootWindow(dpy), xim, 0, 0, AllPlanes))
    {
      fprintf(stderr, "Something went wrong\n");
      break;
    }
#else
    /* NO SHM */
    xim = XGetSubImage(dpy,
                       DefaultRootWindow(dpy),
                       0, 0,
                       width, height,
                       AllPlanes, ZPixmap,
                       xim, 0, 0);
#endif

    gettimeofday(&tvstop, NULL);
    tvsub(&tvstop, &tvstart);
    add_to_avg(&avg, &tvstop);
  }

  get_avg(&avg, &tvstop);
  ms = tvstop.tv_sec * 1000 + tvstop.tv_usec / 1000.0;
  printf("Average: %f ms\n", ms);

  // Release resources
#if defined(USE_SHM)
  destroy_xshm(dpy, &shminfo);
#endif
  XDestroyImage(xim);
  XCloseDisplay(dpy);
  return 0;
}