#ifdef WIN32
#define sleep Sleep
#else
#include <unistd.h>
#endif

#ifdef __IRIX__
#include <netdb.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>
#include <signal.h>

#include <rfb/rfb.h>
#include <rfb/keysym.h>
#include <rfb/rfbregion.h>

#include <xcb/xcb.h>
#include <xcb/shm.h>
#include <xcb/xfixes.h>
#include <xcb/xcb_cursor.h>
#include <xcb/xcb_keysyms.h>
#include <xcb/xtest.h>
#include <xcb/xcb_image.h>
#include <xkbcommon/xkbcommon.h>
#include <xkbcommon/xkbcommon-x11.h>

#include <X11/Xlib.h>
#include <X11/XKBlib.h>
#include <X11/Xlib-xcb.h>

#include "log.h"
#include "tv.h"
#include "bitmap.h"
#include "pixelformat.h"
#include "constants.h"
#include "X11_utils.h"
#include "cursor.h"

#define STREQUAL(str1, str2) (strcmp(str1, str2) == 0)

static int caught_signal = 0;

void sighandler(int sig)
{
  caught_signal = 1;
}

#define SHM_NOT_AVAILABLE 0
#define HAVE_SHM_XIMAGE 1
#define HAVE_SHM_PIXMAP 2

/*
 * Check if the X Shared Memory extension is available.
 * Return:  0 = not available
 *          1 = shared XImage support available
 *          2 = shared Pixmap support available also
 */
int check_for_xshm(xcb_connection_t *con)
{
  xcb_shm_query_version_reply_t *reply;

  reply = xcb_shm_query_version_reply(con, xcb_shm_query_version(con), NULL);
  if (!reply || !reply->shared_pixmaps)
    return SHM_NOT_AVAILABLE;
  if (reply->shared_pixmaps)
    return HAVE_SHM_PIXMAP;
  return SHM_NOT_AVAILABLE;
}

void alloc_xshm(xcb_connection_t *con,
                xcb_visualid_t vis,
                int width,
                int height,
                int bytes_per_pixel,
                xcb_shm_segment_info_t *shminfo)
{
  assert(con);
  assert(shminfo);

  shminfo->shmid = shmget(IPC_PRIVATE, width * height * bytes_per_pixel, IPC_CREAT | 0600);
  shminfo->shmaddr = shmat(shminfo->shmid, 0, 0);

  shminfo->shmseg = xcb_generate_id(con);
  xcb_shm_attach(con, shminfo->shmseg, shminfo->shmid, 0);
  shmctl(shminfo->shmid, IPC_RMID, 0);
}

void destroy_xshm(xcb_connection_t *conn, xcb_shm_segment_info_t *shminfo)
{
  xcb_shm_detach(conn, shminfo->shmseg);
  shmdt(shminfo->shmaddr);
}

typedef struct
{
  INT32 x, y;
} Point;

typedef struct
{
  INT32 width;
  INT32 height;
} Geometry;

typedef struct _XCBVNCServer
{
  Geometry geo;
  xcb_connection_t *conn;
  xcb_screen_t *screen;
  rfbScreenInfoPtr rfbPtr;

  BOOL use_shm; // use shared memory extension
#if defined(USE_SHM)
  xcb_shm_segment_info_t shminfo;
#endif

  Display *dpy;
  xcursor_t *xcursor;

} XCBVNCServer, *XCBVNCServerPtr;

static XCBVNCServerPtr xcb_vnc_server;

/* Here we create a structure so that every client has its own pointer */
typedef struct __client_data
{
  rfbBool oldButton;
  int oldx, oldy;
  XCBVNCServerPtr server; // owner of this client
} client_data;

static void clientgone(rfbClientPtr cl)
{
  printf("Client: %s disconnected\n", cl->host);

  free(cl->clientData);
  cl->clientData = NULL;
}

static enum rfbNewClientAction newclient(rfbClientPtr cl)
{
  cl->clientData = (void *)calloc(sizeof(client_data), 1);
  client_data *data = (client_data *)cl->clientData;
  data->server = xcb_vnc_server;
  cl->clientGoneHook = clientgone;
  return RFB_CLIENT_ACCEPT;
}

// uint8_t thing_to_keycode(xcb_connection_t *c, char *thing)
// {
//   xcb_keycode_t kc;
//   xcb_keysym_t ks;

// #if 0   /* There is no XCB equivalent to XStringToKeysym */
//   ks = XStringToKeysym( thing );
//   if( ks == NoSymbol ){
//     fprintf( stderr, "Unable to resolve keysym for '%s'\n", thing );
//     return( thing_to_keycode( c, "space" ) );
//   }
// #else
//   /* For now, assume thing[0] == Latin-1 keysym */
//   ks = (uint8_t)thing[0];
// #endif

//   kc = xcb_key_symbols_get_keycode( syms, ks );

//   dmsg( 1, "String '%s' maps to keysym '%d'\n", thing, ks );
//   dmsg( 1, "String '%s' maps to keycode '%d'\n", thing, kc );

//   return( kc );
// }

// xcb_key_symbols_t *syms = NULL;
// uint8_t thing_to_keycode(xcb_connection_t *c, char *thing)
// {
//   xcb_keycode_t kc;
//   xcb_keysym_t ks;

// #if 0 /* There is no XCB equivalent to XStringToKeysym */
//   ks = XStringToKeysym( thing );
//   if( ks == NoSymbol ){
//     fprintf( stderr, "Unable to resolve keysym for '%s'\n", thing );
//     return( thing_to_keycode( c, "space" ) );
//   }
// #else
//   /* For now, assume thing[0] == Latin-1 keysym */
//   ks = (uint8_t)thing[0];
// #endif

//   kc = xcb_key_symbols_get_keycode(syms, ks);

//   // dmsg( 1, "String '%s' maps to keysym '%d'\n", thing, ks );
//   // dmsg( 1, "String '%s' maps to keycode '%d'\n", thing, kc );

//   return (kc);
// }

// static void
// setup_xkb(xcb_connection_t *conn)
// {
//     {
//         const xcb_query_extension_reply_t *reply;

//         reply = xcb_get_extension_data(conn, &xcb_xkb_id);
//         if (!reply)
//             errx(1, "no XKB in X server");
//     }

//     {
//         xcb_xkb_use_extension_cookie_t cookie;
//         _cleanup_free_ xcb_xkb_use_extension_reply_t *reply = NULL;

//         cookie = xcb_xkb_use_extension(conn,
//                                         XCB_XKB_MAJOR_VERSION,
//                                         XCB_XKB_MINOR_VERSION);
//         reply = xcb_xkb_use_extension_reply(conn, cookie, NULL);
//         if (!reply)
//             errx(1, "couldn't use XKB extension");

//         if (!reply->supported)
//             errx(1, "the XKB extension is not supported in X server");
//     }
// }

static void on_key_event(rfbBool down, rfbKeySym keysym, rfbClientPtr cl)
{
  // xcb_keysym_t ksym;
  // xcb_keycode_t keycode;
  // xcb_key_symbols_t *syms = xcb_key_symbols_alloc(conn);
  // keycode = xcb_key_symbols_get_keycode(syms, keysym);

  // Display *dpy = XOpenDisplay(NULL);
  // int keycode = XKeysymToKeycode(dpy, keysym);
  // XCloseDisplay(dpy);
  //https://cgit.freedesktop.org/xcb/demo/tree/app/xte/xte.c
  //printf("String '%c' maps to keysym '%d'\n", keysym, kc);

  //return;
  // if (down)
  //   xcb_test_fake_input(conn, XCB_KEY_PRESS, keycode, XCB_CURRENT_TIME, XCB_NONE, 0, 0, 0);
  // else
  //   xcb_test_fake_input(conn, XCB_KEY_RELEASE, keycode, XCB_CURRENT_TIME, XCB_NONE, 0, 0, 0);

  // xcb_key_symbols_t *keys_symbols;

  // keys_symbols = xcb_key_symbols_alloc(conn);

  // xcb_keysym_t symbol = xcb_key_press_lookup_keysym(m_keySymbols, e, 0);

  // xcb_key_symbols_free(keys_symbols);

  // if (down)
  // {
  //   int keycode = XKeysymToKeycode(g_dpy, keysym);
  //   xcb_keysym_t x;
  //   x
  //       XSync(g_dpy, False);
  //   XTestFakeKeyEvent(g_dpy, keycode, down, 0L);
  //   XFlush(g_dpy);
  //   XTestFakeKeyEvent(g_dpy, keycode, 0, 0L);
  //   XFlush(g_dpy);
  //   XTestGrabControl(g_dpy, False);

  //   printf("[*] Key Down event: %c\r\n", keysym);
  // }
  // else
  // {
  //   printf("[*] Key Up event: %c\r\n", keysym);
  // }
}

static void on_cursor_event(int buttonMask, int x, int y, rfbClientPtr cl)
{
  client_data *data = (client_data *)cl->clientData;
  // http://manpages.ubuntu.com/manpages/bionic/man3/xcb_warp_pointer.3.html
  xcb_warp_pointer(data->server->conn,
                   XCB_NONE,
                   data->server->screen->root,
                   0, 0,
                   0, 0,
                   x, y);

  if (buttonMask != data->oldButton)
  {
    for (int i = 0; i < 8; i++)
    {
      if ((buttonMask ^ data->oldButton) & (1 << i))
        if (buttonMask & (1 << i))
          //XTestFakeButtonEvent(g_dpy, i + 1, True, CurrentTime);
          xcb_test_fake_input(data->server->conn, XCB_BUTTON_PRESS, i + 1, 0, NULL, 0, 0, 0);
        else
          // XTestFakeButtonEvent(g_dpy, i + 1, False, CurrentTime);
          xcb_test_fake_input(data->server->conn, XCB_BUTTON_RELEASE, i + 1, 0, NULL, 0, 0, 0);
    }
  }
  data->oldButton = buttonMask;
}

// BOOL init_X11NVCServerKeyboard(VNCServerPtr srv)
// {
//   ///////////////////////////////////////////////////////////////////////////////////////////////////////////
//   //https://superuser.com/questions/248517/show-keys-pressed-in-linux
//   // https://bharathisubramanian.wordpress.com/2010/03/14/x11-fake-key-event-generation-using-xtest-ext/
//   int xkbOpcode, xkbErrorBase;
//   int xkbEventBase;
//   int major, minor;
//   major = 1;
//   minor = 0;

//   if (!XkbQueryExtension(srv->display, &xkbOpcode, &xkbEventBase, &xkbErrorBase, &major, &minor))
//   {
//     FATAL("XKEYBOARD extension not present");
//     return FALSE;
//   }

//   DEBUG("Keyboard extension: %d.%d\n", major, minor);

//   XkbSelectEvents(srv->display, XkbUseCoreKbd, XkbIndicatorStateNotifyMask, XkbIndicatorStateNotifyMask);

// #define XDESKTOP_N_LEDS 3
//   static const char *ledNames[XDESKTOP_N_LEDS] = {
//       "Scroll Lock", "Num Lock", "Caps Lock"};
//   // figure out bit masks for the indicators we are interested in
//   for (int i = 0; i < XDESKTOP_N_LEDS; i++)
//   {
//     Atom a;
//     int shift;
//     Bool on;

//     a = XInternAtom(srv->display, ledNames[i], True);
//     if (!a || !XkbGetNamedIndicator(srv->display, a, &shift, &on, NULL, NULL))
//       continue;

//     //ledMasks[i] = 1u << shift;
//     if (on)
//       printf("Mask for '%s' is 0x%x \033[32m ON\033[00m\n", ledNames[i], 1u << shift);
//     else
//       printf("Mask for '%s' is 0x%x \033[31m OFF\033[00m\n", ledNames[i], 1u << shift);
//     //if (on)
//     //  ledState |= 1u << i;
//   }

//   // X11 unfortunately uses keyboard driver specific keycodes and provides no
//   // direct way to query this, so guess based on the keyboard mapping
//   XkbDescPtr desc = XkbGetKeyboard(srv->display, XkbAllComponentsMask, XkbUseCoreKbd);
//   if (desc && desc->names)
//   {
//     char *keycodes = XGetAtomName(srv->display, desc->names->keycodes);
//     printf("keycodes = %s\n", keycodes);
//     if (keycodes)
//     {

//       if (strncmp("evdev", keycodes, strlen("evdev")) == 0)
//       {
//         //codeMap = code_map_qnum_to_xorgevdev;
//         //codeMapLen = code_map_qnum_to_xorgevdev_len;
//         printf("Using evdev codemap\n");
//       }
//       else if (strncmp("xfree86", keycodes, strlen("xfree86")) == 0)
//       {
//         //codeMap = code_map_qnum_to_xorgkbd;
//         //codeMapLen = code_map_qnum_to_xorgkbd_len;
//         printf("Using xorgkbd codemap\n");
//       }
//       else
//       {
//         printf("Unknown keycode '%s', no codemap\n", keycodes);
//       }

//       XFree(keycodes);
//     }
//     else
//     {
//       //vlog.debug("Unable to get keycode map\n");
//     }
//   }
// }

XCBVNCServerPtr get_XCBVNCServer()
{
  XCBVNCServerPtr server = NULL;

  server = (XCBVNCServerPtr)malloc(sizeof(XCBVNCServer));
  if (!server)
  {
    fprintf(stderr, "malloc failed\n");
    return NULL;
  }

  // Init X11 server
  if ((server->dpy = XOpenDisplay(getenv("DISPLAY"))) == NULL)
  {
    FATAL("Cannot connect to X server %s\n", getenv("DISPLAY") ? getenv("DISPLAY") : "(default)");
    return NULL;
  }

  // Init XCB connection with X server
  server->conn = xcb_connect(NULL, NULL);
  if (xcb_connection_has_error(server->conn) > 0)
  {
    printf("Cannot open display\n");
    goto error;
  }

  server->screen = xcb_setup_roots_iterator(xcb_get_setup(server->conn)).data;
  if (server->screen == NULL)
  {
    fprintf(stderr, "Failed to obtain screen\n");
    goto error;
  }

  INT32 width = server->screen->width_in_pixels;
  INT32 height = server->screen->height_in_pixels;
  xcb_visualid_t vis = server->screen->root_visual;
  INT32 depth, bytes_per_pixel;

  server->geo.width = width;
  server->geo.height = height;

  DEBUG("Display geo: %dx%d", width, height);

  xcb_format_iterator_t it = xcb_setup_pixmap_formats_iterator(xcb_get_setup(server->conn));
  for (; it.rem; xcb_format_next(&it))
  {
    if (it.data->depth == server->screen->root_depth)
    {
      printf("bpp = %d, %d, %d\n", it.data->bits_per_pixel, it.data->depth, it.data->scanline_pad);
      depth = it.data->depth;
      bytes_per_pixel = it.data->bits_per_pixel / 8;
      if (bytes_per_pixel == 0)
        bytes_per_pixel = 1;
      break;
    }
  }
#if defined(USE_SHM)
  /* USE SHM */
  if (!check_for_xshm(server->conn))
  {
    fprintf(stderr, "MITM-SHM extension is not available\n");
    goto error;
  }
  printf("Let's go with" COLOR_GREEN " MITM-SHM!" COLOR_NONE "\n");

  alloc_xshm(server->conn, vis, width, height, bytes_per_pixel, &server->shminfo);
  uint32_t *data = server->shminfo.shmaddr;
#endif

  //init_X11NVCServerKeyboard(srv);

  server->rfbPtr = rfbGetScreen(NULL, NULL,
                                width, height,
                                depth / 3, /* bitsPerSample */
                                3,         /* samplesPerPixel */
                                bytes_per_pixel /* bytesPerPixel */);
  if (!server->rfbPtr)
  {
    FATAL("Failed to rfbGetScreen");
    goto error;
  }

  server->rfbPtr->serverFormat.bigEndian = 0; //XImageByteOrder(srv->display) == MSBFirst;
  server->rfbPtr->serverFormat.bitsPerPixel = bytes_per_pixel * 8;
  server->rfbPtr->serverFormat.redMax = 0xff;   //srv->xim->red_mask >> (ffs(srv->xim->red_mask) - 1);
  server->rfbPtr->serverFormat.greenMax = 0xff; //srv->xim->green_mask >> (ffs(srv->xim->green_mask) - 1);
  server->rfbPtr->serverFormat.blueMax = 0xff;  //srv->xim->blue_mask >> (ffs(srv->xim->blue_mask) - 1);
  server->rfbPtr->serverFormat.redShift = 16;   //ffs(srv->xim->red_mask) - 1;
  server->rfbPtr->serverFormat.greenShift = 8;  //ffs(srv->xim->green_mask) - 1;
  server->rfbPtr->serverFormat.blueShift = 0;   //ffs(srv->xim->blue_mask) - 1;
  server->rfbPtr->desktopName = "XCBVNC";
  server->rfbPtr->alwaysShared = TRUE;
  server->rfbPtr->ptrAddEvent = on_cursor_event;
  // server->rfbPtr->kbdAddEvent = on_key_event;
  server->rfbPtr->newClientHook = newclient;

  rfbCursorPtr cursor = NULL;

  cursor = (rfbCursorPtr)calloc(1, sizeof(rfbCursor));
  if (cursor == NULL)
  {
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////
  XFixesCursorImage *crs = XFixesGetCursorImage(server->dpy);
  if (crs == NULL)
  {
    fprintf(stderr, "Error\n");
  }

  server->xcursor = calloc(1, sizeof(xcursor_t));

  server->xcursor->width = crs->width;
  server->xcursor->height = crs->height;

  server->xcursor->x = crs->x;
  server->xcursor->y = crs->y;

  server->xcursor->xhot = crs->xhot;
  server->xcursor->yhot = crs->yhot;

  server->xcursor->name = strdup(crs->name);

  set_cursor_buf(server->xcursor, crs->width, crs->height, crs->pixels);
  // set_cursor(server->xcursor, crs->pixels);

  cursor->source = get_cursor_bitmap(server->xcursor);
  cursor->cleanupSource = TRUE;

  cursor->mask = get_cursor_bitmask(server->xcursor);
  cursor->cleanupMask = TRUE;

  cursor->alphaPreMultiplied = TRUE;
  cursor->cleanup = cursor->cleanupMask | cursor->cleanupRichSource | cursor->cleanupSource;
  cursor->width = crs->width;
  cursor->height = crs->height;
  cursor->xhot = crs->xhot;
  cursor->yhot = crs->yhot;
  cursor->foreRed = 0xffff;
  cursor->foreGreen = 0xffff;
  cursor->foreBlue = 0xffff;
  cursor->backRed = 0;
  cursor->backGreen = 0;
  cursor->backBlue = 0;

  rfbSetCursor(server->rfbPtr, cursor);

  // rfbLogEnable(0);
  rfbInitServer(server->rfbPtr);
  return server;

error:
  if (server->conn)
    xcb_disconnect(server->conn);
  if (server->rfbPtr)
    rfbScreenCleanup(server->rfbPtr);
  free(server);
  return NULL;
}

void run_XCBVNCServer(XCBVNCServerPtr server)
{
  xcb_generic_error_t *e = NULL;
  UINT32 last_frame_counter = 0, frame_counter = 0;
  UINT64 last_fps_timestamp = get_time_micro();

  /* this is the non-blocking event loop; a background thread is started */
  rfbRunEventLoop(server->rfbPtr, -1, TRUE);

  while (!caught_signal && rfbIsActive(server->rfbPtr))
  {
#if defined(USE_SHM)
    /* WITH SHM*/
    xcb_shm_get_image_cookie_t iq;
    xcb_shm_get_image_reply_t *img;

    iq = xcb_shm_get_image(server->conn,
                           server->screen->root,
                           0, 0,
                           server->screen->width_in_pixels,
                           server->screen->height_in_pixels,
                           ~0U,
                           XCB_IMAGE_FORMAT_Z_PIXMAP,
                           server->shminfo.shmseg,
                           0);
    img = xcb_shm_get_image_reply(server->conn, iq, &e);
    if (e)
    {
      fprintf(stderr, "Cannot get the image data "
                      "event_error: response_type:%d error_code:%d "
                      "sequence:%d resource_id:%d minor_code:%d major_code:%d.\n",
              e->response_type, e->error_code,
              e->sequence, e->resource_id, e->minor_code, e->major_code);
      free(e);
      break;
    }
    server->rfbPtr->frameBuffer = server->shminfo.shmaddr;
#else
    /* NO SHM */
    xcb_get_image_cookie_t iq;
    xcb_get_image_reply_t *img;

    iq = xcb_get_image(server->conn,
                       XCB_IMAGE_FORMAT_Z_PIXMAP,
                       server->screen->root,
                       0, 0,
                       server->screen->width_in_pixels,
                       server->screen->height_in_pixels,
                       ~0);
    img = xcb_get_image_reply(server->conn, iq, &e);
    if (e)
    {
      fprintf(stderr, "Cannot get the image data "
                      "event_error: response_type:%d error_code:%d "
                      "sequence:%d resource_id:%d minor_code:%d major_code:%d.\n",
              e->response_type, e->error_code,
              e->sequence, e->resource_id, e->minor_code, e->major_code);
      free(e);
      break;
    }
    server->rfbPtr->frameBuffer = xcb_get_image_data(img);
#endif
    // data = xcb_get_image_data(img);
    // length = xcb_get_image_data_length(img);

    rfbMarkRectAsModified(server->rfbPtr, 0, 0,
                          server->geo.width,
                          server->geo.height);

    XFixesCursorImage *crs = XFixesGetCursorImage(server->dpy);
    if (crs != NULL)
    {
      if (!STREQUAL(crs->name, server->xcursor->name))
      {
        if (server->xcursor->name)
          free(server->xcursor->name);
        if (server->xcursor->pixels)
          free(server->xcursor->pixels);

        server->xcursor->name = strdup(crs->name);
        server->xcursor->pixels = NULL;

        set_cursor_buf(server->xcursor, crs->width, crs->height, crs->pixels);

        server->xcursor->xhot = crs->xhot;
        server->xcursor->yhot = crs->yhot;

        rfbCursorPtr new_cursor = (rfbCursorPtr)calloc(1, sizeof(rfbCursor));
        if (new_cursor)
        {
          new_cursor->source = get_cursor_bitmap(server->xcursor);
          new_cursor->cleanupSource = TRUE;
          new_cursor->mask = get_cursor_bitmask(server->xcursor);
          new_cursor->cleanupMask = TRUE;

          new_cursor->alphaPreMultiplied = TRUE;
          new_cursor->cleanup = new_cursor->cleanupMask |
                                new_cursor->cleanupRichSource |
                                new_cursor->cleanupSource;
          new_cursor->width = crs->width;
          new_cursor->height = crs->height;
          new_cursor->xhot = crs->xhot;
          new_cursor->yhot = crs->yhot;
          new_cursor->foreRed = 0xffff;
          new_cursor->foreGreen = 0xffff;
          new_cursor->foreBlue = 0xffff;
          new_cursor->backGreen = 0;
          new_cursor->backRed = 0;
          new_cursor->backBlue = 0;

          rfbSetCursor(server->rfbPtr, new_cursor);
        }
      }

      server->xcursor->x = crs->x;
      server->xcursor->y = crs->y;
    }

    XFree(crs);

    if (img)
      free(img);

    ++frame_counter;
    INT64 timestamp = get_time_micro();
    INT64 time = timestamp - last_fps_timestamp;
    if (time > 1000000)
    {
      UINT32 frames = frame_counter - last_frame_counter;
      last_fps_timestamp = timestamp;
      last_frame_counter = frame_counter;
      printf("FPS: %.1f\n", frames / (time * 1.0e-6));
    }

    // breathe 50 miliseconds
    usleep(50000);
  }
}

void deinit_XCBVNCServer(XCBVNCServerPtr server)
{
  if (server == NULL)
    return;

#if defined(USE_SHM)
  destroy_xshm(server->conn, &server->shminfo);
#endif
  rfbScreenCleanup(server->rfbPtr);
  xcb_disconnect(server->conn);
  XCloseDisplay(server->dpy);
  PRINT_GREEN("\nCleanup done!\n\n");
}

/* Entry point */
int main(int argc, char **argv)
{
  // struct xkb_context *ctx;
  // struct xkb_keymap *keymap;

  // ctx = xkb_context_new(XKB_CONTEXT_NO_FLAGS);
  // if (!ctx)
  // {
  //   fprintf(stderr, "failed to create xkb ctx\n");
  //   exit(EXIT_FAILURE);
  // }

  // int32_t device_id;
  // device_id = xkb_x11_get_core_keyboard_device_id(conn);

  // if (device_id == -1)
  // {
  //   fprintf(stderr, "failed to obtain device id\n");
  //   exit(EXIT_FAILURE);
  // }

  // keymap = xkb_x11_keymap_new_from_device(ctx, conn, device_id,
  //                                         XKB_KEYMAP_COMPILE_NO_FLAGS);

  // if (!keymap)
  // {
  //   fprintf(stderr, "failed to get x11 keymap from device\n");
  //   exit(EXIT_FAILURE);
  // }

  // xcb_disconnect(conn);
  // return 0;

  char log_file_path[255];

  snprintf(log_file_path, sizeof(log_file_path), "%s.log", argv[0]);

  if (log_init(LOG_LEVEL_DEBUG, log_file_path, LOG_TIME_ADD) == FALSE)
  {
    fprintf(stderr, "Filed to init logger\n");
    exit(EXIT_FAILURE);
  }

  signal(SIGHUP, sighandler);
  signal(SIGINT, sighandler);
  signal(SIGTERM, sighandler);

  printf("logger has been initialised, logging to: %s\n", log_file_path);

  xcb_vnc_server = get_XCBVNCServer();
  if (!xcb_vnc_server)
  {
    FATAL("Failed to init X11 vnc server");
    exit(EXIT_FAILURE);
  }

  //log_VNCServer(xcb_vnc_server);
  run_XCBVNCServer(xcb_vnc_server);
  deinit_XCBVNCServer(xcb_vnc_server);
  return 0;
}