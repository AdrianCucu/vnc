#include "log.h"

int main(int argc, char **argv)
{
  log_init(LOG_LEVEL_ALL, "test.log", LOG_TIME_ADD);

  INFO("Just testing the logg");

  DEBUG("debug message");
  INFO("info message");
  WARN("warning message");
  ERROR("error message");
  FATAL("fatal message");

  return 0;
}
