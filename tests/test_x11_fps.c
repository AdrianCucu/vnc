#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <unistd.h>
#include <signal.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <xcb/xcb.h>

#include "X11_utils.h"
#include "bitmap.h"
#include "tv.h"

static int caught_signal = 0;

void sighandler(int sig)
{
  caught_signal = 1;
}

/* Entry point */
int main(int argc, char **argv)
{
  Display *dpy;
  XImage *xim;

  signal(SIGHUP, sighandler);
  signal(SIGINT, sighandler);
  signal(SIGTERM, sighandler);

  // Init X11 server
  dpy = XOpenDisplay(NULL);
  if (dpy == NULL)
  {
    fprintf(stderr, "Cannot connect to X server\n");
    exit(EXIT_FAILURE);
  }

  xim = XCreateImage(dpy,
                     DefaultVisual(dpy, DefaultScreen(dpy)),
                     DefaultDepth(dpy, DefaultScreen(dpy)),
                     ZPixmap, 0, 0,
                     DisplayWidth(dpy, DefaultScreen(dpy)),
                     DisplayHeight(dpy, DefaultScreen(dpy)),
                     BitmapPad(dpy), 0);

  if (xim == NULL)
  {
    fprintf(stderr, "Cannot create XImg\n");
    XCloseDisplay(dpy);
    exit(EXIT_FAILURE);
  }

  xim->data = malloc(xim->bytes_per_line * xim->height);
  if (xim->data == NULL)
  {
    fprintf(stderr, "Cannot malloc img data\n");
    XFree(xim);
    XCloseDisplay(dpy);
    exit(EXIT_FAILURE);
  }

  UINT32 last_frame_counter = 0, frame_counter = 0;
  UINT64 last_fps_timestamp = get_time_micro();

  while (!caught_signal)
  {
    XGetSubImage(dpy,
                 DefaultRootWindow(dpy),
                 0, 0,
                 DisplayWidth(dpy, DefaultScreen(dpy)),
                 DisplayHeight(dpy, DefaultScreen(dpy)),
                 AllPlanes,
                 ZPixmap,
                 xim, 0, 0);

    ++frame_counter;

    INT64 timestamp = get_time_micro();
    INT64 time = timestamp - last_fps_timestamp;
    if (time > 500000)
    {
      UINT32 frames = frame_counter - last_frame_counter;
      last_fps_timestamp = timestamp;
      last_frame_counter = frame_counter;
      printf("FPS: %f\n", (double)frames / ((double)time * 1.0e-6));
    }
  }

  XDestroyImage(xim);
  XCloseDisplay(dpy);
  return 0;
}