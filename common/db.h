#ifndef _DB_H_
#define _DB_H_

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <time.h>
#include <stdio.h>

#include <openssl/sha.h>
#include <sqlite3.h>

#define STRINGIFY_HELPER(X) #X
#define STR(X) STRINGIFY_HELPER(X)

#define SQL_OK 0
#define SQL_ERROR -1
#define SQL_NO_ROWS -2

#define MAX_SQL_QUERY_LENGTH 4096

#define DB_NAME "main.db"
#define TABLE_NAME "REGISTRY"

#define MAX_DATETIME_LEN 100
#define MAX_ADDRESS_LEN 100
#define MAX_TOKEN_LEN 40

#define SESSID_LEN 8
#define PASSWD_LEN 6
#define SECRET_HASHED_LEN 64

#define CHECK_SESSID(__sessid) (strlen(__sessid) == SESSID_LEN)
#define CHECK_PASSWD(__passwd) (strlen(__passwd) == PASSWD_LEN)

#define SQL_QUERY_CREATE_TABLE                          \
  "CREATE TABLE IF NOT EXISTS " TABLE_NAME " ("         \
  "ID INTEGER PRIMARY KEY NOT NULL,"                    \
  "ADDRESS VARCHAR(" STR(MAX_ADDRESS_LEN) ") NOT NULL," \
  "SESSID CHAR(" STR(SESSID_LEN) ") NOT NULL,"          \
  "PASSWD CHAR(" STR(SECRET_HASHED_LEN) ") NOT NULL,"   \
  "TOKEN  CHAR(" STR(SECRET_HASHED_LEN) ") NOT NULL,"   \
  "LAST_CONNECT INTEGER);"

#define SQL_QUERY_INSERT_NEW_CLIENT                   \
  "INSERT INTO " TABLE_NAME                           \
  " (ADDRESS, SESSID, PASSWD, TOKEN, LAST_CONNECT) "  \
  " VALUES(?, ?, ?, ?, datetime('now', 'localtime'));"

#define SQL_QUERY_UPDATE_LAST_CONNECT                 \
  "UPDATE " TABLE_NAME                                \
  " SET LAST_CONNECT = datetime('now', 'localtime') " \
  " WHERE ADDRESS = ?;" // AND ID = %d;"

#define SQL_QUERY_DELETE    \
  "DELETE FROM " TABLE_NAME \
  " WHERE ID = ?;"

#define SQL_QUERY_SELECT_ALL \
  "SELECT * FROM " TABLE_NAME ";"

#define SQL_QUERY_SELECT_BY_ADDRESS \
  "SELECT ID, ADDRESS, SESSID, LAST_CONNECT FROM " TABLE_NAME " WHERE ADDRESS = ? ;"

#define SQL_QUERY_SELECT_BY_SESSID \
  "SELECT ID, ADDRESS, SESSID, LAST_CONNECT FROM " TABLE_NAME " WHERE SESSID = ? ;"

#define SQL_QUERY_SELECT_BY_TOKEN \
  "SELECT ID, ADDRESS, SESSID, LAST_CONNECT FROM " TABLE_NAME " WHERE TOKEN = ? ;"

#define DB_SET_ERROR(db_conn, errmsg_fmt, ...)              \
  do                                                        \
  {                                                         \
    if (db_conn)                                            \
    {                                                       \
      char error_msg[1024];                                 \
      snprintf(error_msg, 1024, errmsg_fmt, ##__VA_ARGS__); \
      db_conn->errmsg = strdup(error_msg);                  \
    }                                                       \
  } while (0)

typedef struct _db_connection
{
  sqlite3 *db;
  char *errmsg;
} db_connection;

typedef struct _client
{
  int c_id;
  unsigned char c_ip[MAX_ADDRESS_LEN];
  unsigned char c_sessid[SESSID_LEN + 1];
  unsigned char c_passwd[PASSWD_LEN + 1];
  unsigned char c_last_connect[MAX_DATETIME_LEN];
} client;

int db_open_connection(db_connection **db_conn);
void db_destroy(db_connection *db_conn);

int db_create_table(db_connection *db_conn);

int db_delete_client(db_connection *db_conn, int id);
int db_select_all(db_connection *db_conn);

int db_insert_new_client(db_connection *db_conn,
                         const char *address, size_t address_len,
                         const char *sessid, size_t sessid_len,
                         const char *passwd, size_t passwd_len,
                         const char *token, size_t token_len);

int db_update_client_last_connect(db_connection *db_conn, client *cl);
int db_select_client_by_address(db_connection *db_conn, client *cl,
                                const char *address, size_t address_len);
int db_select_client_by_sessid(db_connection *db_conn, client *cl,
                               const char *sessid, size_t sessid_len);
int db_select_client_by_token(db_connection *db_conn, client *cl,
                              const char *token, size_t token_len);

#endif