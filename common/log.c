#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <time.h>
#include <stdarg.h>
#include "log.h"

int log_level = LOG_LEVEL_ALL;

static struct __log_config_t
{
  FILE *fp;
  char *filepath;
  enum log_add_time timestamp;
} log_config;

BOOL log_init(int level, const char *file_path, enum log_add_time time)
{
  FILE *log_file;

  if (level > LOG_LEVEL_DEBUG || level < LOG_LEVEL_FATAL)
  {
    fprintf(stderr, "Unknown log level %d\n", level);
    return FALSE;
  }

  if (file_path != NULL)
  {

    log_file = fopen(file_path, "a");

    if (log_file == NULL)
    {
      fprintf(stderr, "Unable to open log file: %s\n", file_path);
      return FALSE;
    }

    if (log_config.fp != NULL || log_config.filepath != NULL)
      log_deinit();

    log_config.fp = log_file;

    log_config.filepath = strdup(file_path);
    if (log_config.filepath == NULL)
    {
      fprintf(stderr, "log_init strdup error\n");
      return FALSE;
    }
  }
  else
  {

    if (log_config.fp != NULL || log_config.filepath != NULL)
      log_deinit();

    log_config.fp = stderr; //fileno(stderr);
    log_config.filepath = 0;
  }

  log_config.timestamp = time;
  log_level = level;
  return TRUE;
}

void wlog(int level, const char *fmt, ...)
{
  char buf[LOG_LINE_LIMIT];
  va_list ap;
  size_t written = 0;

  assert(fmt);

  if (log_config.fp == NULL)
  {
    return;
  }

  if (log_config.timestamp == LOG_TIME_ADD)
  {
    // add time to log
    time_t now;
    /* Get time stamp. */
    time(&now);
    written += snprintf(buf, LOG_LINE_LIMIT, "%llu ", (unsigned long long)now);
  }

  va_start(ap, fmt);
  written += vsnprintf(buf + written, LOG_LINE_LIMIT - written, fmt, ap);
  va_end(ap);

  // write log
  fwrite(buf, written, 1, log_config.fp);
}

//ATTR_HIDDEN
//__attribute__((visibility("hidden")))
void log_deinit(void)
{
  free(log_config.filepath);
  if (log_config.fp != NULL)
  {
    fclose(log_config.fp);
  }
}