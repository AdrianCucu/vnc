//
// types.h
//
#ifndef __TYPES_H__
#define __TYPES_H__

typedef unsigned char BYTE;

typedef char CHAR;

typedef unsigned char BOOL;
#ifndef TRUE
# define TRUE  1
#endif
#ifndef FALSE
# define FALSE 0
#endif

typedef unsigned char UINT8;
typedef unsigned short UINT16;
typedef unsigned int UINT32;
typedef unsigned long long UINT64;

typedef char INT8;
typedef short INT16;
typedef int INT32;
typedef long long INT64;

// must be big enough to hold any pixel value
typedef unsigned long PIXEL;

inline BOOL big_endian_test()
{
  BYTE swaptest[2] = {1, 0};

  if (*(UINT16 *)swaptest == 1)
    return FALSE;
  return TRUE;
}

#endif // __TYPES_H__