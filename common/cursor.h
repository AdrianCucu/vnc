//
// X Cursor Pseudo-encoding
// cursor.h
//
#ifndef __CURSOR_H__
#define __CURSOR_H__

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "types.h"

typedef struct
{
  char *name;
  INT16 x, y;
  UINT16 width, height;
  UINT16 xhot, yhot;
  UINT32 *pixels; // RGBA cusor image data buffer
  /*
  The bitmap and bitmask both consist of left-to-right, 
  top-to-bottom scanlines, where each scanline is padded 
  to a whole number of bytes floor((width + 7) / 8). 
  Within each byte the most significant bit represents the leftmost pixel, 
  with a 1-bit meaning the corresponding pixel should use the primary colour, 
  or that the pixel is valid.
  */
  // UINT8 *bmap;
  // UINT8 *bmsk;
} xcursor_t;


BOOL set_cursor_buf(xcursor_t *cursor, INT32 width, INT32 height, UINT8 *argb);

// BOOL set_cursor(xcursor_t *cursor, UINT8 *argb_buf);

// UINT8 *get_cursor_bitmap(xcursor_t *cursor);
// UINT8 *get_cursor_bitmask(xcursor_t *cursor);

#endif //__CURSOR_H__