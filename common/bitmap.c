#include "bitmap.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

BOOL write_pixel_array_bmp(const char *filename, INT32 width, INT32 height, const Pixel_t *pixel_array)
{
  int fd;
  BYTE zeros[3] = {0, 0, 0};
  INT32 padding = 0;
  UINT32 raw_img_size;
  INT32 bytes_per_line;
  BYTE *out;
  INT32 y, x;
  BitmapHeader_t bmp_hdr = {0};
  BitmapInfoHeader_t bmp_info_hdr = {0};

  if ((fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0644)) < 0)
  {
    perror("write_bmp: open ");
    return FALSE;
  }

  if ((width * sizeof(Pixel_t)) % 4)
    padding = 4 - ((width * sizeof(Pixel_t)) % 4);

  raw_img_size = height * (width * (sizeof(Pixel_t) + padding));
  bytes_per_line = width * sizeof(Pixel_t);

  bmp_hdr.type[0] = 'B';
  bmp_hdr.type[1] = 'M';
  bmp_hdr.size = raw_img_size + sizeof(BitmapHeader_t) + sizeof(BitmapInfoHeader_t);
  bmp_hdr.offset = sizeof(BitmapHeader_t) + sizeof(BitmapInfoHeader_t);

  bmp_info_hdr.size = sizeof(BitmapInfoHeader_t);
  bmp_info_hdr.width = width;
  bmp_info_hdr.height = height;
  bmp_info_hdr.color_planes = 1;
  bmp_info_hdr.bits_per_pixel = 24;
  bmp_info_hdr.compress_type = 0; // 0 = none
  bmp_info_hdr.raw_img_size = raw_img_size;
  bmp_info_hdr.w_resolution = 0;
  bmp_info_hdr.h_resolution = 0;
  bmp_info_hdr.num_col_pallete = 0;
  bmp_info_hdr.num_col_used = 0;

  write(fd, &bmp_hdr, sizeof(bmp_hdr));
  write(fd, &bmp_info_hdr, sizeof(bmp_info_hdr));

  out = (BYTE *)pixel_array + (height - 1) * bytes_per_line;
  for (y = height; y > 0; y--)
  {
    write(fd, out, bytes_per_line);

    if (padding > 0)
      write(fd, zeros, padding);
    out -= bytes_per_line;
  }

  close(fd);
  return TRUE;
}

static BOOL _write_pixel_array_bmp(const char *filename, INT32 width, INT32 height, BYTE *data)
{
  int fd;
  BYTE zeros[3] = {0, 0, 0};
  INT32 padding = 0;
  UINT32 raw_img_size;
  INT32 bytes_per_line;
  BYTE *out;
  INT32 y, x;
  BitmapHeader_t bmp_hdr = {0};
  BitmapInfoHeader_t bmp_info_hdr = {0};

  if ((fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0644)) < 0)
  {
    perror("write_bmp: open ");
    return FALSE;
  }

  if ((width * 3) % 4)
    padding = 4 - ((width * 3) % 4);

  raw_img_size = height * (width * (3 + padding));
  bytes_per_line = width * 3;

  bmp_hdr.type[0] = 'B';
  bmp_hdr.type[1] = 'M';
  bmp_hdr.size = raw_img_size + sizeof(BitmapHeader_t) + sizeof(BitmapInfoHeader_t);
  bmp_hdr.offset = sizeof(BitmapHeader_t) + sizeof(BitmapInfoHeader_t);

  bmp_info_hdr.size = sizeof(BitmapInfoHeader_t);
  bmp_info_hdr.width = width;
  bmp_info_hdr.height = height;
  bmp_info_hdr.color_planes = 1;
  bmp_info_hdr.bits_per_pixel = 24;
  bmp_info_hdr.compress_type = 0; // 0 = none
  bmp_info_hdr.raw_img_size = raw_img_size;
  bmp_info_hdr.w_resolution = 0;
  bmp_info_hdr.h_resolution = 0;
  bmp_info_hdr.num_col_pallete = 0;
  bmp_info_hdr.num_col_used = 0;

  write(fd, &bmp_hdr, sizeof(bmp_hdr));
  write(fd, &bmp_info_hdr, sizeof(bmp_info_hdr));

  out = data + (height - 1) * bytes_per_line;
  for (y = height; y > 0; y--)
  {
    write(fd, out, bytes_per_line);

    if (padding > 0)
      write(fd, zeros, padding);
    out -= bytes_per_line;
  }

  close(fd);
  return TRUE;
}