//
// bitmap.h
//
#ifndef __BITMAP_H__
#define __BITMAP_H__

#include "types.h"

typedef struct __attribute__((packed)) _BitmapHeader_t
{
  CHAR type[2];      /* "BM" */
  UINT32 size;       /* The size of the BMP file in bytes */
  UINT32 __reserved; /* Reserved; actual value depends on the 
                            application that creates the image */
  UINT32 offset;     /* The offset, i.e. starting address, 
                            of the byte where the bitmap 
                            image data (pixel array) can be found.*/
} BitmapHeader_t;

typedef struct __attribute__((packed)) _BitmapInfoHeader_t
{
  UINT32 size;            /* size of this header (40 bytes) */
  UINT32 width;           /* bitmap width  in pixels */
  UINT32 height;          /* bitmap height in pixels */
  UINT16 color_planes;    /* must be 1 */
  UINT16 bits_per_pixel;  /* Typical values are  1, 4, 8, 16, 24 and 32 */
  UINT32 compress_type;   /* compression method being used, 0 = none */
  UINT32 raw_img_size;    /* size of image raw data */
  INT32 w_resolution;     /* horizontal resolution (pixel per metre, signed int) */
  INT32 h_resolution;     /* vertical resolution (pixel per metre, signed int) */
  UINT32 num_col_pallete; /* 0 to 2^n */
  UINT32 num_col_used;    /* numbers of important colors used, 
                                or 0 when every color is important; generally ignored */
} BitmapInfoHeader_t;

typedef struct __attribute__((packed)) _Pixel_t
{
  UINT8 b, g, r;
} Pixel_t;

BOOL write_pixel_array_bmp(const char *filename, INT32 width, INT32 height, const Pixel_t *pixel_array);

#endif //__BITMAP_H__