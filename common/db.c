#include "db.h"

void db_destroy(db_connection *db_conn)
{
  if (db_conn == NULL || db_conn->db == NULL)
    return;

  sqlite3_close(db_conn->db);
  free(db_conn);
}

int db_open_connection(db_connection **db_conn)
{
  int ret = SQLITE_OK;
  db_connection *db_c;

  if (db_conn == NULL)
    return SQL_ERROR;

  db_c = (db_connection *)calloc(1, sizeof(db_connection));

  ret = sqlite3_open(DB_NAME, &db_c->db);
  if (ret != SQLITE_OK)
  {
    free(db_c);
    return SQL_ERROR;
  }

  *db_conn = db_c;
  return SQL_OK;
}

int db_create_table(db_connection *db_conn)
{
  int ret = SQLITE_OK;

  if (db_conn == NULL || db_conn->db == NULL)
    return SQL_ERROR;

  ret = sqlite3_exec(db_conn->db, SQL_QUERY_CREATE_TABLE, NULL, 0, &db_conn->errmsg);
  if (ret != SQLITE_OK)
  {
    return SQL_ERROR;
  }
  return ret;
}

int db_delete_client(db_connection *db_conn, int id)
{
  sqlite3_stmt *stmt;
  int ret = SQLITE_OK;

  if (db_conn == NULL || db_conn->db == NULL)
    return SQL_ERROR;

  ret = sqlite3_prepare_v2(db_conn->db, SQL_QUERY_DELETE, -1, &stmt, NULL);
  if (ret != SQLITE_OK)
    goto err;

  ret = sqlite3_bind_int(stmt, 1, id);
  if (ret != SQLITE_OK)
    goto err;

  ret = sqlite3_step(stmt);
  if (ret != SQLITE_ROW && ret != SQLITE_DONE)
    goto err;

  sqlite3_finalize(stmt);
  return SQL_OK;

err:
  DB_SET_ERROR(db_conn, "%s", sqlite3_errmsg(db_conn->db));
  sqlite3_finalize(stmt);
  return SQL_ERROR;
}

int db_select_all(db_connection *db_conn)
{
  sqlite3_stmt *stmt;
  int ret = SQLITE_OK;

  if (db_conn == NULL || db_conn->db == NULL)
    return SQL_ERROR;

  ret = sqlite3_prepare_v2(db_conn->db, SQL_QUERY_SELECT_ALL, -1, &stmt, NULL);
  if (ret != SQLITE_OK)
  {
    DB_SET_ERROR(db_conn, "%s", sqlite3_errmsg(db_conn->db));
    return SQL_ERROR;
  }

  printf("----------------------------\n");
  printf("-----" STR(DB_NAME) "-------\n");

  while ((ret = sqlite3_step(stmt)) != SQLITE_DONE)
  {
    if (ret != SQLITE_ROW && ret != SQLITE_DONE)
    {
      DB_SET_ERROR(db_conn, "%s", sqlite3_errmsg(db_conn->db));
      sqlite3_finalize(stmt);
      return SQL_ERROR;
    }

    printf("#%d\nAddress: %s\n"
           "Session ID  : %s\n"
           "Password    : %s\n"
           "Token       : %s\n"
           "Last connect: %s\n\n",
           sqlite3_column_int(stmt, 0),
           sqlite3_column_text(stmt, 1),
           sqlite3_column_text(stmt, 2),
           sqlite3_column_text(stmt, 3),
           sqlite3_column_text(stmt, 4),
           sqlite3_column_text(stmt, 5));
  }
  printf("-----" STR(DB_NAME) "-------\n");
  printf("----------------------------\n");

  sqlite3_finalize(stmt);
  return SQL_OK;
}

int db_update_client_last_connect(db_connection *db_conn, client *cl)
{
  sqlite3_stmt *stmt;
  int ret = SQLITE_OK;

  if (db_conn == NULL || db_conn->db == NULL)
    return SQL_ERROR;

  ret = sqlite3_prepare_v2(db_conn->db, SQL_QUERY_UPDATE_LAST_CONNECT, -1, &stmt, NULL);
  if (ret != SQLITE_OK)
    goto err;

  ret = sqlite3_bind_text(stmt, 1, cl->c_ip, strlen(cl->c_ip), NULL);
  if (ret != SQLITE_OK)
    goto err;

  ret = sqlite3_step(stmt);
  if (ret != SQLITE_ROW && ret != SQLITE_DONE)
    goto err;

  if (ret != SQLITE_DONE)
  {
    DB_SET_ERROR(db_conn, "ip address: %s was NOT found\n", cl->c_ip);
    sqlite3_finalize(stmt);
    return SQL_NO_ROWS;
  }

  sqlite3_finalize(stmt);
  return SQL_OK;

err:
  DB_SET_ERROR(db_conn, "%s", sqlite3_errmsg(db_conn->db));
  sqlite3_finalize(stmt);
  return SQL_ERROR;
}

int db_insert_new_client(db_connection *db_conn,
                         const char *address, size_t address_len,
                         const char *sessid, size_t sessid_len,
                         const char *passwd, size_t passwd_len,
                         const char *token, size_t token_len)
{
  sqlite3_stmt *stmt;
  SHA256_CTX sha256;
  unsigned char hash[SHA256_DIGEST_LENGTH];
  unsigned char sha256_passwd_hex[65];
  unsigned char sha256_token_hex[65];
  int b, i;
  int ret = SQLITE_OK;

  if (db_conn == NULL || db_conn->db == NULL)
    return SQL_ERROR;

  if (address == NULL || address_len > MAX_ADDRESS_LEN)
  {
    DB_SET_ERROR(db_conn, "Invalid address\n");
    return SQL_ERROR;
  }

  if (sessid == NULL || sessid_len > SESSID_LEN)
  {
    DB_SET_ERROR(db_conn, "Invalid sessid\n");
    return SQL_ERROR;
  }

  if (passwd == NULL)
  {
    DB_SET_ERROR(db_conn, "Invalid password\n");
    return SQL_ERROR;
  }

  if (token == NULL || token_len > MAX_TOKEN_LEN)
  {
    DB_SET_ERROR(db_conn, "Invalid token\n");
    return SQL_ERROR;
  }

  ret = sqlite3_prepare_v2(db_conn->db, SQL_QUERY_INSERT_NEW_CLIENT, -1, &stmt, NULL);

  if (ret != SQLITE_OK)
    goto err;

  ret = sqlite3_bind_text(stmt, 1, address, address_len, NULL);
  if (ret != SQLITE_OK)
    goto err;

  ret = sqlite3_bind_text(stmt, 2, sessid, sessid_len, NULL);
  if (ret != SQLITE_OK)
    goto err;

  // hash the password
  SHA256_Init(&sha256);
  SHA256_Update(&sha256, passwd, passwd_len);
  SHA256_Final(hash, &sha256);

  for (b = 0, i = 0; i < SHA256_DIGEST_LENGTH; ++i, b += 2)
  {
    sprintf(&sha256_passwd_hex[b], "%02x", hash[i]);
  }
  sha256_passwd_hex[64] = '\0';

  ret = sqlite3_bind_text(stmt, 3, sha256_passwd_hex, 64 /* sha-256 digest size(hex characters)*/, NULL);
  if (ret != SQLITE_OK)
    goto err;

  // hash the token
  SHA256_Init(&sha256);
  SHA256_Update(&sha256, token, token_len);
  SHA256_Final(hash, &sha256);

  for (b = 0, i = 0; i < SHA256_DIGEST_LENGTH; ++i, b += 2)
  {
    sprintf(&sha256_token_hex[b], "%02x", hash[i]);
  }
  sha256_token_hex[64] = '\0';

  ret = sqlite3_bind_text(stmt, 4, sha256_token_hex, 64 /* sha-256 digest size(hex characters)*/, NULL);
  if (ret != SQLITE_OK)
    goto err;

  ret = sqlite3_step(stmt);
  if (ret != SQLITE_ROW && ret != SQLITE_DONE)
    goto err;

  if (ret == SQLITE_DONE)
    return SQL_OK;

  return SQL_OK;
err:
  DB_SET_ERROR(db_conn, "%s", sqlite3_errmsg(db_conn->db));
  sqlite3_finalize(stmt);
  return SQL_ERROR;
}

int db_select_client_by_address(db_connection *db_conn, client *cl,
                                const char *address, size_t address_len)
{
  sqlite3_stmt *stmt;
  int ret = SQLITE_OK;

  if (db_conn == NULL || db_conn->db == NULL)
    return SQL_ERROR;

  if (address == NULL || address_len > MAX_ADDRESS_LEN)
  {
    DB_SET_ERROR(db_conn, "Invalid address\n");
    return SQL_ERROR;
  }

  ret = sqlite3_prepare_v2(db_conn->db, SQL_QUERY_SELECT_BY_ADDRESS, -1, &stmt, NULL);
  if (ret != SQLITE_OK)
    goto err;

  ret = sqlite3_bind_text(stmt, 1, address, address_len, NULL);
  if (ret != SQLITE_OK)
    goto err;

  ret = sqlite3_step(stmt);
  if (ret != SQLITE_ROW && ret != SQLITE_DONE)
    goto err;

  if (ret == SQLITE_DONE)
  {
    DB_SET_ERROR(db_conn, "ip address: %s was not found\n", address);
    sqlite3_finalize(stmt);
    return SQL_NO_ROWS;
  }

  printf("------\n#%d\nAddress: %s\nsession id: %s\nlast connect: %s\n------\n",
         sqlite3_column_int(stmt, 0),
         sqlite3_column_text(stmt, 1),
         sqlite3_column_text(stmt, 2),
         sqlite3_column_text(stmt, 3));

  cl->c_id = sqlite3_column_int(stmt, 0);
  strcpy(cl->c_ip, sqlite3_column_text(stmt, 1));
  strcpy(cl->c_sessid, sqlite3_column_text(stmt, 2));
  strcpy(cl->c_last_connect, sqlite3_column_text(stmt, 3));
  sqlite3_finalize(stmt);
  return SQL_OK;

err:
  DB_SET_ERROR(db_conn, "%s", sqlite3_errmsg(db_conn->db));
  sqlite3_finalize(stmt);
  return SQL_ERROR;
}

int db_select_client_by_sessid(db_connection *db_conn, client *cl,
                               const char *sessid, size_t sessid_len)
{
  sqlite3_stmt *stmt;
  int ret = SQLITE_OK;

  if (db_conn == NULL || db_conn->db == NULL)
    return SQL_ERROR;

  if (sessid == NULL || sessid_len > SESSID_LEN)
  {
    DB_SET_ERROR(db_conn, "Invalid sessid\n");
    return SQL_ERROR;
  }

  ret = sqlite3_prepare_v2(db_conn->db, SQL_QUERY_SELECT_BY_SESSID, -1, &stmt, NULL);
  if (ret != SQLITE_OK)
    goto err;

  ret = sqlite3_bind_text(stmt, 1, sessid, sessid_len, NULL);
  if (ret != SQLITE_OK)
    goto err;

  ret = sqlite3_step(stmt);
  if (ret != SQLITE_ROW && ret != SQLITE_DONE)
    goto err;

  if (ret == SQLITE_DONE)
  {
    DB_SET_ERROR(db_conn, "sessid id: %s was not found\n", sessid);
    sqlite3_finalize(stmt);
    return SQL_NO_ROWS;
  }

  printf("------\n#%d\nAddress: %s\nsession id: %s\nlast connect: %s\n------\n",
         sqlite3_column_int(stmt, 0),
         sqlite3_column_text(stmt, 1),
         sqlite3_column_text(stmt, 2),
         sqlite3_column_text(stmt, 3));

  cl->c_id = sqlite3_column_int(stmt, 0);
  strcpy(cl->c_ip, sqlite3_column_text(stmt, 1));
  strcpy(cl->c_sessid, sqlite3_column_text(stmt, 2));
  strcpy(cl->c_last_connect, sqlite3_column_text(stmt, 3));
  sqlite3_finalize(stmt);
  return SQL_OK;

err:
  DB_SET_ERROR(db_conn, "%s", sqlite3_errmsg(db_conn->db));
  sqlite3_finalize(stmt);
  return SQL_ERROR;
}

int db_select_client_by_token(db_connection *db_conn, client *cl,
                              const char *token, size_t token_len)
{
  sqlite3_stmt *stmt;
  int ret = SQLITE_OK;

  if (db_conn == NULL || db_conn->db == NULL)
    return SQL_ERROR;

  if (token == NULL || token_len > MAX_TOKEN_LEN)
  {
    DB_SET_ERROR(db_conn, "Invalid token\n");
    return SQL_ERROR;
  }

  ret = sqlite3_prepare_v2(db_conn->db, SQL_QUERY_SELECT_BY_TOKEN, -1, &stmt, NULL);
  if (ret != SQLITE_OK)
    goto err;

  ret = sqlite3_bind_text(stmt, 1, token, token_len, NULL);
  if (ret != SQLITE_OK)
    goto err;

  ret = sqlite3_step(stmt);
  if (ret != SQLITE_ROW && ret != SQLITE_DONE)
    goto err;

  if (ret == SQLITE_DONE)
  {
    DB_SET_ERROR(db_conn, "token: %s was not found\n", token);
    sqlite3_finalize(stmt);
    return SQL_NO_ROWS;
  }

  printf("------\n#%d\nAddress: %s\nsession id: %s\nlast connect: %s\n------\n",
         sqlite3_column_int(stmt, 0),
         sqlite3_column_text(stmt, 1),
         sqlite3_column_text(stmt, 2),
         sqlite3_column_text(stmt, 3));

  cl->c_id = sqlite3_column_int(stmt, 0);
  strcpy(cl->c_ip, sqlite3_column_text(stmt, 1));
  strcpy(cl->c_sessid, sqlite3_column_text(stmt, 2));
  strcpy(cl->c_last_connect, sqlite3_column_text(stmt, 3));
  sqlite3_finalize(stmt);
  return SQL_OK;

err:
  DB_SET_ERROR(db_conn, "%s", sqlite3_errmsg(db_conn->db));
  sqlite3_finalize(stmt);
  return SQL_ERROR;
}