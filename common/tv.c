#include "tv.h"

void tvsub(struct timeval *out, const struct timeval *in)
{
  if ((out->tv_usec -= in->tv_usec) < 0)
  {
    out->tv_sec--;
    out->tv_usec += 1000000;
  }
  out->tv_sec -= in->tv_sec;
}

void tvadd(struct timeval *out, const struct timeval *in)
{
  if ((out->tv_usec += in->tv_usec) >= 1000000)
  {
    out->tv_sec++;
    out->tv_usec %= 1000000;
  }
  out->tv_sec += in->tv_sec;
}

void add_to_avg(struct tv_avg *avg, const struct timeval *tv)
{
  tvsub(&avg->sum, &avg->samples[avg->curr_sample_idx]);
  tvadd(&avg->sum, tv);

  avg->samples[avg->curr_sample_idx].tv_sec = tv->tv_sec;
  avg->samples[avg->curr_sample_idx].tv_usec = tv->tv_usec;

  if (++avg->curr_sample_idx == MAXSAMPLES)
    avg->curr_sample_idx = 0;
}

void get_avg(struct tv_avg *avg, struct timeval *avg_out)
{
  avg_out->tv_sec = avg->sum.tv_sec / MAXSAMPLES;
  avg_out->tv_usec =
      ((avg->sum.tv_sec % MAXSAMPLES) * 1000000 + avg->sum.tv_usec) / MAXSAMPLES;
}