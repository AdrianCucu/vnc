#ifndef _PROXY_MESSAGES_H_
#define _PROXY_MESSAGES_H_

#include <stdint.h>
#include "db.h"

enum ProxyMessageType {
  INIT_MESSAGE = 0,
  INIT_CONNECTION,
  REQUEST_CONNECTION,
  REQUEST_CONNECTION_FAIL,
  ASK_FOR_CONNECT,
  DECLINE_FOR_CONNECT,
  APPROVE_FOR_CONNECT
};

typedef struct _proxyInitMsg
{
  uint8_t type; /* always ProxyMessageType.INIT_MESSAGE */
  char sessid[SESSID_LEN + 1];
  char passwd[PASSWD_LEN + 1];
} ProxyInitMsg;

#define sz_ProxyInitMsg (sizeof(struct _proxyInitMsg))

typedef struct _proxyReqConnMsg
{
  uint8_t type; /* always ProxyMessageType.REQUEST_CONNECTION */
  char sessid[SESSID_LEN + 1];
  char passwd[PASSWD_LEN + 1];
} ProxyReqConnMsg;

#define sz_ProxyReqConnMsg (sizeof(struct _proxyReqConnMsg))

typedef struct _proxyAskForConnectMsg
{
  uint8_t type; /* always ProxyMessageType.ASK_FOR_CONNECT */
} ProxyAskForConnectMsg;

#define sz_ProxyAskForConnectMsg (sizeof(struct _proxyAskForConnectMsg))

typedef struct _proxyInitConnectionMsg
{
  uint8_t type; /* always ProxyMessageType.INIT_CONNECTION */
  uint8_t __pad;
  uint16_t port;
} ProxyInitConnectionMsg;

#define sz_ProxyInitConnectionMsg 4

typedef struct _proxyDeclineForConnectMsg
{
  uint8_t type; /* always ProxyMessageType.DECLINE_FOR_CONNECT */
} ProxyDeclineForConnectMsg;

#define sz_ProxyDeclineForConnectMs 1

typedef struct _proxyApproveForConnectMsg
{
  uint8_t type; /* always ProxyMessageType.APPROVE_FOR_CONNECT */
} ProxyApproveForConnectMsg;

#define sz_ProxyApproveForConnectMs 1

typedef union {
  uint8_t type;
  ProxyInitMsg pim;
  ProxyInitConnectionMsg pinifcm;
  ProxyReqConnMsg prcm;
  ProxyAskForConnectMsg pafcm;
  ProxyDeclineForConnectMsg pdecfcm;
  ProxyApproveForConnectMsg pappfcm;
 } ProxyMsg;

 #endif