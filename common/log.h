#ifndef __LOG_H__
#define __LOG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "types.h"

#define XSTR(__x) STRINGIFY(__x)
#define STRINGIFY(__s) #__s

#define LOG_LINE_LIMIT 4096

  // DEBUG < INFO < WARN < ERROR < FATAL < OFF.

#define LOG_LEVEL_FATAL 0x01
#define LOG_LEVEL_ERROR 0x02
#define LOG_LEVEL_WARN 0x03
#define LOG_LEVEL_INFO 0x04
#define LOG_LEVEL_DEBUG 0x05
#define LOG_LEVEL_ALL 0xff

  enum log_add_time
  {
    LOG_TIME_NONE = 0,
    LOG_TIME_ADD
  };

  extern int log_level;

  extern BOOL log_init(int,          /* log level */
                       const char *, /* log file path*/
                       enum log_add_time /* add or not timestamp */);

  extern void wlog(int,          /* log_level */
                  const char *, /* format */
                  ... /* args */);

  extern void log_deinit(void);

#define __log(tag, level, fmt, args...)                                     \
  do                                                                        \
  {                                                                         \
    if (level <= log_level)                                                 \
    {                                                                       \
      if (LOG_LEVEL_WARN < level)                                           \
        wlog(level, " %s: " fmt "\n", tag, ##args);                         \
      else                                                                  \
        wlog(level,                                                         \
             " %s: " fmt " (in %s() at " __FILE__ ":" XSTR(__LINE__) ")\n", \
             tag, ##args, __func__);                                        \
    }                                                                       \
  } while (0)

#define LOG(tag, level, fmt, args...) __log(tag, level, fmt, ##args)

#define DEBUG(fmt, args...) __log("DEBUG", LOG_LEVEL_DEBUG, fmt, ##args)
#define INFO(fmt, args...) __log("INFO", LOG_LEVEL_INFO, fmt, ##args)
#define WARN(fmt, args...) __log("WARN", LOG_LEVEL_WARN, fmt, ##args)
#define ERROR(fmt, args...) __log("ERROR", LOG_LEVEL_ERROR, fmt, ##args)
#define FATAL(fmt, args...) __log("FATAL", LOG_LEVEL_FATAL, fmt, ##args)

#ifdef __cplusplus
}
#endif

#endif // __LOG_H__
