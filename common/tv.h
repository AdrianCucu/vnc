#ifndef _TV_H_
#define _TV_H_

#include <sys/time.h>
#include <time.h>

#include "types.h"

#define MAXSAMPLES 10

struct tv_avg
{
  struct timeval sum;
  struct timeval samples[MAXSAMPLES];
  int curr_sample_idx;
};

#define INIT_TV_AVG {{0, 0}, {{0}}, 0}

void tvadd(struct timeval *out, const struct timeval *in);
void tvsub(struct timeval *out, const struct timeval *in);

void add_to_avg(struct tv_avg *avg, const struct timeval *tv);
void get_avg(struct tv_avg *avg, struct timeval *avg_out);

static inline INT64 get_time_micro()
{
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);

  return (INT64)ts.tv_sec * (INT64)1000000ULL + // seconds to microseconds
         (INT64)(ts.tv_nsec / 1000);            // nanoseconds to microseconds
}

#endif