//
// PixelFormat - structure to represent a pixel format
//
#ifndef __PIXELFORMAT_H__
#define __PIXELFORMAT_H__

#include "types.h"

typedef struct
{
	UINT8 bits_pp;		/* 8,16,32 only */
	UINT8 depth;			/* 8 to 32 */
	BOOL big_endian;	/* True if multi-byte pixels are interpreted
				            as big endian, or if single-bit-per-pixel
				            has most significant bit of the byte
				            corresponding to first (leftmost) pixel. Of
				            course this is meaningless for 8 bits/pix */
	BOOL true_colour; /* If false then we need a "colour map" to
				            convert pixels to RGB.  If true, xxxMax and
				            xxxShift specify bits used for red, green
				            and blue */

	/* the following fields are only meaningful if true_colour is TRUE */
	/* maximum red value (= 2^n - 1 where n is the
				   number of bits used for red). Note this
				   value is always in big endian order. */
	UINT16 red_max;
	UINT16 green_max;
	UINT16 blue_max;

	/* number of shifts needed to get the red
				        value in a pixel to the least significant
				        bit. To find the red value from a given
				        pixel, do the following:
				        1) Swap pixel value according to big_endian
				            (e.g. if big_endian is false and host byte
				            order is big endian, then swap).
				        2) Shift right by red_shift.
				        3) AND with red_max (in host byte order).
				        4) You now have the red value between 0 and
				        red_max. */
	UINT8 red_shift;
	UINT8 green_shift;
	UINT8 blue_shift;

	UINT8 pad1;
	UINT16 pad2;

} PixelFormat;

BOOL pixel_format_eql(PixelFormat *pf1, PixelFormat *pf2);
BOOL pixel_format_888(PixelFormat *pf);

BOOL pixel_format_big_endian(PixelFormat *pf);
BOOL pixel_format_little_endian(PixelFormat *pf);

#endif //__PIXELFORMAT_H__