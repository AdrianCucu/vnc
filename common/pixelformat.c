#include "pixelformat.h"

BOOL pixel_format_eql(PixelFormat *pf1, PixelFormat *pf2)
{
  if (pf1->bits_pp != pf2->bits_pp || pf1->depth != pf2->depth)
    return FALSE;

  if (pf1->red_max != pf2->red_max ||
      pf1->blue_max != pf2->blue_max ||
      pf1->green_max != pf2->green_max)
    return FALSE;

  // If pixel formats have same byte order or pixel format consisnt only from one byte
  if (pf1->big_endian == pf2->big_endian || pf1->bits_pp == 8)
  {
    if (pf1->red_shift != pf2->red_shift)
      return FALSE;
    if (pf1->blue_shift != pf2->blue_shift)
      return FALSE;
    if (pf1->green_shift != pf2->green_shift)
      return FALSE;
  }
  else
  {
    if (pf1->red_shift != 24 - pf2->red_shift)
      return FALSE;
    if (pf1->green_shift != 24 - pf2->green_shift)
      return FALSE;
    if (pf1->blue_shift != 24 - pf2->blue_shift)
      return FALSE;
  }
  return TRUE;
}

BOOL pixel_format_888(PixelFormat *pf)
{
  if (pf->true_colour == FALSE)
    return FALSE;
  if (pf->bits_pp != 32)
    return FALSE;
  if (pf->depth != 24)
    return FALSE;
  if (pf->red_max != 255)
    return FALSE;
  if (pf->green_max != 255)
    return FALSE;
  if (pf->blue_max != 255)
    return FALSE;
  return TRUE;
}

BOOL pixel_format_big_endian(PixelFormat *pf)
{
  return pf->big_endian == TRUE;
}

BOOL pixel_format_little_endian(PixelFormat *pf)
{
  return pf->big_endian == FALSE;
}