#include <assert.h>
#include "cursor.h"

static UINT16 pow223[] = {0, 30, 143, 355, 676, 1113, 1673,
                            2361, 3181, 4139, 5237, 6479, 7869,
                            9409, 11103, 12952, 14961, 17130,
                            19462, 21960, 24626, 27461, 30467,
                            33647, 37003, 40535, 44245, 48136,
                            52209, 56466, 60907, 65535};

// Floyd-Steinberg dithering
static void dither(int width, int height, INT32 *data)
{
  INT32 error;
  INT32 i, x, y;

  for (y = 0; y < height; ++y)
  {
    for (i = 0; i < width; ++i)
    {
      x = (y & 1) ? (width - i - 1) : i;

      if (data[x] > 32767)
      {
        error = data[x] - 65535;
        data[x] = 65535;
      }
      else
      {
        error = data[x] - 0;
        data[x] = 0;
      }

      if (y & 1)
      {
        if (x > 0)
        {
          data[x - 1] += error * 7 / 16;
        }
        if ((y + 1) < height)
        {
          if (x > 0)
            data[x - 1 + width] += error * 3 / 16;
          data[x + width] += error * 5 / 16;
          if ((x + 1) < width)
            data[x + 1] += error * 1 / 16;
        }
      }
      else
      {
        if ((x + 1) < width)
        {
          data[x + 1] += error * 7 / 16;
        }
        if ((y + 1) < height)
        {
          if ((x + 1) < width)
            data[x + 1 + width] += error * 3 / 16;
          data[x + width] += error * 5 / 16;
          if (x > 0)
            data[x - 1] += error * 1 / 16;
        }
      }
    }
    data += width;
  }
}

static UINT16 ipow(UINT16 val, UINT16 lut[])
{
  INT32 idx = val >> (16 - 5);
  INT32 a, b;

  if (val < 0x8000)
  {
    a = lut[idx];
    b = lut[idx + 1];
  }
  else
  {
    a = lut[idx - 1];
    b = lut[idx];
  }

  return (val & 0x7ff) * (b - a) / 0x7ff + a;
}

static UINT16 srgb_to_lin(BYTE srgb)
{
  return ipow((UINT16)srgb * 65535 / 255, pow223);
}

UINT8 *xcursor_pseudo_encode(INT32 width, INT32 height, UINT8 *buf)
{
  UINT8 *array = NULL;
  UINT32 *buf_data = NULL;
  INT32 mask_bytes_per_row;
  INT32 x, y;

  array = (UINT8 *)malloc((width + 7) / 8 * height);
  if (array == NULL)
    return NULL;

  memset(array, 0, (width + 7) / 8 * height);

  mask_bytes_per_row = (width + 7) / 8;
  
  buf_data = (UINT32 *)buf;
  for (y = 0; y < height; ++y)
  {
    for (x = 0; x < width; ++x)
    {
      int byte = y * mask_bytes_per_row + x / 8;
      int bit = 7 - x % 8;

      if (*buf_data > 32767)
        array[byte] |= (1 << bit);

      buf_data++;
    }
  }
  return array;
}

// BOOL set_cursor(xcursor_t *cursor, UINT8 *argb_buf)
// {
//   INT32 x, y;
//   UINT8 rgba[4];
//   INT32 a;
//   PIXEL pixel;

//   INT32 *alpha, *alpha_ptr;
//   INT32 *luminance, *lum_ptr;
//   UINT8 *source;
//   INT32 lum;
//   const UINT8 *data_ptr;
//   UINT8 *bitmap, *bitmask;

//   assert(cursor);

//   lum_ptr = luminance = (INT32 *)malloc(cursor->width * cursor->height * 4);
//   alpha_ptr = alpha = (INT32 *)malloc(cursor->width * cursor->height * 4);
//   BOOL once = TRUE;
//   data_ptr = (const UINT8 *)argb_buf;
//   for (y = 0; y < cursor->height; ++y)
//   {
//     for (x = 0; x < cursor->width; ++x)
//     {
//       // Un-premultiply alpha
//       pixel = *((PIXEL *)data_ptr);

//       a = (pixel >> 24) & 0xff;
//       if (a == 0)
//         a = 1; // Avoid division by zero
//       rgba[0] = ((pixel >> 16) & 0xff) * 255 / a; // R
//       rgba[1] = ((pixel >>  8) & 0xff) * 255 / a; // G
//       rgba[2] = ((pixel >>  0) & 0xff) * 255 / a; // B
//       rgba[3] = ((pixel >> 24) & 0xff);           // A

//       if (once) {
//         printf("==> %.08lx %08lx\n", *((PIXEL *)rgba), a);
//         once = FALSE;
//       }

//       // Use BT.709 coefficients for grayscale
//       lum = 0;
//       lum += (UINT32)srgb_to_lin(rgba[0]) * 6947;  // 0.2126
//       lum += (UINT32)srgb_to_lin(rgba[1]) * 23436; // 0.7152
//       lum += (UINT32)srgb_to_lin(rgba[2]) * 2366;  // 0.0722
//       lum /= 32768;

//       *lum_ptr++ = lum;

//       *alpha_ptr++ = (UINT32)rgba[3] * 65535 / 255;
      
//       data_ptr += 4;
//     }
//   }

//   // Then conversion to a bit mask
//   bitmap = XCursor_pseudo_encode(cursor->width, cursor->height, luminance);
//   bitmask = XCursor_pseudo_encode(cursor->width, cursor->height, alpha);

//   if (bitmap && bitmask)
//   {
//     if (cursor->bmap)
//       free(cursor->bmap);
//     if (cursor->bmsk)
//       free(cursor->bmsk);
//     cursor->bmap = bitmap;
//     cursor->bmsk = bitmask;
//   }
  
// good:
//   free(luminance);
//   free(alpha);
//   return TRUE;
// error:
//   free(luminance);
//   free(alpha);
//   return FALSE;
// }

BYTE *get_premultiplied_alpha(INT32 width, INT32 height, UINT8 *argb)
{
  // ARGB cusor image data buffer 
  BYTE *cursor_data = malloc(width * height * 4);
  BYTE *out;
  const PIXEL *pixels;
  PIXEL pixel;
  INT32 x, y;
  UINT8 alpha;

  // Un-premultiply alpha
  pixels = (const PIXEL *)argb;
  out = cursor_data;
  for (y = 0; y < height; y++) {
    for (x = 0; x < width; x++) {
      pixel = *pixels++;

      alpha = (pixel >> 24) & 0xff;
      if (alpha == 0)
        alpha = 1; // Avoid division by zero

      *out++ = ((pixel >> 16) & 0xff) * 255/alpha;
      *out++ = ((pixel >>  8) & 0xff) * 255/alpha;
      *out++ = ((pixel >>  0) & 0xff) * 255/alpha;
      *out++ = ((pixel >> 24) & 0xff);
    }
  }
  return cursor_data;
}


BOOL set_cursor_buf(xcursor_t *cursor, INT32 width, INT32 height, UINT8 *argb)
{
  if (!cursor)
    return FALSE;

  cursor->width = width;
  cursor->height = height;

  cursor->pixels = (UINT32 *)get_premultiplied_alpha(cursor->width, cursor->height, argb);
  if (cursor->pixels)
    return TRUE;
  return FALSE;
}


UINT8 *get_cursor_bitmap(xcursor_t *cursor)
{
  INT32 x, y;
  // First step is converting to luminance
  INT32 *luminance;
  INT32 *lum_ptr;
  UINT8 *source;
  INT32 lum;
  const BYTE *data_ptr;

  assert(cursor);

  lum_ptr = luminance = (INT32 *)malloc(cursor->width * cursor->height * 4);
  data_ptr = (const BYTE *)cursor->pixels;

  for (y = 0; y < cursor->height; ++y)
  {
    for (x = 0; x < cursor->width; ++x)
    {
      // Use BT.709 coefficients for grayscale
      lum = 0;
      lum += (UINT32)srgb_to_lin(data_ptr[0]) * 6947;  // 0.2126
      lum += (UINT32)srgb_to_lin(data_ptr[1]) * 23436; // 0.7152
      lum += (UINT32)srgb_to_lin(data_ptr[2]) * 2366;  // 0.0722
      lum /= 32768;

      *lum_ptr++ = lum;
      data_ptr += 4;
    }
  }

  // Then diterhing
  // dither(cursor->width, cursor->height, luminance);

  // Then conversion to a bit mask
  UINT8 *bitmask = xcursor_pseudo_encode(cursor->width, cursor->height, (UINT8 *)luminance);
  free(luminance);
  return bitmask;
}

UINT8 *get_cursor_bitmask(xcursor_t *cursor)
{
  INT32 x, y;
  INT32 *alpha;
  INT32 *alpha_ptr;
  UINT8 *mask;
  const BYTE *data_ptr;

  assert(cursor);

  alpha_ptr = alpha = (INT32 *)malloc(cursor->width * cursor->height * 4);
  data_ptr = (const BYTE *)cursor->pixels;

  for (y = 0; y < cursor->height; ++y)
  {
    for (x = 0; x < cursor->width; ++x)
    {
      *alpha_ptr++ = (UINT32)data_ptr[3] * 65535 / 255;
      data_ptr += 4;
    }
  }

  // Then diterhing
  // dither(cursor->width, cursor->height, alpha);

  // Then conversion to a bit mask
  UINT8 *bitmask = xcursor_pseudo_encode(cursor->width, cursor->height, (UINT8 *)alpha);
  free(alpha);
  return bitmask;
}