# Default c compiler
CC := gcc

INC_DIR := ./common ./src
SRC_DIR := ./src
COMMON_SRC_DIR := ./common
OBJ_DIR := ./obj
COMMON_OBJ_DIR := ./obj/common
BIN_DIR := ./bin
OUT := ./out

HAVE_X11 := $(shell pkg-config --exists x11; echo $$?)
HAVE_XTEST := $(shell pkg-config --exists xtst; echo $$?)
HAVE_XFIXES := $(shell pkg-config --exists xfixes; echo $$?)
HAVE_XRANDR := $(shell pkg-config --exists xrandr; echo $$?)
HAVE_VNCSERVER := $(shell pkg-config --exists libvncserver; echo $$?)

CFLAGS := -Wall -lvncserver -lvncclient -lX11 -lXext -lcrypto \
					-lXtst -lXfixes -lXrandr -lpthread -ljpeg -lsqlite3 \
					`pkg-config --cflags --libs gtk+-3.0` \
					`pkg-config --cflags --libs gthread-2.0` \
					-DGDK_VERSION_MIN_REQIRED=GDK_VERSION_3_2

X11_LIBS := -lX11 -lXext -lXtst -lXfixes -lXrandr
XCB_LIBS := -lxcb -lxcb-xkb -lxcb-xtest -lxcb-keysyms \
						-lxkbcommon -lxkbcommon-x11 -lxcb-shm  \
						-lxcb-image -lxcb-xfixes -lxcb-render \
						-lxcb-shape -lxcb

COMMON_SOURCES := log.c db.c tv.c \
									bitmap.c pixelformat.c cursor.c
SOURCES := gtk_vnc_viewer.c X11_utils.c	vnc_server.c

COMMON_SRCS := $(patsubst %.c, $(COMMON_SRC_DIR)/%.c, $(COMMON_SOURCES))
SRCS := $(patsubst %.c, $(SRC_DIR)/%.c, $(SOURCES))

OBJS := $(patsubst %.c, $(OBJ_DIR)/%.o, $(SOURCES))
COMMON_OBJS := $(patsubst %.c, $(COMMON_OBJ_DIR)/%.o, $(COMMON_SOURCES))

INCLUDE_PATHS := $(patsubst %, -I%, $(INC_DIR))

# Default target
default: x11_vnc

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	@mkdir -p $(@D)
	$(CC) -c $< $(CFLAGS) $(INCLUDE_PATHS) -D USE_SHM -o $@
	#$(CC) -c $< $(CFLAGS) $(INCLUDE_PATHS) -o $@

$(COMMON_OBJ_DIR)/%.o: $(COMMON_SRC_DIR)/%.c
	@mkdir -p $(@D)
	$(CC) -c $< $(INCLUDE_PATHS) -o $@

x11_vnc: x11_vnc.c $(OBJS)
	make check_dependencies
	@mkdir -p $(OUT)
	$(CC) $^ $(CFLAGS) $(INCLUDE_PATHS) -D USE_SHM -o $(OUT)/$@
	@echo "----------------------------------------"
	@echo "Target created at: $(OUT)/$@"
	@echo "----------------------------------------"

test_%: ./tests/test_%.c $(COMMON_OBJS) $(OBJS)
	@echo "$@ <== $<"
	@mkdir -p $(OUT)
	$(CC) $^ $(CFLAGS) $(INCLUDE_PATHS) -o $(OUT)/$@
	@echo "----------------------------------------"
	@echo "Target created at: $(OUT)/$@"
	@echo "----------------------------------------"

test_gtk_vnc_viewer: ./tests/test_gtk_vnc_viewer.c $(COMMON_OBJS) $(OBJS)
	@echo "$@ <== ./tests/$@.c"
	@mkdir -p $(OUT)
	$(CC) $^ $(CFLAGS) $(INCLUDE_PATHS) -o $(OUT)/$@
	@echo "----------------------------------------"
	@echo "Target created at: $(OUT)/$@"
	@echo "----------------------------------------"

test_x11_%: ./tests/test_x11_%.c $(OBJS)
	@echo "$@ <== $<"
	@mkdir -p $(OUT)
	$(CC) $^ $(CFLAGS) $(X11_LIBS) $(INCLUDE_PATHS) -o $(OUT)/$@
	@echo "----------------------------------------"
	@echo "Target created at: $(OUT)/$@"
	@echo "----------------------------------------"

test_xcb_%: ./tests/test_xcb_%.c $(COMMON_OBJS) $(OBJS)
	@echo "$@ <== $<"
	@mkdir -p $(OUT)
	$(CC) $^ $(CFLAGS) $(XCB_LIBS) $(INCLUDE_PATHS) -D USE_SHM -o $(OUT)/$@
	@echo "----------------------------------------"
	@echo "Target created at: $(OUT)/$@"
	@echo "----------------------------------------"

.PHONY: show
show:
	@echo $(patsubst %, "\t%\n", $(COMMON_SRCS)) \
				$(patsubst %, "\t%\n", $(SRCS)) "\n---\n" \
				$(patsubst %, "\t%\n", $(COMMON_OBJS)) \
				$(patsubst %, "\t%\n", $(OBJS))

.PHONY: make_dir
make_dir:
	mkdir -p $(OUT)

.PHONY: clean
clean:
	rm -rf $(BIN_DIR)/* \
		$(OBJ_DIR)/*.o $(OBJ_DIR)/*.obj \
		$(COMMON_OBJ_DIR)/*.o $(COMMON_OBJ_DIR)/*.obj \
		$(OUT)/*
	if [ -d "$(OUT)" ]; then rmdir --ignore-fail-on-non-empty $(OUT); fi

.PHONY: check_dependencies
check_dependencies:
ifeq ($(HAVE_X11),1)
	$(error "X11 extension not found")
endif
ifeq ($(HAVE_XTEST),1)
	$(error "Xtests extension not found: consider install libxtst-dev")
endif
ifeq ($(HAVE_XFIXES),1)
	$(error "Xfixes extension not found: consider install libxfixes-dev")
endif
ifeq ($(HAVE_XRANDR),1)
	$(error "Xrandr extension not found: consider install libxrandr-dev")
endif
ifeq ($(HAVE_VNCSERVER),1)
	$(error "Xrandr extension not found: consider install libvncserver-dev")
endif
	@echo "All dependencies satisfied :)"
