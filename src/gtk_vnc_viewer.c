#include "gtk_vnc_viewer.h"

static encoding vnc_encodings[] = {
    {"raw", FALSE},
    {"hextile", FALSE},
    {"copyrect", FALSE},
    {"ultra", FALSE},
    {"rre", FALSE},
    {"corre", FALSE},
    {"hextile", FALSE},
#ifdef LIBVNCSERVER_HAVE_LIBZ
    {"zlib", TRUE},
    {"zrle", TRUE},
#ifdef LIBVNCSERVER_HAVE_LIBJPEG
    {"tight", TRUE},
#endif
#endif
    {NULL, 0}};

static struct
{
  char mask;
  int bits_stored;
} kb_utf8Mapping[] = {
    {0b00111111, 6},
    {0b01111111, 7},
    {0b00011111, 5},
    {0b00001111, 4},
    {0b00000111, 3},
    {0, 0}};

/************************* GUI *************************/
static rfbBool init_rfb(MyVncViewerGUI *gui, int *argc, char **argv);

// static gboolean on_key_press(GtkWidget *widget, GdkEventKey *event, gpointer data);
// static gboolean on_button_press(GtkWidget *widget, GdkEventButton *event);
SUPPRESS_UNUSED_FUN static gboolean
on_key_released_or_pressed(GtkWidget *widget, GdkEventMotion *event, gpointer data);
SUPPRESS_UNUSED_FUN static gboolean
on_mousewheel_event(GtkWidget *widget, GdkEventMotion *event, gpointer data);
SUPPRESS_UNUSED_FUN static gboolean
on_motion_notify_event(GtkWidget *widget, GdkEventMotion *event, gpointer data);

// SUPPRESS_UNUSED_FUN static void set_status_message(MyGUI *gui, const char *msg, int msgtype);
// SUPPRESS_UNUSED_FUN static gboolean on_timeout(gpointer data);

SUPPRESS_UNUSED_FUN static gboolean
draw_cb(GtkWidget *widget, cairo_t *cr, gpointer data);
/* Create a new surface of the appropriate size to store our scribbles */
SUPPRESS_UNUSED_FUN static gboolean
configure_event_cb(GtkWidget *widget, GdkEventConfigure *event, gpointer data);
SUPPRESS_UNUSED_FUN static void
button_toggled_cb(GtkWidget *button, gpointer data);
SUPPRESS_UNUSED_FUN static gboolean
process_rfb_cb(gpointer data);
/************************* GUI *************************/

/************************* RFB *************************/
SUPPRESS_UNUSED_FUN static void
update(rfbClient *cl, int x, int y, int w, int h);
SUPPRESS_UNUSED_FUN static rfbBool
resize(rfbClient *cl);
/* trivial support for textchat */
SUPPRESS_UNUSED_FUN static void
text_chat(rfbClient *cl, int value, char *text);
SUPPRESS_UNUSED_FUN static void
kbd_leds(rfbClient *cl, int value, int pad);
SUPPRESS_UNUSED_FUN static void
got_selection(rfbClient *cl, const char *text, int len);

SUPPRESS_UNUSED_FUN static rfbKeySym
gtk_keyevent_to_rfb_keysym(GdkEventKey *key_event);
SUPPRESS_UNUSED_FUN static rfbKeySym
utf8char2rfbKeySym(const char chr[4]);
/************************* RFB *************************/

/************************* GUI *************************/
static gboolean
on_key_released_or_pressed(GtkWidget *widget, GdkEventMotion *event, gpointer data)
{
  MyVncViewerGUI *gui = (MyVncViewerGUI *)data;
  GdkEventKey *key_event;

  if (gui == NULL || gui->cl == NULL)
    return FALSE;
  if (gui->cl->appData.viewOnly)
    return TRUE;

  key_event = (GdkEventKey *)event;

  switch (event->type)
  {
  case GDK_KEY_RELEASE:
  case GDK_KEY_PRESS:
    SendKeyEvent(gui->cl, gtk_keyevent_to_rfb_keysym(key_event),
                 event->type == GDK_KEY_PRESS ? TRUE : FALSE);
    break;
  default:
    return FALSE;
  }
  return TRUE;
}

static gboolean
on_mousewheel_event(GtkWidget *widget, GdkEventMotion *event, gpointer data)
{
  MyVncViewerGUI *gui = (MyVncViewerGUI *)data;
  SUPPRESS_UNUSED_VAR(gui);
  GdkEventScroll *scroll_event;
  int steps;

  if (gui == NULL || gui->cl == NULL)
    return FALSE;
  if (gui->cl->appData.viewOnly)
    return TRUE;

  if (event->type != GDK_SCROLL)
    return FALSE;

  scroll_event = (GdkEventScroll *)event;

  switch (scroll_event->direction)
  {
  case GDK_SCROLL_LEFT:
  case GDK_SCROLL_RIGHT:
  case GDK_SCROLL_SMOOTH:
    break;
  case GDK_SCROLL_DOWN:
    g_print("Scroll down: %f %f %f %f-> %f\n",
            scroll_event->x_root, scroll_event->y_root,
            scroll_event->x, scroll_event->y,
            scroll_event->delta_y);
    for (steps = 0; steps > scroll_event->delta_y; --steps)
    {
      SendPointerEvent(gui->cl, scroll_event->x_root, scroll_event->y_root, rfbWheelDownMask);
      SendPointerEvent(gui->cl, scroll_event->x_root, scroll_event->y_root, 0);
    }
    break;
  case GDK_SCROLL_UP:
    // g_print("Scroll up: %d %d\n", scroll_event->x, scroll_event->y);
    for (steps = 0; steps < 10; ++steps)
    {
      SendPointerEvent(gui->cl, 200, 200, rfbWheelUpMask);
      SendPointerEvent(gui->cl, 200, 200, 0);
    }
    break;
  default:
    return FALSE;
  }
  return TRUE;
}

static gboolean
on_motion_notify_event(GtkWidget *widget, GdkEventMotion *event, gpointer data)
{
  MyVncViewerGUI *gui = (MyVncViewerGUI *)data;
  int mouse_x, mouse_y;
  GdkModifierType state;

  if (gui == NULL || gui->cl == NULL)
    return FALSE;
  if (gui->cl->appData.viewOnly)
    return TRUE;

  if (event->type == GDK_2BUTTON_PRESS || event->type == GDK_3BUTTON_PRESS)
    return TRUE;

  if (event->is_hint)
  {
    gdk_window_get_device_position(event->window, event->device, &mouse_x, &mouse_y, &state);
    // gdk_window_get_pointer(event->window, &mouse_x, &mouse_y, &state);
    //TODO
    // g_print ("on_motion_notify_event: event->is_hint TODO");
  }
  else
  {
    mouse_x = event->x;
    mouse_y = event->y;
    state = event->state;
  }

  switch (event->type)
  {
  case GDK_BUTTON_RELEASE:
    if (gui->last_button_press_state & GDK_BUTTON1_MASK)
      gui->buttonMask &= ~(rfbButton1Mask);
    else if (gui->last_button_press_state & GDK_BUTTON2_MASK)
      gui->buttonMask &= ~(rfbButton2Mask);
    else if (gui->last_button_press_state & GDK_BUTTON3_MASK)
      gui->buttonMask &= ~(rfbButton3Mask);

    gui->last_button_press_state = 0;
    break;
  case GDK_BUTTON_PRESS:
    gui->last_button_press_state = state;

    if (state & GDK_BUTTON1_MASK)
      gui->buttonMask |= rfbButton1Mask;
    else if (state & GDK_BUTTON2_MASK)
      gui->buttonMask |= rfbButton2Mask;
    else if (state & GDK_BUTTON3_MASK)
      gui->buttonMask |= rfbButton3Mask;
    break;
  case GDK_MOTION_NOTIFY:
    break;
  default:
    break;
  }
  SendPointerEvent(gui->cl, mouse_x, mouse_y, gui->buttonMask);
  return TRUE;
}

static gboolean
draw_cb(GtkWidget *widget, cairo_t *cr, gpointer data)
{
  MyVncViewerGUI *gui = (MyVncViewerGUI *)data;
  SUPPRESS_UNUSED_VAR(gui);
  if (gui == NULL)
    return FALSE;
  if (gui->surface == NULL)
    return FALSE;
  cairo_set_source_surface(cr, gui->surface, 0, 0);
  cairo_paint(cr);
  return TRUE;
}

static gboolean
configure_event_cb(GtkWidget *widget, GdkEventConfigure *event, gpointer data)
{
  MyVncViewerGUI *gui = (MyVncViewerGUI *)data;
  SUPPRESS_UNUSED_VAR(gui);
  // TODO
  return FALSE;
}

static void button_toggled_cb(GtkWidget *button, gpointer data)
{
  MyVncViewerGUI *gui = (MyVncViewerGUI *)data;

  if (gui == NULL || gui->cl == NULL)
    return;

  const char *encoding_str = gtk_menu_item_get_label(GTK_MENU_ITEM(button));

  if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(button)))
  {
    if (strcmp(gui->cl->appData.encodingsString, encoding_str) == 0)
      return;

    encoding *enc;
    for (enc = vnc_encodings; enc->name; ++enc)
    {
      if (strcmp(encoding_str, enc->name) == 0)
      {
        printf("%s <= %s\n", gui->rfbSupportedEncoding, enc->name);

        gui->cl->appData.encodingsString = strdup(enc->name);
        if (enc->have_compression)
          gui->cl->appData.compressLevel = 5;
        if (strcmp(enc->name, "tight") == 0)
        {
          gui->cl->appData.enableJPEG = TRUE;
          gui->cl->appData.qualityLevel = 5;
        }
        SetFormatAndEncodings(gui->cl);
        break;
      }
    }
  }
}

static void on_sock_connect_cb(GObject /*GSocketConnection*/ *obj, GAsyncResult *res, gpointer data)
{
  MyVncViewerGUI *gui = (MyVncViewerGUI *)data;
  GSocketConnection *conn = (GSocketConnection *)obj;
  GSocket *gsock = NULL;
  GCancellable *gcancel = NULL;
  GError *gerr = NULL;
  // gboolean ret = 0;
  int bytes_recv;
  // gchar buffer[256];
  SUPPRESS_UNUSED_VAR(bytes_recv);
  //gsock_conn = (GSocketConnection *)source_object;

  if (!g_socket_connection_connect_finish(conn, res, &gerr))
  {
    // set_status_message(gui, "Connection failed", MSG_ERROR);
    fprintf(stderr, "Connection failed: %s\n", gerr->message);
    return;
  }

  // Socket is connected
  // printf("GSocketConnection: [%p]\n\n", conn);
  // printf("####################: [%p]\n\n", conn);

  printf("---------------------------------------------------\n");

  if (g_socket_address_get_family(gui->gaddr) == G_SOCKET_FAMILY_IPV4)
  {
    printf("######CONNECTED TO IPv4 #######\n\n");
    GInetSocketAddress *inet_addr = (GInetSocketAddress *)gui->gaddr;
    printf("%s:%d\n",
           g_inet_address_to_string(g_inet_socket_address_get_address(inet_addr)),
           g_inet_socket_address_get_port(inet_addr));

    // g_socket_connection_get_local_address(conn, NULL);
    // g_socket_connection_get_remote_address(conn, NULL);
  }

  gsock = g_socket_connection_get_socket(conn);

  // printf("fd: %d fd: %d\n\n", g_socket_get_fd(gsock), g_socket_get_fd(gui->gsock));

  init_rfb(gui, 0, 0);
  return;

  gsock = g_socket_connection_get_socket(conn);

  gcancel = g_cancellable_new();

  // g_socket_send(gsock, "aaa\n", 4, gcancel, &gerr);
  // g_socket_set_blocking(gsock, 1);

  do
  {
    if (gerr)
    {
      if (gerr->code == G_IO_ERROR_TIMED_OUT)
      {
        fprintf(stderr, "Timeput exceeded!\n");
      }
      g_error_free(gerr);
    }
    gerr = NULL;

  } while (!g_socket_condition_timed_wait(gsock, G_IO_IN, (gint64)2e+6, gcancel, &gerr));

  // ProxyInitMsg msg;

  // bytes_recv = g_socket_receive(gsock, (gchar *)&msg, sz_ProxyInitMsg, gcancel, &gerr);

  // gtk_label_set_text(GTK_LABEL(gui->id_field), msg.sessid);
  // gtk_label_set_text(GTK_LABEL(gui->pass_field), msg.passwd);
  // write(1, buffer, bytes_recv);

  // set_status_message(gui, "Successfully conected", MSG_SUCCESS);
  printf("Successfully conected");
}

static gboolean process_rfb_cb(gpointer data)
{
  MyVncViewerGUI *gui = (MyVncViewerGUI *)data;
  int ret = 0;

  if (gui->cl == NULL)
    return FALSE;

  ret = WaitForMessage(gui->cl, 5000);
  if (ret < 0)
    return FALSE;

  if (ret)
  {
    if (!HandleRFBServerMessage(gui->cl))
      return FALSE;
  }
  return TRUE;
}

static gboolean on_exit_menu_item(GtkWidget *widget, gpointer data)
{
  // TODO
  return TRUE;
}

static gboolean on_view_only_change(GtkWidget *widget, gpointer data)
{
  MyVncViewerGUI *gui = (MyVncViewerGUI *)data;
  if (gui == NULL || gui->menu_item_check_view_only == NULL)
    return FALSE;

  if (gtk_check_menu_item_get_active(GTK_CHECK_MENU_ITEM(gui->menu_item_check_view_only)))
    gui->cl->appData.viewOnly = TRUE;
  else
    gui->cl->appData.viewOnly = FALSE;
  return TRUE;
}
/************************* GUI *************************/

/************************* RFB *************************/
static void update(rfbClient *cl, int x, int y, int w, int h)
{
  MyVncViewerGUI *gui = (MyVncViewerGUI *)rfbClientGetClientData(cl, vnc_viewer_gui_init);
  gtk_widget_queue_draw_area(gui->drawing_area, x, y, w, h);
}

static void text_chat(rfbClient *cl, int value, char *text)
{
  MyVncViewerGUI *gui = (MyVncViewerGUI *)rfbClientGetClientData(cl, vnc_viewer_gui_init);
  SUPPRESS_UNUSED_VAR(gui);
  // TODO
}

static rfbBool resize(rfbClient *cl)
{
  MyVncViewerGUI *gui = (MyVncViewerGUI *)rfbClientGetClientData(cl, vnc_viewer_gui_init);
  int width, height, depth;

  PRINT("Resize: %d %d\n", cl->width, cl->height);

  gtk_widget_set_size_request(gui->drawing_area, gui->cl->width, gui->cl->height);

  width = cl->width;
  height = cl->height;
  depth = cl->format.bitsPerPixel;

  cl->updateRect.x = 0;
  cl->updateRect.y = 0;
  cl->updateRect.w = width;
  cl->updateRect.h = height;

  if (gui->surface)
  {
    cairo_surface_destroy(gui->surface);
    gui->surface = NULL;
    cl->frameBuffer = NULL;
  }

  unsigned char *data;
  int stride;
  cairo_surface_t *surface;

  surface = cairo_image_surface_create(CAIRO_FORMAT_RGB24, width, height);

  if (cairo_surface_status(surface) != CAIRO_STATUS_SUCCESS)
  {
    fprintf(stderr, "Failed to: cairo_surface_status\n");
    return FALSE;
  }

  data = cairo_image_surface_get_data(surface);
  stride = cairo_image_surface_get_stride(surface);

  memset(data, 0xff, stride * height);

  cl->width = stride / (depth / 8);
  cl->frameBuffer = data;
  cl->format.bitsPerPixel = depth;
  cl->format.redShift = 16;
  cl->format.greenShift = 8;
  cl->format.blueShift = 0;
  cl->format.redMax = 0xff;
  cl->format.greenMax = 0xff;
  cl->format.blueMax = 0xff;

  if (!SetFormatAndEncodings(cl))
  {
    PRINT("Error SetFormatAndEncodings\n");
  }
  gui->surface = surface;

  return TRUE;
}

static void kbd_leds(rfbClient *cl, int value, int pad)
{
  MyVncViewerGUI *gui = (MyVncViewerGUI *)rfbClientGetClientData(cl, vnc_viewer_gui_init);
  SUPPRESS_UNUSED_VAR(gui);
  /* note: pad is for future expansion 0=unused */
  // TODO
  g_print("Led State= 0x%02X\n", value);
}

static void got_selection(rfbClient *cl, const char *text, int len)
{
  MyVncViewerGUI *gui = (MyVncViewerGUI *)rfbClientGetClientData(cl, vnc_viewer_gui_init);
  g_print("received clipboard text '%s'\n", text);
  gtk_clipboard_set_text(gui->clipboard, text, len);
}

static rfbKeySym gtk_keyevent_to_rfb_keysym(GdkEventKey *key_event)
{
  rfbKeySym k = 0;

  switch (key_event->keyval)
  {
  case GDK_KEY_BackSpace:
    k = XK_BackSpace;
    break;
  case GDK_KEY_Tab:
    k = XK_Tab;
    break;
  case GDK_KEY_Clear:
    k = XK_Clear;
    break;
  case GDK_KEY_Return:
    k = XK_Return;
    break;
  case GDK_KEY_Pause:
    k = XK_Pause;
    break;
  case GDK_KEY_Escape:
    k = XK_Escape;
    break;
  case GDK_KEY_Delete:
    k = XK_Delete;
    break;
  case GDK_KEY_KP_0:
    k = XK_KP_0;
    break;
  case GDK_KEY_KP_1:
    k = XK_KP_1;
    break;
  case GDK_KEY_KP_2:
    k = XK_KP_2;
    break;
  case GDK_KEY_KP_3:
    k = XK_KP_3;
    break;
  case GDK_KEY_KP_4:
    k = XK_KP_4;
    break;
  case GDK_KEY_KP_5:
    k = XK_KP_5;
    break;
  case GDK_KEY_KP_6:
    k = XK_KP_6;
    break;
  case GDK_KEY_KP_7:
    k = XK_KP_7;
    break;
  case GDK_KEY_KP_8:
    k = XK_KP_8;
    break;
  case GDK_KEY_KP_9:
    k = XK_KP_9;
    break;
  case GDK_KEY_KP_Decimal:
    k = XK_KP_Decimal;
    break;
  case GDK_KEY_KP_Divide:
    k = XK_KP_Divide;
    break;
  case GDK_KEY_KP_Multiply:
    k = XK_KP_Multiply;
    break;
  case GDK_KEY_KP_Subtract:
    k = XK_KP_Subtract;
    break;
  case GDK_KEY_KP_Add:
    k = XK_KP_Add;
    break;
  case GDK_KEY_KP_Enter:
    k = XK_KP_Enter;
    break;
  case GDK_KEY_KP_Equal:
    k = XK_KP_Equal;
    break;
  case GDK_KEY_Up:
    k = XK_Up;
    break;
  case GDK_KEY_Down:
    k = XK_Down;
    break;
  case GDK_KEY_Right:
    k = XK_Right;
    break;
  case GDK_KEY_Left:
    k = XK_Left;
    break;
  case GDK_KEY_Insert:
    k = XK_Insert;
    break;
  case GDK_KEY_Home:
    k = XK_Home;
    break;
  case GDK_KEY_End:
    k = XK_End;
    break;
  case GDK_KEY_Page_Up:
    k = XK_Page_Up;
    break;
  case GDK_KEY_Page_Down:
    k = XK_Page_Down;
    break;
  case GDK_KEY_F1:
    k = XK_F1;
    break;
  case GDK_KEY_F2:
    k = XK_F2;
    break;
  case GDK_KEY_F3:
    k = XK_F3;
    break;
  case GDK_KEY_F4:
    k = XK_F4;
    break;
  case GDK_KEY_F5:
    k = XK_F5;
    break;
  case GDK_KEY_F6:
    k = XK_F6;
    break;
  case GDK_KEY_F7:
    k = XK_F7;
    break;
  case GDK_KEY_F8:
    k = XK_F8;
    break;
  case GDK_KEY_F9:
    k = XK_F9;
    break;
  case GDK_KEY_F10:
    k = XK_F10;
    break;
  case GDK_KEY_F11:
    k = XK_F11;
    break;
  case GDK_KEY_F12:
    k = XK_F12;
    break;
  case GDK_KEY_F13:
    k = XK_F13;
    break;
  case GDK_KEY_F14:
    k = XK_F14;
    break;
  case GDK_KEY_F15:
    k = XK_F15;
    break;
  case GDK_KEY_Num_Lock:
    k = XK_Num_Lock;
    break;
  case GDK_KEY_Caps_Lock:
    k = XK_Caps_Lock;
    break;
  case GDK_KEY_Scroll_Lock:
    k = XK_Scroll_Lock;
    break;
  case GDK_KEY_Shift_R:
    k = XK_Shift_R;
    break;
  case GDK_KEY_Shift_L:
    k = XK_Shift_L;
    break;
  case GDK_KEY_Control_R:
    k = XK_Control_R;
    break;
  case GDK_KEY_Control_L:
    k = XK_Control_L;
    break;
  case GDK_KEY_Alt_R:
    k = XK_Alt_R;
    break;
  case GDK_KEY_Alt_L:
    k = XK_Alt_L;
    break;
  case GDK_KEY_Super_L:
    k = XK_Super_L;
    break;
  case GDK_KEY_Super_R:
    k = XK_Super_R;
    break;
  case GDK_KEY_Mode_switch:
    k = XK_Mode_switch;
    break;
  case GDK_KEY_Help:
    k = XK_Help;
    break;
  case GDK_KEY_Print:
    k = XK_Print;
    break;
  case GDK_KEY_Sys_Req:
    k = XK_Sys_Req;
    break;
  default:
    break;
  }

  //TODO
  if (k == 0 &&
      key_event->keyval > 0x0 && key_event->keyval < 0x100) // &&
    // key_event->state & GDK_CONTROL_MASK)
    k = gdk_keyval_to_unicode(key_event->keyval);
  // {
  //   k = utf8char2rfbKeySym((const char *)&key_event->keyval);
  // }

  return k;
}

static rfbKeySym utf8char2rfbKeySym(const char chr[4])
{
  int bytes = strlen(chr);
  int shift = kb_utf8Mapping[0].bits_stored * (bytes - 1);
  rfbKeySym codep = (*chr++ & kb_utf8Mapping[bytes].mask) << shift;
  int i;
  for (i = 1; i < bytes; ++i, ++chr)
  {
    shift -= kb_utf8Mapping[0].bits_stored;
    codep |= ((char)*chr & kb_utf8Mapping[0].mask) << shift;
  }
  return codep;
}

static rfbBool init_rfb(MyVncViewerGUI *gui, int *argc, char **argv)
{
  gui->cl = rfbGetClient(BITS_PER_SAMPLE, SAMPLES_PER_PIXEL, BYTES_PER_PIXEL);

  rfbClientSetClientData(gui->cl, vnc_viewer_gui_init, gui);

  // initAppData(AppData* data)

  gui->cl->MallocFrameBuffer = resize;
  gui->cl->canHandleNewFBSize = TRUE;
  gui->cl->GotFrameBufferUpdate = update;
  gui->cl->HandleKeyboardLedState = kbd_leds;
  gui->cl->HandleTextChat = text_chat;
  gui->cl->GotXCutText = got_selection;

  gui->cl->sock = g_socket_get_fd(gui->gsock);
  gui->cl->appData.shareDesktop = FALSE;

  if (!InitialiseRFBConnection(gui->cl))
  {
    fprintf(stderr, "Failed: InitialiseRFBConnection\n");
    return FALSE;
  }

  gui->cl->width = gui->cl->si.framebufferWidth;
  gui->cl->height = gui->cl->si.framebufferHeight;

  /* set a minimum size of the drawing area */
  gtk_widget_set_size_request(gui->drawing_area, gui->cl->width, gui->cl->height);
  if (gui->cl->desktopName != NULL && strlen(gui->cl->desktopName))
  {
    gtk_window_set_title(GTK_WINDOW(gui->window), gui->cl->desktopName);
  }

  printf("Server supported encodings: %s\n\n", gui->cl->appData.encodingsString);

  if (!gui->cl->MallocFrameBuffer(gui->cl))
  {
    fprintf(stderr, "Failed: MallocFrameBuffer\n");
    return FALSE;
  }

  gui->rfbSupportedEncoding = strdup(gui->cl->appData.encodingsString);

  if (gui->cl->updateRect.x < 0)
  {
    gui->cl->updateRect.x = 0;
    gui->cl->updateRect.y = 0;
    gui->cl->updateRect.w = gui->cl->width;
    gui->cl->updateRect.h = gui->cl->height;
  }

  if (!SendFramebufferUpdateRequest(gui->cl,
                                    gui->cl->updateRect.x,
                                    gui->cl->updateRect.y,
                                    gui->cl->updateRect.w,
                                    gui->cl->updateRect.h, FALSE))
  {
    return FALSE;
  }

  g_print("bpp=%d, rshift=%d, gshift=%d, bshift=%d\n",
          gui->cl->format.bitsPerPixel,
          gui->cl->format.redShift,
          gui->cl->format.greenShift,
          gui->cl->format.blueShift);

  return TRUE;
}
/************************* RFB *************************/

void vnc_viewer_gui_set_socket(MyVncViewerGUI *gui, int sock)
{
  gui->gerror = NULL;
  gui->gsock = g_socket_new_from_fd(sock, &gui->gerror);
  if (!gui->gsock)
  {
    fprintf(stderr, "gui_connect: %s\n", gui->gerror->message);
    return;
  }
  gui->gsock_conn = g_socket_connection_factory_create_connection(gui->gsock);
  printf("GSocketConnection: ---[%p]\n\n", gui->gsock_conn);

  init_rfb(gui, NULL, NULL);
}

void vnc_viewer_gui_init(MyVncViewerGUI *gui)
{
  gtk_init(0, NULL);
  memset(gui, 0, sizeof(MyVncViewerGUI));

  g_assert((gui->window = gtk_window_new(GTK_WINDOW_TOPLEVEL)));
  g_assert((gui->menu_bar = gtk_menu_bar_new()));
  g_assert((gui->settings_menu_item = gtk_menu_item_new_with_label("Settings")));
  g_assert((gui->menu_item_check_view_only = gtk_check_menu_item_new_with_label("View Only")));
  g_assert((gui->exit_menu_item = gtk_menu_item_new_with_label("Exit")));
  g_assert((gui->settings_menu = gtk_menu_new()));
  g_assert((gui->frame = gtk_frame_new(NULL)));
  g_assert((gui->drawing_area = gtk_drawing_area_new()));
  g_assert((gui->grid = gtk_grid_new()));
  g_assert((gui->scrolled_window = gtk_scrolled_window_new(NULL, NULL)));
  g_assert((gui->viewport = gtk_viewport_new(NULL, NULL)));
  // Get a handle to the given clipboard. You can also ask for
  // GDK_SELECTION_SECONDARY.
  // GDK_SELECTION_PRIMARY (the X "primary selection") or
  g_assert((gui->clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD)));

  // Set Menu
  // menu bar -> menu item -> menu, menu, menu
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(gui->settings_menu_item), gui->settings_menu);

  g_signal_connect(gui->menu_item_check_view_only, "activate",
                   G_CALLBACK(on_view_only_change), gui);
  g_signal_connect(gui->exit_menu_item, "activate",
                   G_CALLBACK(on_exit_menu_item), gui);

  // Encodings menu item
  GtkWidget *settings_encodings_menu_item = gtk_menu_item_new_with_label("Encodings");
  GtkWidget *settings_encodings_menu_item_submenu = gtk_menu_new();
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(settings_encodings_menu_item),
                            settings_encodings_menu_item_submenu);

  GSList *encodings_group = NULL;

  GtkWidget *item;
  encoding *enc;

  for (enc = vnc_encodings; enc->name; ++enc)
  {
    printf("encoding: %s, have compression:", enc->name);
    item = gtk_radio_menu_item_new_with_label(encodings_group, enc->name);
    encodings_group = gtk_radio_menu_item_get_group(GTK_RADIO_MENU_ITEM(item));

    g_signal_connect(GTK_WIDGET(item), "activate",
                     G_CALLBACK(button_toggled_cb), gui);

    gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(item), TRUE);
    gtk_menu_shell_append(GTK_MENU_SHELL(settings_encodings_menu_item_submenu), item);
  }

  GSList *iter;
  for (iter = encodings_group; iter; iter = iter->next)
  {
    printf("===> %s\n", gtk_menu_item_get_label(GTK_MENU_ITEM(iter->data)));
  }
  /////////////////////////

  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(gui->menu_item_check_view_only), FALSE);

  gtk_menu_shell_append(GTK_MENU_SHELL(gui->settings_menu), gui->menu_item_check_view_only);
  // gtk_menu_shell_append(GTK_MENU_SHELL(gui->settings_menu), menu_item_radio);
  gtk_menu_shell_append(GTK_MENU_SHELL(gui->settings_menu), settings_encodings_menu_item);

  gtk_menu_shell_append(GTK_MENU_SHELL(gui->menu_bar), gui->settings_menu_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(gui->menu_bar), gui->exit_menu_item);

  GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

  gtk_box_pack_start(GTK_BOX(vbox), gui->menu_bar, FALSE, FALSE, 2);
  gtk_widget_show(gui->menu_bar);
  // gtk_widget_show(gui->settings_menu);
  // gtk_widget_show(gui->menu_item_view_only);

  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(gui->scrolled_window),
                                 GTK_POLICY_AUTOMATIC,
                                 GTK_POLICY_AUTOMATIC);
  gtk_widget_set_hexpand(GTK_WIDGET(gui->scrolled_window), TRUE);
  gtk_widget_set_vexpand(GTK_WIDGET(gui->scrolled_window), TRUE);

  gtk_widget_set_can_focus(GTK_WIDGET(gui->drawing_area), TRUE);
  gtk_widget_add_events(GTK_WIDGET(gui->drawing_area), GDK_SCROLL_MASK);
  gtk_widget_add_events(GTK_WIDGET(gui->drawing_area), GDK_KEY_RELEASE_MASK | GDK_KEY_PRESS_MASK);
  gtk_widget_add_events(GTK_WIDGET(gui->drawing_area), GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK);
  gtk_widget_add_events(GTK_WIDGET(gui->drawing_area), GDK_POINTER_MOTION_MASK);
  // gtk_widget_add_events(GTK_WIDGET(gui->drawing_area), GDK_BUTTON_MOTION_MASK);
  // gtk_widget_add_events(GTK_WIDGET(gui->drawing_area), GDK_POINTER_MOTION_HINT_MASK);

  gtk_frame_set_shadow_type(GTK_FRAME(gui->frame), GTK_SHADOW_IN);

  gtk_container_add(GTK_CONTAINER(gui->viewport), gui->drawing_area);
  gtk_container_add(GTK_CONTAINER(gui->scrolled_window), gui->viewport);

  gtk_grid_attach(GTK_GRID(gui->grid), gui->scrolled_window, 0, 0, 1, 1);

  gtk_container_add(GTK_CONTAINER(gui->frame), GTK_WIDGET(gui->grid));

  gtk_box_pack_start(GTK_BOX(vbox), gui->frame, TRUE, TRUE, 0);
  gtk_container_add(GTK_CONTAINER(gui->window), vbox);
  // gtk_container_add(GTK_CONTAINER(gui->window), GTK_WIDGET(gui->frame));
  //  gtk_widget_show (vbox);

  /* set a minimum size of the drawing area */
  // gtk_widget_set_size_request(gui->drawing_area, 1000, 1000);

  // Set the title, border, minimum size of the main window
  gtk_window_set_title(GTK_WINDOW(gui->window), "vncViewer");
  // gtk_container_set_border_width(GTK_CONTAINER(gui->window), 2);
  gtk_widget_set_margin_start(GTK_WIDGET(gui->frame), 2);
  gtk_widget_set_margin_end(GTK_WIDGET(gui->frame), 2);
  gtk_widget_set_margin_bottom(GTK_WIDGET(gui->frame), 2);
  gtk_widget_set_size_request(gui->window, MIN_WIN_WIDTH, MIN_WIN_HEIGHT);

  /* Add signals */
  // g_signal_connect(gui->window, "destroy", G_CALLBACK(gtk_main_quit), NULL);
  // g_signal_connect(gui->window, "destroy",
  //                  G_CALLBACK(vnc_viewer_gui_destroy), NULL);
  g_signal_connect(gui->window, "delete-event",
                   G_CALLBACK(vnc_viewer_gui_destroy), (gpointer)gui);

  printf("\033[32m\033[01m gui=%p\033[00m\n\n", gui);

  /* Signals used to handle the backing surface */
  g_signal_connect(gui->drawing_area, "draw",
                   G_CALLBACK(draw_cb), gui);
  // g_signal_connect(gui->drawing_area, "expose_event",
  //                  G_CALLBACK(draw_cb), NULL);
  g_signal_connect(gui->drawing_area, "configure-event",
                   G_CALLBACK(configure_event_cb), gui);

  // Window events
  g_signal_connect(GTK_WIDGET(gui->drawing_area), "motion_notify_event",
                   G_CALLBACK(on_motion_notify_event), gui);
  g_signal_connect(GTK_WIDGET(gui->drawing_area), "button_press_event",
                   G_CALLBACK(on_motion_notify_event), gui);
  g_signal_connect(GTK_WIDGET(gui->drawing_area), "button_release_event",
                   G_CALLBACK(on_motion_notify_event), gui);
  g_signal_connect(GTK_WIDGET(gui->drawing_area), "scroll_event",
                   G_CALLBACK(on_mousewheel_event), gui);
  g_signal_connect(GTK_WIDGET(gui->drawing_area), "key_release_event",
                   G_CALLBACK(on_key_released_or_pressed), gui);
  g_signal_connect(GTK_WIDGET(gui->drawing_area), "key_press_event",
                   G_CALLBACK(on_key_released_or_pressed), gui);

  gtk_widget_show_all(gui->window);
}

void vnc_viewer_gui_main(MyVncViewerGUI *gui)
{
  if (gui == NULL)
    return;

  g_idle_add(process_rfb_cb, (gpointer)gui);
  // gtk_main();
}

gboolean vnc_viewer_gui_destroy(GtkWidget *widget, GdkEvent *event, gpointer data)
{
  printf("vnc_viewer_gui_destroy called\n");

  MyVncViewerGUI *gui = NULL;

  if (data == NULL)
    return FALSE;

  gui = (MyVncViewerGUI *)data;

  // GUI cleanup
  g_idle_remove_by_data((gpointer)gui);

  if (gui->cl)
    rfbClientCleanup(gui->cl);

  if (gui->gcancel != NULL)
    g_cancellable_release_fd(gui->gcancel);

  if (gui->gsock != NULL)
    g_socket_close(gui->gsock, NULL);

  if (gui->gerror != NULL)
    g_error_free(gui->gerror);

  if (gui->surface)
    cairo_surface_destroy(gui->surface);

  g_print("GUI cleanup done\n");
  // gtk_main_quit();
}