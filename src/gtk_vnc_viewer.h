#ifndef __GTK_VNC_VIEWER_H__
#define __GTK_VNC_VIEWER_H__

#include <gtk/gtk.h>
#include <rfb/rfbclient.h>
#include <cairo.h>

#define PRINT(__fmt, ...) g_print(__fmt, ##__VA_ARGS__)

#define SUPPRESS_UNUSED_FUN __attribute__((unused))
#define SUPPRESS_UNUSED_VAR(__var) (void)__var

/* 16-bit: cl=rfbGetClient(5,3,2); */

/* 32-bit */
#define BITS_PER_SAMPLE 8
#define SAMPLES_PER_PIXEL 3
#define BYTES_PER_PIXEL 4

#define PROXY_ADDRESS "192.168.1.6"
#define PROXY_PORT 7777

#define SERVER_IP "127.0.0.1"
#define SERVER_PORT 5903
// #define SERVER_IP "128.224.170.135"
// #define SERVER_PORT 5902

#define MIN_WIN_WIDTH 300
#define MIN_WIN_HEIGHT 400

#define MSG_ERROR -1
#define MSG_SUCCESS 0
#define MSG_LOADING 1

// Encodings: tight zrle ultra copyrect hextile zlib corre rre raw
typedef struct
{
  char *name;
  char have_compression;
} encoding;

typedef struct __MyVncViewerGUI
{
  // Box
  GtkWidget *hbox, *btnbox;
  GtkWidget *window, *button, *label;

  GtkWidget *menu_bar;
  GtkWidget *settings_menu_item, *exit_menu_item;
  GtkWidget *settings_menu, *menu_item_view_only;
  GtkWidget *menu_item_check_view_only;

  GtkWidget *grid;
  GtkWidget *scrolled_window;
  GtkWidget *viewport;

  GtkWidget *id_label, *pass_label;
  GtkWidget *id_field, *pass_field;

  GtkWidget *top_inner_hbox;
  GtkWidget *sessid_field;
  GtkWidget *connect_btn;

  GtkWidget *hseparator;

  GtkWidget *frame;
  GtkWidget *drawing_area;
  cairo_surface_t *surface;

  GtkWidget *status_image, *status_label;

  GtkClipboard *clipboard;

  // last message
  int msg_type;
  char *msg;

  // Box
  GtkWidget *root_vbox, *top_hbox, *bottom_hbox, *status_hbox;

  GdkPixbuf *pbuf;
  double scale_x, scale_y;

  // g socket connection
  GError *gerror;
  GSocketAddress *gaddr;
  GCancellable *gcancel;
  GSocket *gsock;
  GSocketConnection *gsock_conn;

  // RFB
  rfbClient *cl;
  int buttonMask;
  GdkModifierType last_button_press_state;
  char *rfbSupportedEncoding;
} MyVncViewerGUI;

void vnc_viewer_gui_init(MyVncViewerGUI *gui);
void vnc_viewer_gui_main(MyVncViewerGUI *gui);
gboolean vnc_viewer_gui_destroy(GtkWidget *widget, GdkEvent *event, gpointer data);

#endif /*__GTK_VNC_VIEWER_H__*/