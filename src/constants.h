#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

#include <stdio.h>

#define COLOR_RED     "\033[31m"
#define COLOR_GREEN   "\033[32m"
#define COLOR_YELLOW  "\033[33m"
#define COLOR_NONE    "\033[00m"

#define PRINT_RED(__fmt, args...) \
  printf(COLOR_RED __fmt COLOR_NONE, ##args)

#define PRINT_GREEN(__fmt, args...) \
  printf(COLOR_GREEN __fmt COLOR_NONE, ##args)

#define PRINT_YELLOW(__fmt, args...) \
  printf(COLOR_YELLOW __fmt COLOR_NONE, ##args)

#endif /*__CONSTANTS_H__*/