#ifndef _X11_UTILS_H_
#define _X11_UTILS_H_

#include <X11/X.h>
#include <X11/Xlib.h>

#include <X11/extensions/XTest.h>
#include <X11/extensions/Xfixes.h>
#include <X11/extensions/Xrandr.h>

#include "types.h"
#include "bitmap.h"

BOOL write_X11Cursor_bmp(const char *filename, XFixesCursorImage *cim);
BOOL write_X11ScaledCursor_bmp(const char *filename, XFixesCursorImage *cim, INT32 x_scale, INT32 y_scale);

BOOL write_X11Image_bmp(const char *filename, XImage *img);
BOOL write_X11SubImage_bmp(const char *filename, XImage *img,
                           INT32 x1, INT32 y1, INT32 x2, INT32 y2);

BOOL write_X11Cursor_bmp(const char *filename, XFixesCursorImage *cim);
BOOL write_X11ScaledCursor_bmp(const char *filename, XFixesCursorImage *cim, INT32 x_scale, INT32 y_scale);

BOOL write_X11Image_bmp(const char *filename, XImage *img);
BOOL write_X11SubImage_bmp(const char *filename, XImage *img,
                           INT32 x1, INT32 y1, INT32 x2, INT32 y2);

#endif