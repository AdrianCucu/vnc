#include "vnc_server.h"

/*
 * Error handling.
 */
static int ErrorFlag = 0;

static int HandleXError(Display *dpy, XErrorEvent *evt);
static XImage *alloc_xshm_image(Display *dpy,
                                Visual *vis,
                                int width,
                                int height,
                                int depth,
                                XShmSegmentInfo *shminfo);
void destroy_xshm(Display *dpy, XShmSegmentInfo *shminfo);

static int check_for_xshm(Display *dpy);
static int check_for_xfixes(Display *dpy);
static int check_for_xkb(Display *dpy);

static void noob_X11VNCServer_compute_diff(VNCServerPtr srv);
static enum rfbNewClientAction newclient(rfbClientPtr cl);
static void clientgone(rfbClientPtr cl);
static void on_cursor_event(int buttonMask, int x, int y, rfbClientPtr cl);
static void on_key_event(rfbBool down, rfbKeySym keysym, rfbClientPtr cl);

/*
 * Error handling.
 */
static int HandleXError(Display *dpy, XErrorEvent *evt)
{
  char xerror[1024];
  XGetErrorText(dpy, evt->error_code, xerror, 1024);
  fprintf(stderr, "Error: %s\n", xerror);
  ErrorFlag = 1;
  return 0;
}

/*
 * Check if the X Shared Memory extension is available.
 * Return:  0 = not available
 *          1 = shared XImage support available
 *          2 = shared Pixmap support available also
 */
static int check_for_xshm(Display *dpy)
{
  int major, minor, ignore;
  Bool pixmaps;

  if (XQueryExtension(dpy, "MIT-SHM", &ignore, &ignore, &ignore))
    if (XShmQueryVersion(dpy, &major, &minor, &pixmaps))
      return pixmaps ? HAVE_SHM_PIXMAP : HAVE_SHM_XIMAGE;
  return SHM_NOT_AVAILABLE;
}

static int check_for_xfixes(Display *dpy)
{
  int event, error;
  if (!XFixesQueryExtension(dpy, &event, &error))
  {
    fprintf(stderr, "Warning: XFixes is not supported by X server, the cursor has been hidden.");
    return XFIXES_NOT_AVAILABLE;
  }
  return HAVE_XFIXES;
}

static int check_for_xkb(Display *dpy)
{
  //https://superuser.com/questions/248517/show-keys-pressed-in-linux
  // https://bharathisubramanian.wordpress.com/2010/03/14/x11-fake-key-event-generation-using-xtest-ext/
  int xkbOpcode, xkbErrorBase;
  int xkbEventBase;
  int major, minor;
  major = 1;
  minor = 0;

  if (!XkbQueryExtension(dpy, &xkbOpcode, &xkbEventBase, &xkbErrorBase, &major, &minor))
  {
    FATAL("XKEYBOARD extension not present");
    return XKB_NOT_AVAILABLE;
  }

  DEBUG("Keyboard extension: %d.%d\n", major, minor);

  XkbSelectEvents(dpy,
                  XkbUseCoreKbd,
                  XkbIndicatorStateNotifyMask,
                  XkbIndicatorStateNotifyMask);

#define XDESKTOP_N_LEDS 3
  static const char *ledNames[XDESKTOP_N_LEDS] = {
      "Scroll Lock", "Num Lock", "Caps Lock"};

  // figure out bit masks for the indicators we are interested in
  for (int i = 0; i < XDESKTOP_N_LEDS; i++)
  {
    Atom a;
    int shift;
    Bool on;

    a = XInternAtom(dpy, ledNames[i], True);
    if (!a || !XkbGetNamedIndicator(dpy, a, &shift, &on, NULL, NULL))
      continue;

    //ledMasks[i] = 1u << shift;
    if (on)
      printf("Mask for '%s' is 0x%x " COLOR_GREEN "ON\033[00m\n",
             ledNames[i], 1u << shift);
    else
      printf("Mask for '%s' is 0x%x " COLOR_RED "\033[31m OFF\033[00m\n",
             ledNames[i], 1u << shift);
    //if (on)
    //  ledState |= 1u << i;
  }

  // X11 unfortunately uses keyboard driver specific keycodes and provides no
  // direct way to query this, so guess based on the keyboard mapping
  XkbDescPtr desc = XkbGetKeyboard(dpy, XkbAllComponentsMask, XkbUseCoreKbd);
  if (desc && desc->names)
  {
    char *keycodes = XGetAtomName(dpy, desc->names->keycodes);
    printf("keycodes = %s\n", keycodes);
    if (keycodes)
    {

      if (strncmp("evdev", keycodes, strlen("evdev")) == 0)
      {
        //codeMap = code_map_qnum_to_xorgevdev;
        //codeMapLen = code_map_qnum_to_xorgevdev_len;
        printf("Using evdev codemap\n");
      }
      else if (strncmp("xfree86", keycodes, strlen("xfree86")) == 0)
      {
        //codeMap = code_map_qnum_to_xorgkbd;
        //codeMapLen = code_map_qnum_to_xorgkbd_len;
        printf("Using xorgkbd codemap\n");
      }
      else
      {
        printf("Unknown keycode '%s', no codemap\n", keycodes);
      }
      XFree(keycodes);
    }
    else
    {
      //vlog.debug("Unable to get keycode map\n");
    }
  }
  return HAVE_XKB;
}

/*
 * Allocate a shared memory XImage.
 */
static XImage *alloc_xshm_image(Display *dpy,
                                Visual *vis,
                                int width,
                                int height,
                                int depth,
                                XShmSegmentInfo *shminfo)
{
  XImage *img;

  assert(dpy);
  assert(vis);
  assert(shminfo);

  /*
    * We have to do a _lot_ of error checking here to be sure we can
    * really use the XSHM extension.  It seems different servers trigger
    * errors at different points if the extension won't work.  Therefore
    * we have to be very careful...
    */

  img = XShmCreateImage(dpy, vis, depth,
                        ZPixmap, NULL, shminfo,
                        width, height);
  if (img == NULL)
  {
    printf("XShmCreateImage failed!\n");
    return NULL;
  }

  shminfo->shmid = shmget(IPC_PRIVATE, img->bytes_per_line * img->height, IPC_CREAT | 0600);
  if (shminfo->shmid < 0)
  {
    perror("shmget");
    XDestroyImage(img);
    return NULL;
  }

  shminfo->shmaddr = img->data = (char *)shmat(shminfo->shmid, 0, 0);
  if (shminfo->shmaddr == (char *)-1)
  {
    perror("alloc_back_buffer");
    XDestroyImage(img);
    img = NULL;
    return NULL;
  }

  shminfo->readOnly = False;
  ErrorFlag = 0;
  XSetErrorHandler(HandleXError);
  /* This may trigger the X protocol error we're ready to catch: */
  XShmAttach(dpy, shminfo);
  XSync(dpy, False);

  if (ErrorFlag)
  {
    /* we are on a remote display, this error is normal, don't print it */
    XFlush(dpy);
    ErrorFlag = 0;
    XDestroyImage(img);
    shmdt(shminfo->shmaddr);
    shmctl(shminfo->shmid, IPC_RMID, 0);
    return NULL;
  }

  shmctl(shminfo->shmid, IPC_RMID, 0); /* nobody else needs it */
  return img;
}

void destroy_xshm(Display *dpy, XShmSegmentInfo *shminfo)
{
  XShmDetach(dpy, shminfo);
  shmdt(shminfo->shmaddr);
}

static void on_cursor_event(int buttonMask, int x, int y, rfbClientPtr cl)
{
  client_data *data = (client_data *)cl->clientData;

  // XWarpPointer(data->server_ptr->display,
  //               None,
  //               DefaultRootWindow(data->server_ptr->display),
  //              0, //src_x,
  //              0, //src_y,
  //              0, //src_width,
  //              0, //src_height,
  //              x,
  //              y);

  XTestFakeMotionEvent(data->server->dpy,
                       DefaultScreen(data->server->dpy),
                       x, y, CurrentTime);

  if (buttonMask != data->oldButton)
  {
    int btn;
    for (btn = 0; btn < 8; ++btn)
      if ((buttonMask ^ data->oldButton) & (1 << btn))
        if (buttonMask & (1 << btn))
        {
          printf("CLicked: %d\n", btn);
          XTestFakeButtonEvent(data->server->dpy, btn + 1, True, CurrentTime);
        }
        else
        {
          printf("Released: %d\n", btn);
          XTestFakeButtonEvent(data->server->dpy, btn + 1, False, CurrentTime);
        }
  }
  XFlush(data->server->dpy);
  data->oldButton = buttonMask;
}

static void on_key_event(rfbBool down, rfbKeySym keysym, rfbClientPtr cl)
{
  //TODO
  client_data *data = (client_data *)cl->clientData;
  if (down)
  {
    printf("%c down\n", keysym);
    int keycode = XKeysymToKeycode(data->server->dpy, keysym);
    //XSync(data->server_ptr->display, False);
    XTestFakeKeyEvent(data->server->dpy, keycode, down, 0L);
    // XFlush(data->server_ptr->display);
    XTestFakeKeyEvent(data->server->dpy, keycode, 0, 0L);
    XFlush(data->server->dpy);
  }
}

void logVNCServer(VNCServerPtr server)
{
  if (server == NULL || server->dpy == NULL)
    return;

  Display *dpy = server->dpy;

  DEBUG("Display connection number: %d", ConnectionNumber(dpy));
  DEBUG("Display name: '%s'", DisplayString(dpy));
  DEBUG("Display protocol version: %d.%d", ProtocolVersion(dpy), ProtocolRevision(dpy));
  DEBUG("Display vendor hw: %s", ServerVendor(dpy));
  DEBUG("Display byte order: %s", XImageByteOrder(dpy) == LSBFirst ? "Little Endian" : "Big Endian");

  DEBUG("Display bitmap unit: %d", BitmapUnit(dpy));
  DEBUG("Display bitmap pad : %d", BitmapPad(dpy));
  DEBUG("Display bitmap bit_order: %s", BitmapBitOrder(dpy) == LSBFirst ? "Little Endian" : "Big Endian");

  DEBUG("Display num of screens: %d", ScreenCount(dpy));
  int screen;
  for (screen = 0; screen < ScreenCount(dpy); ++screen)
  {
    if (screen == DefaultScreen(dpy))
      DEBUG("[DEFAULT SCREEN]");
    else
      DEBUG("[SCREEN]");

    Screen *scr = ScreenOfDisplay(dpy, screen);

    DEBUG("  screen root window ID: %d", scr->root);
    DEBUG("  screen res: %dx%d pixels", scr->width, scr->height);
    DEBUG("  screen res: %dx%d millimeters", scr->mwidth, scr->mheight);
    DEBUG("  screen bits per pixels: %d", scr->root_depth);

    /*
    DEBUG("  screen depths:");
    INT32 d, v;
    UINT32 depth, vid, rmsk, gmsk, bmsk, bits_p_rgb, map_entries; 
    for (d = 0; d < scr->ndepths; ++d) {
      depth = scr->depths[d].depth;
      DEBUG("    for depth: %d, ", depth);
      for (v = 0; v < scr->depths[d].nvisuals; ++v) {
        vid  = scr->depths[d].visuals[v].visualid;
        rmsk = scr->depths[d].visuals[v].red_mask;
        gmsk = scr->depths[d].visuals[v].green_mask;
        bmsk = scr->depths[d].visuals[v].blue_mask;
        bits_p_rgb = scr->depths[d].visuals[v].bits_per_rgb;
        map_entries = scr->depths[d].visuals[v].map_entries;
        
        DEBUG("      visual[%d]:\n"
              "\t\t bits pre RGB: %d\n"
              "\t\t red   msk: %08lx\n"
              "\t\t green msk: %08lx\n"
              "\t\t blue  msk: %08lx\n"
              "\t\t map entries: %d", 
          vid, bits_p_rgb, rmsk, gmsk, bmsk, map_entries);
      }
    }
    */

    DEBUG("  screen depth   : %d", DefaultDepthOfScreen(scr));
    DEBUG("  screen colormap: %d", DefaultColormapOfScreen(scr));

    // Pixel
    DEBUG("  screen white PIXEL: %08lx", WhitePixelOfScreen(scr));
    DEBUG("  screen black PIXEL: %08lx", BlackPixelOfScreen(scr));

    // Screen visuals
    Visual *vis = DefaultVisualOfScreen(scr);

    DEBUG("  screen red   msk: %08lx", vis->red_mask);
    DEBUG("  screen green msk: %08lx", vis->green_mask);
    DEBUG("  screen blue  msk: %08lx", vis->blue_mask);
    DEBUG("  screen bits_per_rgb: %d", vis->bits_per_rgb);
    DEBUG("  screen colour map entries : %d", vis->map_entries);
  }
}

VNCServerPtr getVNCServer()
{
  VNCServerPtr server;
  server = (VNCServerPtr)calloc(sizeof(VNCServer), 1);
  if (!server)
  {
    fprintf(stderr, "malloc failed\n");
    return NULL;
  }

  // This function must be the first Xlib function a multi-threaded program calls,
  // and it must complete before any other Xlib call is made
  if (XInitThreads() == False)
  {
    FATAL("XInitThreads error\n");
    return NULL;
  }

  // Init X11 server
  if ((server->dpy = XOpenDisplay(getenv("DISPLAY"))) == NULL)
  {
    FATAL("Cannot connect to X server %s\n", getenv("DISPLAY") ? getenv("DISPLAY") : "(default)");
    return NULL;
  }

  // check for extensions
  if (check_for_xkb(server->dpy) == XKB_NOT_AVAILABLE)
  {
    fprintf(stderr, "XKB extension not available!\n");
    XCloseDisplay(server->dpy);
    free(server);
    return NULL;
  }

  if (check_for_xfixes(server->dpy) == XFIXES_NOT_AVAILABLE)
  {
    fprintf(stderr, "XFixes extension not available!\n");
    XCloseDisplay(server->dpy);
    free(server);
    return NULL;
  }

#if defined(USE_SHM)
  if (check_for_xshm(server->dpy) == SHM_NOT_AVAILABLE)
  {
    fprintf(stderr, "MITM-SHM extension is not available\n");
    XCloseDisplay(server->dpy);
    return NULL;
  }
  printf("Let's go with" COLOR_GREEN " MITM-SHM!\n" COLOR_NONE);

  /* make shared XImage */
  server->xim = alloc_xshm_image(server->dpy,
                                 DefaultVisual(server->dpy, DefaultScreen(server->dpy)),
                                 DisplayWidth(server->dpy, DefaultScreen(server->dpy)),
                                 DisplayHeight(server->dpy, DefaultScreen(server->dpy)),
                                 DefaultDepth(server->dpy, DefaultScreen(server->dpy)),
                                 &server->shminfo);
  if (!server->xim)
  {
    fprintf(stderr, "couldn't allocate shared XImage\n");
    XCloseDisplay(server->dpy);
    free(server);
    return NULL;
  }
  server->use_shm = TRUE;
#else
  /* NO SHM */
  server->use_shm = FALSE;
  printf("Let's go without" COLOR_RED " MITM-SHM!\n" COLOR_NONE);
  server->xim = XCreateImage(server->dpy,
                             DefaultVisual(server->dpy, DefaultScreen(server->dpy)),
                             DefaultDepth(server->dpy, DefaultScreen(server->dpy)),
                             ZPixmap, 0, 0,
                             DisplayWidth(server->dpy, DefaultScreen(server->dpy)),
                             DisplayHeight(server->dpy, DefaultScreen(server->dpy)),
                             BitmapPad(server->dpy),
                             0);
  if (!server->xim)
  {
    fprintf(stderr, "couldn't allocate XImage\n");
    XCloseDisplay(server->dpy);
    return NULL;
  }
  server->xim->data = malloc(server->xim->bytes_per_line * server->xim->height);
  if (server->xim->data == NULL)
  {
    fprintf(stderr, "couldn't allocate XImage data\n");
    XCloseDisplay(server->dpy);
    return NULL;
  }
#endif

  INT32 width = DisplayWidth(server->dpy, DefaultScreen(server->dpy));
  INT32 height = DisplayHeight(server->dpy, DefaultScreen(server->dpy));
  INT32 depth = DefaultDepth(server->dpy, DefaultScreen(server->dpy));
  INT32 bits_per_rgb = DefaultVisual(server->dpy, DefaultScreen(server->dpy))->bits_per_rgb;
  INT32 bpp = server->xim->bits_per_pixel;

  server->geo.width = width;
  server->geo.height = height;

  DEBUG("Display geo: %dx%d", width, height);

  server->last_frame = malloc(server->xim->bytes_per_line * server->xim->height);
  memset(server->last_frame, 0, server->xim->bytes_per_line * server->xim->height);
  return server;
}

void initVNCServer(VNCServerPtr server, int *argc, char **argv)
{
  INT32 width = DisplayWidth(server->dpy, DefaultScreen(server->dpy));
  INT32 height = DisplayHeight(server->dpy, DefaultScreen(server->dpy));
  INT32 depth = DefaultDepth(server->dpy, DefaultScreen(server->dpy));
  INT32 bits_per_rgb = DefaultVisual(server->dpy, DefaultScreen(server->dpy))->bits_per_rgb;
  INT32 bpp = server->xim->bits_per_pixel;

  rfbScreenInfoPtr rfb = rfbGetScreen(argc, &argv, /* argc, argv */
                                      width,
                                      height,
                                      bits_per_rgb,         /* bitsPerSample */
                                      depth / bits_per_rgb, /* samplesPerPixel */
                                      bpp / 8 /* bytesPerPixel */);

  if (!rfb)
  {
    FATAL("Failed to rfbGetScreen");
    return;
    // XCloseDisplay(server->dpy);
    // XDestroyImage(server->xim);
    // free(server);
    // return NULL;
  }

  rfb->serverFormat.bigEndian = XImageByteOrder(server->dpy) == MSBFirst;
  rfb->serverFormat.bitsPerPixel = bpp;
  rfb->serverFormat.redMax = server->xim->red_mask >> (ffs(server->xim->red_mask) - 1);
  rfb->serverFormat.greenMax = server->xim->green_mask >> (ffs(server->xim->green_mask) - 1);
  rfb->serverFormat.blueMax = server->xim->blue_mask >> (ffs(server->xim->blue_mask) - 1);
  rfb->serverFormat.redShift = ffs(server->xim->red_mask) - 1;
  rfb->serverFormat.greenShift = ffs(server->xim->green_mask) - 1;
  rfb->serverFormat.blueShift = ffs(server->xim->blue_mask) - 1;

  rfb->desktopName = "VNC";
  //rfb->frameBuffer = (char*)malloc(maxx*maxy*bpp);
  rfb->alwaysShared = TRUE;
  rfb->ptrAddEvent = on_cursor_event;
  rfb->kbdAddEvent = on_key_event;

  server->rfbPtr = rfb;

  // in_addr_t iface = rfb->listenInterface;
  // if (rfb->socketState!=RFB_SOCKET_INIT)
  //   return;
  // rfb->socketState = RFB_SOCKET_READY;

  // rfb->inetdSock = 2;
  // const int one = 1;
  // if (!rfbSetNonBlocking(rfb->inetdSock))
  //   return;

  // if (setsockopt(rfb->inetdSock, IPPROTO_TCP, TCP_NODELAY,
  //                (char *)&one, sizeof(one)) < 0)
  // {
  //   rfbLogPerror("setsockopt");
  //   return;
  // }

  // FD_ZERO(&(rfb->allFds));
  // FD_SET(rfb->inetdSock, &(rfb->allFds));
  // rfb->maxFd = rfb->inetdSock;

  // rfbInitServer(rfb);
  /* this is the non-blocking event loop; a background thread is started */
  // rfbRunEventLoop(rfb, -1, TRUE);
}

void setServerSock(VNCServerPtr srv, int sock)
{
  if (srv == NULL)
    return;
  if (srv->rfbPtr == NULL)
    return;

  if (srv->rfbPtr->socketState == RFB_SOCKET_READY)
    return;

  srv->rfbPtr->socketState = RFB_SOCKET_READY;

  srv->rfbPtr->listenSock = sock;

  FD_ZERO(&(srv->rfbPtr->allFds));
  FD_SET(srv->rfbPtr->listenSock, &(srv->rfbPtr->allFds));
  srv->rfbPtr->maxFd = srv->rfbPtr->listenSock;

  /* this is the non-blocking event loop; a background thread is started */
  //rfbRunEventLoop(srv->rfbPtr, -1, TRUE);
  rfbClientPtr cl = NULL;
  cl = rfbNewClient(srv->rfbPtr, sock);
  if (cl && !cl->onHold)
    rfbStartOnHoldClient(cl);
}

static void noob_X11VNCServer_compute_diff(VNCServerPtr srv)
{
  struct timeval tv_start, tv_stop;
  struct tm *tm_info;
  time_t timer;
  int x, y;
  char file_name[255];
  double ms;

  const UINT32 *current_frame;
  const UINT32 *last_frame;

  if (!srv->last_frame || !srv->curr_frame)
    return;

  last_frame = (const UINT32 *)srv->last_frame;
  current_frame = (const UINT32 *)srv->curr_frame;

  BOOL same = TRUE;
  BOOL time_it = FALSE;
  BOOL save_diff = FALSE;

  // top left coord, bottom right coord of
  // single rectangle that contains different pixels
  // of the 2 frames
  INT32 tl_x, tl_y, br_x, br_y;

  tl_x = srv->xim->width;
  tl_y = srv->xim->height;
  br_x = 0;
  br_y = 0;

  // [Compare frames] - Start time
  if (time_it)
    gettimeofday(&tv_start, NULL);

  for (y = 0; y < srv->xim->height; ++y)
  {
    last_frame = (const UINT32 *)(srv->last_frame +
                                  srv->xim->bytes_per_line * y);
    current_frame = (const UINT32 *)(srv->curr_frame +
                                     srv->xim->bytes_per_line * y);

    if (memcmp(last_frame, current_frame, srv->xim->bytes_per_line) != 0)
    {
      same = FALSE;
      for (x = 0; x < srv->xim->width; ++x)
      {
        if (*last_frame++ != *current_frame++)
        {
          if (x < tl_x)
            tl_x = x;
          if (y < tl_y)
            tl_y = y;
          if (x > br_x)
            br_x = x;
          if (y > br_y)
            br_y = y;
        }
      }
    }
  }

  // [Compare frames] - Stop time
  if (time_it)
    gettimeofday(&tv_stop, NULL);

  // [Compare frames] - Avg time
  if (time_it)
  {
    tvsub(&tv_stop, &tv_start);
    ms = tv_stop.tv_sec * 1000 + tv_stop.tv_usec / 1000.0;
    printf("[Compare frames] %f ms\n", ms);
  }

  // frames are not the same
  if (!same)
  {
    if (time_it)
      gettimeofday(&tv_start, NULL);

    if (save_diff)
    {
      time(&timer);
      tm_info = localtime(&timer);
      strftime(file_name, sizeof(file_name), "diff/diff%Y%m%d_%H%M%S.bmp", tm_info);
      write_X11SubImage_bmp(file_name, srv->xim, tl_x, tl_y, br_x, br_y);
    }

    if (time_it)
      gettimeofday(&tv_stop, NULL);

    if (time_it)
    {
      tvsub(&tv_stop, &tv_start);
      ms = tv_stop.tv_sec * 1000 + tv_stop.tv_usec / 1000.0;
      printf("[Save diff] %f ms\n", ms);
    }

    if (time_it)
      gettimeofday(&tv_start, NULL);

    char *tmp = srv->rfbPtr->frameBuffer;
    srv->rfbPtr->frameBuffer = srv->xim->data;
    srv->xim->data = tmp;

    rfbMarkRectAsModified(srv->rfbPtr, tl_x, tl_y, br_x, br_y);

    if (time_it)
      gettimeofday(&tv_stop, NULL);

    if (time_it)
    {
      tvsub(&tv_stop, &tv_start);
      //printf("[Send diff] %d.%d sec\n", tv_stop.tv_sec, tv_stop.tv_usec);
    }
  }
}

void runVNCServer(VNCServerPtr srv)
{
  // printf("runVNCServer\n");
  if (srv == NULL)
    return;

  // printf("runVNCServer ---\n");

  srv->curr_frame = srv->xim->data;
  memcpy(srv->last_frame, srv->curr_frame, srv->xim->bytes_per_line * srv->xim->height);
  // Grab the screen
#if defined(USE_SHM)
  /* WITH SHM*/
  if (!XShmGetImage(srv->dpy,
                    DefaultRootWindow(srv->dpy),
                    srv->xim,
                    0, 0, AllPlanes))
  {
    fprintf(stderr, "Something went wrong\n");
    return;
  }
  srv->rfbPtr->frameBuffer = srv->shminfo.shmaddr;
#else
  /* NO SHM */
  if (!XGetSubImage(srv->dpy,
                    DefaultRootWindow(srv->dpy),
                    0, 0,
                    srv->geo.width,
                    srv->geo.height,
                    AllPlanes, ZPixmap,
                    srv->xim, 0, 0))
  {
    fprintf(stderr, "Something went wrong\n");
    return;
  }
  srv->rfbPtr->frameBuffer = srv->xim->data;
#endif

  // Calculate diff
  noob_X11VNCServer_compute_diff(srv);
  // Calculate diff
}

void deinitVNCServer(VNCServerPtr srv)
{
  if (srv == NULL)
    return;

    // Release resources
#if defined(USE_SHM)
  destroy_xshm(srv->dpy, &srv->shminfo);
#endif
  if (srv->xim != NULL)
    XDestroyImage(srv->xim);
  if (srv->rfbPtr != NULL)
    rfbScreenCleanup(srv->rfbPtr);
  if (srv->dpy != NULL)
    XCloseDisplay(srv->dpy);
  printf("Cleanup done!\n\n");
}