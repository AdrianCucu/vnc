#include "X11_utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

BOOL write_X11Image_bmp(const char *filename, XImage *img)
{
  assert(filename);
  assert(img);
  return write_X11SubImage_bmp(filename, img, 0, 0, img->width - 1, img->height - 1);
}

BOOL write_X11SubImage_bmp(const char *filename, XImage *img, INT32 x1, INT32 y1, INT32 x2, INT32 y2)
{
  BOOL ret;
  UINT32 pix_value;
  UINT32 *pix;
  INT32 y, x;
  INT32 width, height;
  INT32 bytes_per_pixel;
  Pixel_t *pixel_array;
  Pixel_t *pixel;

  assert(filename);
  assert(img);

  if (img->format != ZPixmap)
  {
    fprintf(stderr, "XImage format is not ZPixmap\n");
    return FALSE;
  }

  if (x1 < 0 || y1 < 0 || x2 >= img->width || y2 >= img->height)
  {
    fprintf(stderr, "Invalid XImage subimage bounds\n");
    return FALSE;
  }

  width  = x2 - x1 + 1;
  height = y2 - y1 + 1;
  bytes_per_pixel = img->bits_per_pixel / 8;

  pixel_array = (Pixel_t *)malloc(width * height * sizeof(Pixel_t));
  if (!pixel_array)
  {
    fprintf(stderr, "write_X11SubImage_bmp: malloc not enough memory\n");
    return FALSE;
  }
  pixel = pixel_array;

  /* collect separate RGB values to pixel_array */
  for (y = y1; y <= y2; ++y)
  {
    pix = (UINT32 *)(img->data + y * img->bytes_per_line + x1 * bytes_per_pixel);

    for (x = x1; x <= x2; ++x)
    {
      pix_value = *pix++;
      pixel->r = (UINT8)((pix_value >> 16) & 0xff);
      pixel->g = (UINT8)((pix_value >>  8) & 0xff);
      pixel->b = (UINT8)((pix_value >>  0) & 0xff);
      if (y == y2 && x == x2)
        break;
      ++pixel;
    }
  }

  ret = write_pixel_array_bmp(filename, width, height, pixel_array);

  free(pixel_array);
  return ret;
}

BOOL write_X11Cursor_bmp(const char *filename, XFixesCursorImage *cim)
{
  return write_X11ScaledCursor_bmp(filename, cim, 1, 1);
}

BOOL write_X11ScaledCursor_bmp(const char *filename,
                               XFixesCursorImage *cim,
                               INT32 x_scale,
                               INT32 y_scale)
{
  BOOL ret;
  INT32 y, x;
  PIXEL *crs_pixels;
  PIXEL crs_pix;
  Pixel_t *pixel_array;
  Pixel_t *pixel;
  INT32 scale_width, scale_height;

  assert(filename);
  assert(cim);

  scale_width = x_scale;
  scale_height = y_scale;
  if (x_scale <= 0)
    scale_width = 1;
  if (y_scale <= 0)
    scale_height = 1;

  pixel_array = (Pixel_t *)malloc(cim->width * scale_width *
                                  cim->height * scale_height * sizeof(Pixel_t));
  if (!pixel_array)
  {
    fprintf(stderr, "malloc not enough memory\n");
    return FALSE;
  }
  pixel = pixel_array;

  crs_pixels = (PIXEL *)cim->pixels;

  for (y = 0; y < cim->height * scale_height; ++y)
  {
    crs_pixels = ((PIXEL *)cim->pixels) + (y / scale_height) * cim->width;

    for (x = 0; x < cim->width * scale_width; ++x)
    {
      crs_pix = *(crs_pixels + (x / scale_width));
      UINT8 alpha = (crs_pix >> 24) & 0xff;

      if (alpha == 0)
        alpha = 1; // Avoid division by zero

      // Mark xhot, yhot point
      if (-1 <= (x / scale_width) - cim->xhot &&
          (x / scale_width) - cim->xhot <= 1 &&
          -1 <= (y / scale_height) - cim->yhot &&
          (y / scale_height) - cim->yhot <= 1)
      {
        pixel->r = 0x00;
        pixel->g = 0x00;
        pixel->b = 0xff;
      }
      else
      {
        pixel->r = ((crs_pix >> 16) & 0xff) * 255 / alpha;
        pixel->g = ((crs_pix >> 8) & 0xff) * 255 / alpha;
        pixel->b = ((crs_pix >> 0) & 0xff) * 255 / alpha;
      }
      ++pixel;
    }
  }

  ret = write_pixel_array_bmp(filename, cim->width * scale_width, cim->height * scale_height, pixel_array);

  free(pixel_array);
  return ret;
}