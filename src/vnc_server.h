#ifndef __VNC_SERVER_H__
#define __VNC_SERVER_H__

#include <assert.h>

#ifdef WIN32
#define sleep Sleep
#else
#include <unistd.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#endif

#include <rfb/rfb.h>
#include <rfb/keysym.h>
#include <rfb/rfbregion.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/XKBlib.h>

#include <X11/extensions/XTest.h>
#include <X11/extensions/Xfixes.h>
#include <X11/extensions/Xrandr.h>
#include <X11/extensions/XShm.h>

#include "log.h"
#include "bitmap.h"
#include "pixelformat.h"
#include "X11_utils.h"
#include "tv.h"
#include "constants.h"

#define SHM_NOT_AVAILABLE 0
#define HAVE_SHM_XIMAGE 1
#define HAVE_SHM_PIXMAP 2

#define XFIXES_NOT_AVAILABLE 0
#define HAVE_XFIXES 1

#define XKB_NOT_AVAILABLE 0
#define HAVE_XKB 1

typedef struct
{
  INT32 x, y;
} Point;

typedef struct
{
  INT32 width;
  INT32 height;
} Geometry;

typedef struct _X11VNCServer
{
  Geometry geo;
  Display *dpy;
  rfbScreenInfoPtr rfbPtr;
  XImage *xim;

  BYTE *last_frame;
  BYTE *curr_frame;

  BOOL use_shm; // use shared memory extension
#if defined(USE_SHM)
  XShmSegmentInfo shminfo;
#endif

} VNCServer, *VNCServerPtr;

/* Here we create a structure so that every client has its own pointer */
typedef struct __client_data
{
  rfbBool oldButton;
  int oldx, oldy;
  VNCServerPtr server; // owner of this client
} client_data;

void logVNCServer(VNCServerPtr server);
VNCServerPtr getVNCServer();
void runVNCServer(VNCServerPtr srv);
void initVNCServer(VNCServerPtr srv, int *argc, char **argv);
void setServerSock(VNCServerPtr srv, int sock);
void deinitVNCServer(VNCServerPtr srv);

#endif /*__VNC_SERVER_H__*/
